# Munich Motion Dataset of Natural Driving (MONA)

The Munich Motion Dataset of Natural Driving (MONA) is a large-scale dataset of vehicle trajectories in urban and highway environments. It allows free and unconstraint use of the raw, processed, and reference data. The processing toolchain is published under the BSD license.

The video footage was recorded in 2021. Three cameras were placed on the 28th floor of the Highlight Tower 1, located in north Munich, at the interchange of the Autobahn A9 and the Schenkendorfstraße, a main bypass road in Munich. We provide a converter to use the dataset within the [CommonRoad](https://commonroad.in.tum.de) framework.

## Images
<div align=center>
![Perspektives of the MONA dataset](/docs/images/perspectives.png)
</div>   


## Statistics
The dataset features over 700.000 trajectories, as well as, ground-truth data captured from a measurement vehicle.

<div align=center>
![Summary statistics of the MONA dataset](/docs/images/stats.png)
</div>

## Cite us
If you refer to our dataset, please cite our publication:


> L. Gressenbuch, K. Esterle, T. Kessler and M. Althoff, "MONA: The Munich Motion Dataset of Natural Driving," 2022 IEEE 25th International Conference on Intelligent Transportation Systems (ITSC), 2022, pp. 2093-2100, doi: 10.1109/ITSC55140.2022.9922263.

## Acknowledgements

The MONA toolchain is based on the work of Clausse, Benslimane and de La Fortelle [\[paper\]](https://doi.org/10.1109/IVS.2019.8814095) [\[code\]](https://github.com/AubreyC/trajectory-extractor):
> A. Clausse, S. Benslimane and A. de La Fortelle, "Large-Scale extraction of accurate vehicle trajectories for driving behavior learning," 2019 IEEE Intelligent Vehicles Symposium (IV), Paris, France, 2019, pp. 2391-2396.


# Getting started

## Prerequisites

* Make sure you have Git LFS and [Anaconda](https://anaconda.org/anaconda/conda) installed.
* For docker execution, [Docker](https://www.docker.com/) has to be installed. Using GPUs in the docker container during the detection stage requires a working [NVIDIA docker container setup](https://github.com/NVIDIA/nvidia-container-toolkit). 
* Note that the package was developed and tested under Ubuntu 20.04.

## Docker

For best reproducibility, we recommend to run the toolchain in Docker. 

### Build docker image
Docker images can be easily build by the provided Dockerfile
```bash
docker build -t IMAGE_TAG --target TARGET .
```

The package provides two flavors of images (targets)

* `app`: The app image contains all python dependencies and third-party package build dependencies. The image is best suited for development.
* `minimal`: The minimal image is a [packed conda environment](https://conda.github.io/conda-pack/) and is much smaller size. The image is best suited for running the mona toolchain as it is.

### Run mona toolchain in docker

Once the image has been build, the toolchain can be run
```bash
docker run [--gpus 1] --shm-size=2GB -v /path/to/input:/mnt:ro -v /path/to/output:/output IMAGE_TAG mona detect /mnt /output
```


## Get the data

To get access to the dataset please log in to commonroad.in.tum.de and browse https://commonroad.in.tum.de/datasets.

### Hint:
To download all data at once simply run:
```bash
wget -nP -nH -r --reject "index.html*" "URL_FROM_COMMONROAD_WEBSITE"
```

# Quick start

The package provides a CLI to run the extraction process:

```
Usage: mona [OPTIONS] COMMAND [ARGS]...

Options:
  --help                          Show this message and exit.

Commands:
  association           Perform association stage
  detect                Detect vehicles in video and write detections to...
  feasibility           Perform feasibility optimization stage
  pose-estimation       Perform pose estimation stage
  post-processing       Run pipeline processing steps except detection.
  reference-evaluation  Evaluate extracted trajectories of reference drives
  smoothing             Perform smoothing stage
```

As the detection requires GPUs for acceptable runtime (even if, in theory, it could be also run on a CPU), the detection stage is separated from the other pipeline stages. To start the detection on a video, run the following command where `reference_drives/raw/east_outgoing/east-000-000.yaml` is the video's corresponding meta file and `output` is the output directory within which a video-specific directory `output/east-000-000.yaml` containing the detection results is created:

```bash
mona detect reference_drives/raw/east_outgoing/east-000-000.yaml output
```

The remaining pipeline stages can then be run with:

```bash
mona post-processing output/east-000-000
```

For fine-grained control of all parameters, we use [hydra](https://hydra.cc). The configuration files are stored under `mona/config/` and can be modified.

The CLI also allows running each stage separately. See the `dvc.yaml` file and the CLI help `mona --help` for examples and further details.

# Meta files
A corresponding metafile accompanies each video file in YAML format to store metadata for the video recordings. 
The metafile contains required information for further processing steps like the recording location, or the frame rate, and optional information like the number of frames.


```yaml
location: east # Recording location
duration_seconds: 28.84 # Video duration in seconds
num_frames: 723 # Number of frames
width: 3840 # Frame width in pixels
height: 2160 # Frame height in pixels
fps: 25 # Frame rate in frames per second
path: east-000-000.MP4 # Path to the video recording relative to the meta file
```

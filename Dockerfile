# Uncomment to enable blocksqp solver
#FROM debian:buster AS coinhsl
#RUN apt-get update && apt-get install -y build-essential unzip gfortran
#COPY coinhsl-2021.05.05.zip /root
#WORKDIR /root
#RUN unzip coinhsl-2021.05.05.zip
#RUN cd coinhsl-2021.05.05 && ./configure && make && make install

FROM continuumio/miniconda3 AS base
# Uncomment to enable blocksqp solver
#COPY --from=coinhsl /usr/local/lib/libcoinhsl.so* /usr/local/lib/
#RUN ldconfig
RUN conda update conda pip
RUN apt-get update && apt-get install -y build-essential ffmpeg libsm6  \
    libxext6 git-lfs unzip gfortran rsync cmake libeigen3-dev libboost-dev
WORKDIR /root
COPY environment.yml /root/
RUN conda env create -f environment.yml

FROM base AS app
COPY mona /app/mona
COPY setup.py MANIFEST.in /app/
RUN conda run -n mona-toolchain pip install /app

FROM base AS packed
RUN conda install -c conda-forge conda-pack
RUN conda-pack -n mona-toolchain -o /tmp/env.tar && \
  mkdir /venv && cd /venv && tar xf /tmp/env.tar && \
  rm /tmp/env.tar
RUN /venv/bin/conda-unpack

FROM debian:buster AS minimal
RUN apt-get update && apt-get install -y libglib2.0-0 libsm6 libxrender1 libxext6 libgl1 git
COPY --from=packed /venv /venv
# Uncomment to enable blocksqp solver
#COPY --from=coinhsl /usr/local/lib/libcoinhsl.so* /usr/local/lib/
#RUN ldconfig
ENV PATH="/venv/bin:$PATH"
COPY mona /app/mona
COPY setup.py MANIFEST.in /app/
RUN pip install /app
CMD mona --help

from pathlib import Path
from typing import Optional

import hydra
import typer
from omegaconf import OmegaConf

from mona.utils.util import read_pandas
from mona.visualization import (
    visualize_pose,
    visualize_tracks,
    visualize_trajectories,
    visualize_detections,
)

app = typer.Typer()


def _set_start_end(
    processed_folder: Path,
    track_id: int,
    start_frame: Optional[int],
    end_frame: Optional[int],
    return_df: bool = False,
):
    assert processed_folder.exists(), (
        f"Directory not found " f"{processed_folder.resolve().absolute()}!"
    )
    df = None
    if track_id is not None:
        df = read_pandas(next(processed_folder.rglob("trajectories.*")))
        assert track_id in df.track_id.unique(), f"Track ID {track_id} not found!"
        track_df = df[df.track_id == track_id]
        if start_frame is None:
            start_frame = track_df.frame_id.min()
        if end_frame is None:
            end_frame = track_df.frame_id.max() + 50

    with hydra.initialize("../config"):
        base_config = hydra.compose("visualization")
    video_config = OmegaConf.load(processed_folder / "config.yaml")
    OmegaConf.set_struct(base_config, False)
    config = OmegaConf.merge(base_config, video_config)

    if return_df and df is None:
        df = read_pandas(next(processed_folder.rglob("trajectories.*")))

    if start_frame is not None:
        config.video.skip = int(start_frame)
        if df is not None:
            df = df[df.frame_id >= start_frame]
    if end_frame is not None:
        config.video.frame_limit = int(end_frame)
        if df is not None:
            df = df[df.frame_id <= end_frame]

    config.video.path = str((processed_folder / config.video.path).resolve().absolute())
    config.output_dir = str(processed_folder)

    ret = config
    if return_df:
        ret = ret, df

    return ret


@app.command()
def trajectories(
    processed_folder: Path = typer.Argument(
        ..., help="Path to folder with outputs of trajectory extraction"
    ),
    track_id: Optional[int] = typer.Option(
        None, help="Limit video to lifetime of object with ID"
    ),
    start_frame: Optional[int] = typer.Option(None, help="Frame ID to start with"),
    end_frame: Optional[int] = typer.Option(None, help="Frame ID to end with"),
):
    """Visualize trajectories."""
    config, traj_df = _set_start_end(
        processed_folder, track_id, start_frame, end_frame, return_df=True
    )
    visualize_trajectories(config, trajs=traj_df)


@app.command()
def poses(
    processed_folder: Path = typer.Argument(
        ..., help="Path to folder with outputs of trajectory extraction"
    ),
    track_id: Optional[int] = typer.Option(
        None, help="Limit video to lifetime of object with ID"
    ),
    start_frame: Optional[int] = typer.Option(None, help="Frame ID to start with"),
    end_frame: Optional[int] = typer.Option(None, help="Frame ID to end with"),
):
    """Visualize poses"""
    config = _set_start_end(processed_folder, track_id, start_frame, end_frame)
    visualize_pose(config)


@app.command()
def tracks(
    processed_folder: Path = typer.Argument(
        ..., help="Path to folder with outputs of trajectory extraction"
    ),
    track_id: Optional[int] = typer.Option(
        None, help="Limit video to lifetime of object with ID"
    ),
    start_frame: Optional[int] = typer.Option(None, help="Frame ID to start with"),
    end_frame: Optional[int] = typer.Option(None, help="Frame ID to end with"),
):
    """Visualize tracks"""
    config = _set_start_end(processed_folder, track_id, start_frame, end_frame)
    visualize_tracks(config)


@app.command()
def detections(
    processed_folder: Path = typer.Argument(
        ..., help="Path to folder with outputs of trajectory extraction"
    ),
    track_id: Optional[int] = typer.Option(
        None, help="Limit video to lifetime of object with ID"
    ),
    start_frame: Optional[int] = typer.Option(None, help="Frame ID to start with"),
    end_frame: Optional[int] = typer.Option(None, help="Frame ID to end with"),
):
    """Visualize detections"""
    config = _set_start_end(processed_folder, track_id, start_frame, end_frame)
    visualize_detections(config)


if __name__ == "__main__":
    app()

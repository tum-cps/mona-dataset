import itertools
import logging
import traceback
from enum import Enum
from pathlib import Path

import hydra
import ray
import typer
from omegaconf import OmegaConf
from tqdm import tqdm


import mona.evaluation.reference_evaluation
import mona.evaluation
from mona.association.execution import associate
from mona.pose_estimation.execution import estimate_pose
from mona.feasibility_processing.execution import optimize_feasibility
from mona.smoothing.execution import smooth

logger = logging.getLogger(__name__)
app = typer.Typer()


class TrajectoryFormat(str, Enum):
    parquet = "parquet"
    csv = "csv"
    feather = "feather"


@app.command()
def detect(
    video_dir: Path = typer.Argument(..., help="Directory to search for video files"),
    output_dir: Path = typer.Argument(
        ..., help="Output directory for processed results"
    ),
):
    """
    Detect vehicles in video and write detections to detections.hd5 file.
    """
    import mona.detection.execution
    from mona.detection.detector import MpDetectronDetection
    import torch.multiprocessing as mp

    with hydra.initialize("../config"):
        config = hydra.compose("detection")
    OmegaConf.set_struct(config, False)
    mp.set_start_method("spawn")
    video_dir = video_dir
    base_output_dir = output_dir
    OmegaConf.set_struct(config, False)
    detector = MpDetectronDetection.create_from_config(config)
    if video_dir.is_dir():
        files = list(
            itertools.chain(video_dir.rglob("*.yml"), video_dir.rglob("*.yaml"))
        )
    else:
        files = [video_dir]
        video_dir = video_dir.parent
    for video_meta_path in tqdm(files, desc="Total progress", unit="file"):
        video_meta = OmegaConf.load(video_meta_path)
        video_conf = config.copy()
        video_conf.video = video_meta
        # Try to get video.video_path (old) or video.path
        video_path = video_conf.video.get("video_path", video_conf.video.path)
        video_path = video_meta_path.parent / video_path
        video_conf.video.path = str(video_path)
        video_conf.output_dir = str(
            base_output_dir
            / (video_meta_path.with_name(video_meta_path.stem)).relative_to(video_dir)
        )
        try:
            mona.detection.execution.detect(video_conf, detector)
        except KeyboardInterrupt:
            break


@app.command()
def pose_estimation(
    output_dir: Path = typer.Argument(
        ..., help="Output directory for processed results"
    ),
    num_threads: int = typer.Option(1, help="Number of worker processes"),
):
    """
    Perform pose estimation stage
    """
    _post_processing(
        output_dir,
        pose_estimation=True,
        association=False,
        smoothing=False,
        feasibility=False,
        num_threads=num_threads,
    )


@app.command()
def association(
    output_dir: Path = typer.Argument(
        ..., help="Output directory for processed results"
    ),
    num_threads: int = typer.Option(1, help="Number of worker processes"),
):
    """
    Perform association stage
    """
    import ray.util.multiprocessing as mp

    with hydra.initialize("../config"):
        config = hydra.compose("config")
    config.num_threads = num_threads
    path = output_dir
    jobs = list(path.rglob("poses.pkl"))
    if len(jobs) <= 1:
        _post_processing(
            output_dir,
            pose_estimation=False,
            association=True,
            smoothing=False,
            feasibility=False,
            num_threads=num_threads,
        )
    else:
        ray.init(num_cpus=config.num_threads)

        def prepare_config(p: Path):
            try:
                cfg = config.copy()
                cfg.output_dir = str(p.parent.absolute())
                video_config = OmegaConf.load(p.with_name("config.yaml")).video
                cfg.video = video_config
                associate(cfg, progress=False)
            except Exception:
                print(traceback.format_exc())

        with mp.Pool() as pool:
            it = pool.imap_unordered(prepare_config, jobs)
            for _ in tqdm(it, desc="Total progress", unit="file", total=len(jobs)):
                pass


@app.command()
def smoothing(
    output_dir: Path = typer.Argument(
        ..., help="Output directory for processed results"
    ),
    num_threads: int = typer.Option(1, help="Number of worker processes"),
    trajectory_output_format: TrajectoryFormat = typer.Option(
        TrajectoryFormat.parquet, help="Trajectory file format"
    ),
):
    """
    Perform smoothing stage
    """
    _post_processing(
        output_dir,
        pose_estimation=False,
        association=False,
        smoothing=True,
        feasibility=False,
        num_threads=num_threads,
        trajectory_output_format=trajectory_output_format,
    )


@app.command()
def feasibility(
    output_dir: Path = typer.Argument(
        ..., help="Output directory for processed results"
    ),
    num_threads: int = typer.Option(1, help="Number of worker processes"),
    trajectory_output_format: TrajectoryFormat = typer.Option(
        TrajectoryFormat.parquet, help="Trajectory file format"
    ),
):
    """
    Perform feasibility optimization stage
    """
    _post_processing(
        output_dir,
        pose_estimation=False,
        association=False,
        smoothing=False,
        feasibility=True,
        num_threads=num_threads,
        trajectory_output_format=trajectory_output_format,
    )


@app.command()
def post_processing(
    output_dir: Path = typer.Argument(
        ..., help="Input/Output directory for processed results"
    ),
    pose_estimation: bool = typer.Option(True, help="run pose estimation stage"),
    association: bool = typer.Option(True, help="run association stage"),
    smoothing: bool = typer.Option(True, help="run smoothing stage"),
    feasibility: bool = typer.Option(True, help="run feasibility stage"),
    num_threads: int = typer.Option(1, help="Number of worker processes"),
    trajectory_output_format: TrajectoryFormat = typer.Option(
        TrajectoryFormat.parquet, help="Trajectory file format"
    ),
):
    """
    Run pipeline processing steps except detection.
    """
    _post_processing(
        output_dir,
        pose_estimation,
        association,
        smoothing,
        feasibility,
        num_threads,
        trajectory_output_format,
    )


def _post_processing(
    output_dir: Path,
    pose_estimation: bool = True,
    association: bool = True,
    smoothing: bool = True,
    feasibility: bool = True,
    num_threads: int = 1,
    trajectory_output_format: TrajectoryFormat = TrajectoryFormat.parquet,
):
    """
    Run pipeline processing steps except detection.
    """
    assert output_dir.exists(), f"Directory does not exist! {output_dir.absolute()}"
    with hydra.initialize("../config"):
        base_config = hydra.compose("config")

    OmegaConf.set_struct(base_config, False)
    base_output_dir = output_dir
    base_config.trajectory_output_format = trajectory_output_format.value
    base_config.num_threads = num_threads
    ray.init(
        num_cpus=num_threads,
        local_mode=num_threads == 1,
        runtime_env={
            "env_vars": {
                "MKL_NUM_THREADS": "1",
                "NUMEXPR_NUM_THREADS": "1",
                "OMP_NUM_THREADS": "1",
            }
        },
    )
    files = list(base_output_dir.rglob("config.yaml"))
    for config_path in tqdm(files, desc="Total progress", unit="file"):
        config = OmegaConf.load(config_path)
        base_config.video = config.video
        config = base_config
        video_path = config.video.path
        config.video.path = str((config_path.parent / video_path).absolute())

        config.output_dir = str(config_path.parent)
        logger.info("Processing directory: %s", config_path.parent.absolute())

        # Pose estimation
        if pose_estimation:
            poses = estimate_pose(config)
        else:
            poses = None

        # Association
        if association:
            pose_tracks = associate(config, poses)
        else:
            pose_tracks = None

        # Smoothing
        if smoothing:
            trajectories = smooth(config, pose_tracks)
        else:
            trajectories = None

        if feasibility:
            optimize_feasibility(config, trajectories)

        # Restore original relative path
        config.video.path = video_path


@app.command()
def reference_evaluation(
    output_dir: Path = typer.Argument(
        ..., help="Input/Output directory for processed results"
    ),
    trajectory_format: TrajectoryFormat = typer.Option(
        TrajectoryFormat.parquet, help="Trajectory file format"
    ),
):
    """
    Evaluate extracted trajectories of reference drives
    """
    with hydra.initialize("../config"):
        config = hydra.compose("evaluation")
    config.output_dir = str(output_dir)
    config.trajectory_output_format = trajectory_format.value
    mona.evaluation.reference_evaluation(config)


if __name__ == "__main__":
    app()

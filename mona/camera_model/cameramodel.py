import importlib.resources
import logging
import os
from pathlib import Path
from typing import Any, Optional, Tuple, Union, Sequence

import cv2
import hydra.utils
import numpy as np
import pandas as pd
import ruamel.yaml.comments
import scipy.optimize as opt
from omegaconf import DictConfig
from osgeo import gdal
from ruamel.yaml import YAML
from scipy.interpolate import LinearNDInterpolator

from mona.utils.util import read_bz2_pickle, write_bz2_pickle

logger = logging.getLogger(__name__)


def _display_points(
    image: np.ndarray,
    points: np.ndarray,
    color: Tuple[int, int, int] = (0, 0, 255),
    marker: int = cv2.MARKER_CROSS,
) -> np.ndarray:
    for pt in points:
        image = cv2.drawMarker(image, pt.astype(int), color, marker, 2)
    return image


class CameraModel:
    """Pinhole camera model."""

    def __init__(
        self,
        rotation_matrix: Any,
        translation_vector: Any,
        cam_matrix: Any,
        origin: Optional[Sequence[float]] = None,
        camera_position: Optional[Sequence[float]] = None,
    ):
        """Create a camera model.

        :param rotation_matrix: Extrinsic rotation matrix
        :param translation_vector: Extrinsic translation vector
        :param cam_matrix: Intrinsic camera matrix
        :param origin: Geo location of origin of the world coordinate system
        :param camera_position: Camera position in UTM coordinates
        """
        self.rotation_matrix = np.array(rotation_matrix).reshape((3, 3))
        self.translation_vector = np.array(translation_vector).reshape(3, 1)
        self.cam_matrix = np.array(cam_matrix).reshape(3, 3)
        # Pre-computed combined projection matrix
        self.proj_matrix = (
            cam_matrix
            @ np.concatenate((self.rotation_matrix, self.translation_vector), axis=-1)
        ).T
        self.cam_matrix_inv = np.linalg.inv(self.cam_matrix)
        self.origin = origin
        if camera_position is None:
            self.camera_position = self.calculate_camera_position()
        else:
            self.camera_position = camera_position - np.append(self.origin, [0])

    def calculate_camera_position(self) -> np.ndarray:
        """Calculate the camera position in the world frame.

        Position is obtained from extrinsic camera parameters.

        :return: np.array([x, y, z]) position of the camera in world coordinates.
        """
        return (-self.rotation_matrix.T @ self.translation_vector).ravel()

    @classmethod
    def create_from_config(
        cls, config: DictConfig, location: Optional[str] = None
    ) -> "CameraModel":
        """Create a camera model from a configuration.

        If location is specified, creates the camera model accordingly. Otherwise,
        the location is inferred from config.video.location.

        :param config: Configuration object
        :param location: Optional location string indexing to config.locations
        :return: camera model
        """
        location_name = location or config.video.location

        loc = config.locations[location_name]

        cam = loc.camera
        cam_model = hydra.utils.instantiate(cam)
        return cam_model

    @classmethod
    def calibrate(
        cls,
        calib_points: pd.DataFrame,
        image_size: Tuple[int, int],
        origin: np.ndarray = np.zeros(2),
    ) -> "CameraModel":
        """Create a calibrated camera model.

        Calibration is performed by matching correspondences of pixel coordinates and
        world coordinates.

        :param calib_points: pandas.Dataframe with columns pixel_x, pixel_y , pos_x,
            pos_y
        :param image_size: Tuple (width, height) of the camera image in pixels
        :param origin: Origin of the world coordinates in UTM coordinates
        :return: Calibrated camera model
        """
        assert (
            "pixel_x" in calib_points.columns
            and "pixel_y" in calib_points.columns
            and "pos_x" in calib_points.columns
            and "pos_y" in calib_points.columns
        )
        assert len(calib_points.index) >= 4
        calib_points["pos_z"] = 0.0
        image_pos = calib_points[["pixel_x", "pixel_y"]].values.astype(np.float32)
        world_pos = calib_points[["pos_x", "pos_y", "pos_z"]].values.astype(np.float32)

        # Get an initial guess for the focal length (implemented as by Clausse)
        # Unclear, why this works better in some cases. Maybe poses constraint on
        # camera matrix
        def calibrate_with_focal(
            focal_length: float,
            im_size: Tuple[int, int],
            image_points: np.ndarray,
            world_points: np.ndarray,
        ) -> Tuple[int, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
            center = (im_size[0] / 2, im_size[1] / 2)
            camera_matrix = np.array(
                [[focal_length, 0, center[0]], [0, focal_length, center[1]], [0, 0, 1]],
                dtype=np.float64,
            )
            return cv2.calibrateCamera(
                [world_points.astype(np.float32)],
                [image_points.astype(np.float32)],
                im_size[:2],
                camera_matrix,
                np.zeros(4),
                flags=cv2.CALIB_USE_INTRINSIC_GUESS
                | cv2.CALIB_FIX_K1
                | cv2.CALIB_FIX_K2
                | cv2.CALIB_FIX_K3
                | cv2.CALIB_FIX_TANGENT_DIST,
            )

        cons = {"type": "ineq", "fun": lambda x: x[0]}
        param = opt.minimize(
            lambda *p: calibrate_with_focal(p[0][0], *p[1:])[0],
            np.array([image_size[1]]),
            constraints=cons,
            args=(image_size, image_pos, world_pos),
        )
        focal_length = param.x[0]

        ret, cam_matrix, _, rot_vec, trans = calibrate_with_focal(
            focal_length, image_size, image_pos, world_pos
        )
        logger.info("Re-projection RMSE: %.3f pixel", ret)
        rot_mat = np.array(cv2.Rodrigues(rot_vec[0])[0])
        cam_model = cls(rot_mat, np.array(trans), np.array(cam_matrix), origin)
        back_projected = cam_model.backproject_pixels(image_pos)
        back_project_error = np.linalg.norm(back_projected - world_pos, axis=-1)
        logger.info("Back-projection MAE: %.3f m", back_project_error.mean())
        logger.info(
            "Estimated camera location: %.4f, %.4f, elevation: %.2f",
            *(cam_model.camera_position[:2]),
            cam_model.camera_position[2],
        )
        return cam_model

    def show_calibration(
        self, image: np.ndarray, aerial_image: np.ndarray, calib_points: pd.DataFrame
    ) -> Tuple[np.ndarray, np.ndarray]:
        """Display the calibration result on the camera image and the aerial image.

        :param image: Camera image as NxMx3 BGR array
        :param aerial_image: Aerial image as N'xM'x3 BGR array
        :param calib_points: pandas.Dataframe with
            columns pixel_x, pixel_y, pos_x, pos_y
        :return: Annotated camera image and aerial image
        """
        world_points = calib_points[["pos_x", "pos_y"]].values
        image_points = calib_points[["pixel_x", "pixel_y"]].values
        reproj_points = self.project_points(world_points)
        image = _display_points(image, image_points)
        image = _display_points(
            image, reproj_points, color=(0, 255, 0), marker=cv2.MARKER_SQUARE
        )
        reproj_rms = np.sqrt(np.mean(np.square(image_points - reproj_points)))
        logger.info("Overall re-projection RMS error: %.3f pixel", reproj_rms)
        backproj_points = self.backproject_pixels(image_points)
        world_points[:, 0] /= 0.2
        world_points[:, 1] /= -0.2
        backproj_points[:, 0] /= 0.2
        backproj_points[:, 1] /= -0.2
        aerial_image = _display_points(aerial_image, world_points)
        aerial_image = _display_points(aerial_image, backproj_points[:, :-1])
        return image, aerial_image

    def save_to_yml(self, output_path: Union[str, Path]) -> None:
        """Save the camera parameters to a yaml file.

        :param output_path: Path to the output file.
        """

        def flow_list(arr: np.ndarray) -> ruamel.yaml.comments.CommentedSeq:
            sequence = ruamel.yaml.comments.CommentedSeq(arr.tolist())
            sequence.fa.set_flow_style()
            return sequence

        conf = {
            "_target_": "mona.camera_model.CameraModel",
            "rotation_matrix": flow_list(self.rotation_matrix),
            "translation_vector": flow_list(self.translation_vector),
            "cam_matrix": flow_list(self.cam_matrix),
            "origin": flow_list(np.array(self.origin)),
        }
        output_path = Path(output_path)
        YAML().dump(conf, output_path)
        logger.info("Camera config file saved %s", output_path.absolute())

    def display_coordinate_frame(self, image: np.ndarray) -> np.ndarray:
        """Draws the coordinate frame in the center of the image.

        :param image: Input image
        :return: Modified image
        """
        length = 5.0
        # Origin axis on image
        origin = np.array(image.shape)[:2] * 0.5
        # row, col -> col, row
        pt_origin = origin[::-1].astype(int)
        origin_world = self.backproject_pixels(pt_origin)
        pt_x = self.project_points(
            np.array([(length, 0.0, 0.0)]) + origin_world,
        )
        pt_y = self.project_points(
            np.array([(0.0, length, 0.0)]) + origin_world,
        )
        pt_z = self.project_points(
            np.array([(0.0, 0.0, length)]) + origin_world,
        )

        pt_o = pt_origin.tolist()
        pt_x = pt_x.tolist()
        pt_y = pt_y.tolist()
        pt_z = pt_z.tolist()

        # Add line of axis
        cv2.line(image, pt_o, pt_x, (255, 0, 0), 2)
        cv2.line(image, pt_o, pt_y, (0, 255, 0), 2)
        cv2.line(image, pt_o, pt_z, (0, 0, 255), 2)

        return image

    def get_ground_height(self, points: np.ndarray) -> np.ndarray:
        """Return the ground height (z component) for given x, y.

        :param points: A (N, 2) or (2,) array of world coordinates
        :return: (N,) array of ground elevation.
        """
        return np.squeeze(np.zeros((points.reshape(-1, 2).shape[0])))

    def project_points(self, world_points: np.ndarray) -> np.ndarray:
        """Project 3D world coordinates to image pixels.

        .. note::
            Multiple points can be efficiently converted by providing a [N, 3] matrix.

        :param world_points: 3D points in world coordinates
        :return: Integer pixel coordinates
        """
        # Assume z=0 if only two coordinates provided
        if len(world_points.shape) == 2 and world_points.shape[1] == 2:
            world_points = np.append(
                world_points, np.zeros((world_points.shape[0], 1)), axis=-1
            )
        world_points = np.reshape(world_points, (-1, 3))
        world_points = np.append(
            world_points, np.ones((world_points.shape[0], 1)), axis=-1
        )
        pixels = world_points @ self.proj_matrix
        pixels /= pixels[:, -1].reshape(-1, 1)
        pixels = np.squeeze(pixels[:, :-1])
        pixels = np.around(pixels).astype(int)
        return pixels

    def backproject_pixels(
        self, pixel_xy: np.ndarray, z_constraint: Union[np.ndarray, float] = 0
    ) -> np.ndarray:
        """Restore 3D coordinates from pixel coordinates with known height (z-axis).

        .. note::
            Multiple points can be efficiently converted by providing a [N, 2] matrix.

        :param pixel_xy: Integer pixel coordinates
        :param z_constraint: Height/z value of the resulting world coordinates.
        :return: 3D world coordinates.
        """
        cam_pos = self.calculate_camera_position()
        pixel_xy = pixel_xy.reshape((-1, 2))
        pixels = np.append(pixel_xy, np.ones((pixel_xy.shape[0], 1)), axis=-1)
        directions = np.linalg.multi_dot(
            (pixels, self.cam_matrix_inv.T, self.rotation_matrix)
        )
        line_params = (z_constraint - cam_pos[2]) / directions[:, 2]
        world_xyz = cam_pos + line_params[:, np.newaxis] * directions
        return np.squeeze(world_xyz)

    def project_covariance(
        self, mean: np.ndarray, cov: np.ndarray
    ) -> Tuple[np.ndarray, np.ndarray]:
        """Project a 3D covariance matrix in the world frame to the image frame.

        This uses a `first-order approximation <https://stackoverflow.com/questions/49239473/how-to-project-3d-covariance-matrix-to-a-given-image-plane-pose>`_  # noqa: E501
        for the camera model.

        :param mean: 3D mean vector
        :param cov: 3x3 covariance matrix
        :return: Projected 2D mean and covariance matrix (2x2)
        """
        cam_coords = mean @ self.rotation_matrix.T + self.translation_vector.T[0]
        jac = (
            self.cam_matrix[:2, :2]
            @ np.array(
                [
                    [1 / cam_coords[2], 0, -cam_coords[0] / (cam_coords[2] ** 2)],
                    [0, 1 / cam_coords[2], -cam_coords[1] / (cam_coords[2] ** 2)],
                ]
            )
            @ self.rotation_matrix
        )
        return self.project_points(mean), np.linalg.multi_dot((jac, cov, jac.T))


class TerrainCameraModel(CameraModel):
    """Camera model with integrated terrain model for non-planar target areas."""

    def _propagate_terrain_model(
        self, terrain_model_name: str, image_size: Tuple[int, int]
    ) -> Tuple[LinearNDInterpolator, np.ndarray]:
        cache_path = Path.home() / ".cache" / "mona" / "dgm" / terrain_model_name
        cache_path = cache_path.with_name(f"{cache_path.stem}.pkl")
        if cache_path.exists():
            return read_bz2_pickle(cache_path)
        with importlib.resources.path("mona.dgm", terrain_model_name) as dgm_path:
            terrain_model = pd.read_csv(
                dgm_path,
                sep=" ",
                names=["easting", "northing", "elevation"],
            )
        terrain_model.easting = terrain_model.easting - self.origin[0]
        terrain_model.northing = terrain_model.northing - self.origin[1]
        terrain_model.elevation = terrain_model.elevation - 505.0
        pixel_points = super().project_points(terrain_model.values)
        elevations = terrain_model.elevation.values
        elevations = elevations[
            (pixel_points[:, 0] >= 0)
            & (pixel_points[:, 1] >= 0)
            & (pixel_points[:, 0] < image_size[1])
            & (pixel_points[:, 1] < image_size[0])
        ]
        pixel_points = pixel_points[
            (pixel_points[:, 0] >= 0)
            & (pixel_points[:, 1] >= 0)
            & (pixel_points[:, 0] < image_size[1])
            & (pixel_points[:, 1] < image_size[0])
        ]
        rows = np.arange(start=0, stop=image_size[0])
        cols = np.arange(start=0, stop=image_size[1])
        # If multiple elevations for the same image pixel were found, we take the mean.
        df = pd.DataFrame(
            np.concatenate((pixel_points, elevations.reshape(-1, 1)), axis=-1),
            columns=["u", "v", "z"],
        )
        d = df.groupby(["u", "v"])["z"].mean().reset_index()
        # Ensure that every pixel has a corresponding elevation by interpolation
        grid = np.meshgrid(cols, rows)
        interpolator = LinearNDInterpolator(
            d[["u", "v"]].values, d.z.values, fill_value=0.0
        )
        elevation_map = interpolator(grid[0].ravel(), grid[1].ravel()).reshape(
            image_size
        )

        terrain_model = LinearNDInterpolator(
            terrain_model[["easting", "northing"]].values,
            terrain_model.elevation.values,
            fill_value=0.0,
        )
        os.makedirs(cache_path.parent, exist_ok=True)
        write_bz2_pickle(cache_path, (terrain_model, elevation_map))
        return terrain_model, elevation_map

    @classmethod
    def calibrate_terrain(
        cls,
        calib_points: pd.DataFrame,
        image_size: Tuple[int, int],
        terrain_model_name: str,
        origin: np.ndarray = np.zeros(2),
    ) -> CameraModel:
        """Calibrate a camera model using a terrain model to obtain ground heights.

        :param calib_points: pandas.Dataframe with columns
            pixel_x, pixel_y, pos_x, pos_y
        :param image_size: Tuple (width, height) of the camera image in pixels
        :param terrain_model_name: Name of the terrain model in `mona.dgm`
        :param origin: Origin of the world coordinates in UTM coordinates
        :return: Calibrated camera model
        """
        assert (
            "pixel_x" in calib_points.columns
            and "pixel_y" in calib_points.columns
            and "pos_x" in calib_points.columns
            and "pos_y" in calib_points.columns
            and "pos_z" in calib_points.columns
        )
        image_pos = calib_points[["pixel_x", "pixel_y"]].values.astype(np.float32)
        world_pos = calib_points[["pos_x", "pos_y", "pos_z"]].values.astype(np.float32)
        # Intrinsic guess has to be provided for non-planar calibration target
        # Filter out non-planar points
        planar_points = calib_points[calib_points.pos_z.abs() < 0.5].copy()
        intrinsic_guess = CameraModel.calibrate(planar_points, image_size, origin)
        back_projected = intrinsic_guess.backproject_pixels(image_pos.astype(int))
        back_project_error = np.linalg.norm(back_projected - world_pos, axis=-1)
        logger.info(
            "Overall back-projection MAE with planar model: %.3f m",
            back_project_error.mean(),
        )
        # Overall re-projection RMS
        reprojected = intrinsic_guess.project_points(world_pos)
        reproj_rms = np.sqrt(np.mean(np.square(image_pos - reprojected)))
        logger.info(
            "Overall re-projection RMS error with planar model: %.3f pixel", reproj_rms
        )
        if len(calib_points.index) <= 6:
            return intrinsic_guess
        cam_matrix_guess = intrinsic_guess.cam_matrix
        ret, cam_matrix, _, rot_vec, trans = cv2.calibrateCamera(
            [world_pos],
            [image_pos],
            image_size,
            cameraMatrix=cam_matrix_guess,
            distCoeffs=np.zeros(4),
            flags=cv2.CALIB_USE_INTRINSIC_GUESS
            | cv2.CALIB_FIX_K1
            | cv2.CALIB_FIX_K2
            | cv2.CALIB_FIX_K3
            | cv2.RANSAC
            | cv2.CALIB_FIX_TANGENT_DIST,
        )
        logger.info("Re-projection RMS with terrain model: %.3f pixel", ret)
        rot_mat = np.array(cv2.Rodrigues(rot_vec[0])[0])
        cam_model = cls(
            rot_mat,
            np.array(trans),
            np.array(cam_matrix),
            terrain_model_name,
            image_size,
            origin,
        )
        back_projected = cam_model.backproject_pixels(image_pos)
        back_project_error = np.linalg.norm(back_projected - world_pos, axis=-1)
        logger.info(
            "Back-projection error with terrain model: %.3f m",
            back_project_error.mean(),
        )
        logger.info(
            "Estimated camera location with terrain model: %.4f, %.4f, elevation: "
            "%.2f",
            *(cam_model.camera_position),
        )
        return cam_model

    def __init__(
        self,
        rotation_matrix: Any,
        translation_vector: Any,
        cam_matrix: Any,
        terrain_model_name: str,
        image_size: Tuple[int, int],
        origin: Optional[Tuple[float, float]] = None,
        camera_position: Optional[Tuple[float, float, float]] = None,
    ):
        """Create a camera model.

        :param rotation_matrix: Extrinsic rotation matrix
        :param translation_vector: Extrinsic translation matrix
        :param cam_matrix: Intrinsic camera matrix
        :param terrain_model_name: Name of a terrain model stored at `mona.dgm`
        :param origin: Geo location of origin of the world coordinate system
        :param camera_position: (Optional) Camera position in UTM coordinates.
            If not provided, will be inferred from the camera calibration.
        """
        super().__init__(
            rotation_matrix, translation_vector, cam_matrix, origin, camera_position
        )
        self.terrain_model_name = terrain_model_name
        self.terrain_model, self.pixel_elevations = self._propagate_terrain_model(
            terrain_model_name, image_size
        )

    def save_to_yml(self, output_path: Union[str, Path]) -> None:
        """Save camera calibration data to a YAML file.

        :param output_path: Output file path
        """
        super().save_to_yml(output_path)
        # Only add the terrain model path and image size
        output_path = Path(output_path)
        conf = YAML().load(output_path)
        conf["_target_"] = "mona.camera_model.TerrainCameraModel"
        conf["image_size"] = self.pixel_elevations.shape
        conf["terrain_model_name"] = self.terrain_model_name
        YAML().dump(conf, output_path)

    def get_ground_height(self, points: np.ndarray) -> np.ndarray:
        """Return the ground height (z component) for given x, y.

        :param points: A (N, 2) or (2,) array of world coordinates
        :return: (N,) array of ground elevation.
        """
        return self.terrain_model(points)

    def backproject_pixels(
        self, pixel_xy: np.ndarray, z_constraint: Union[float, np.ndarray] = 0.0
    ) -> np.ndarray:
        """Restore 3D coordinates from pixel coordinates with known height (z-axis).

        .. note::
            Multiple points can be efficiently converted by providing a [N, 2] matrix.

        :param pixel_xy: Integer pixel coordinates
        :param z_constraint: Height/z value of the resulting world coordinates.
        :return: 3D world coordinates.
        """
        pixel_xy = pixel_xy.reshape((-1, 2))
        pixel_xy = pixel_xy.astype(int)
        pixel_xy[:, 0] = np.clip(pixel_xy[:, 0], 0, self.image_size[1] - 1)
        pixel_xy[:, 1] = np.clip(pixel_xy[:, 1], 0, self.image_size[0] - 1)
        elevations = self.pixel_elevations[pixel_xy[:, 1], pixel_xy[:, 0]]
        elevations = np.nan_to_num(elevations)
        elevations += z_constraint
        back_projected = super().backproject_pixels(pixel_xy, elevations)
        back_projected[..., 2] = z_constraint
        return back_projected

    @property
    def image_size(self) -> Tuple[int, int]:
        """Get the image size of the camera image.

        :return: (width, height)
        """
        return self.pixel_elevations.shape

    def project_points(self, world_points: np.ndarray) -> np.ndarray:
        """Project 3D world coordinates to image pixels.

        .. note::
            Multiple points can be efficiently converted by providing a [N, 3] matrix.

        :param world_points: 3D points in world coordinates
        :return: Integer pixel coordinates
        """
        world_points = world_points.copy()
        if len(world_points.shape) == 2 and world_points.shape[1] == 2:
            world_points = np.append(
                world_points, np.zeros((world_points.shape[0], 1)), axis=-1
            )
        world_points = np.reshape(world_points, (-1, 3))
        world_points[:, 2] += self.get_ground_height(world_points[:, :2])
        return super().project_points(world_points)


class OrthoCameraModel:
    """Camera model for orthographic images."""

    def __init__(
        self,
        ortographic_path: Path,
        world_origin: Optional[np.ndarray] = None,
        crop: Optional[np.ndarray] = None,
    ):
        """
        Create camera model.

        :param ortographic_path: Path to the orthographic image
        :param world_origin: UTM coordinates of the frame world origin
        :param crop: Top left corner of the orthographic image in pixel coordinates,
            if the image is cropped.
        """
        assert ortographic_path.exists(), str(ortographic_path)
        sat_img = str(ortographic_path)
        ds = gdal.Open(sat_img)
        trans = ds.GetGeoTransform()
        self.world_origin = world_origin if world_origin is not None else np.zeros(2)
        self.origin = np.array([trans[0], trans[3]])
        self.crop = crop if crop is not None else np.zeros(2)
        self.scale = np.array([[trans[1], trans[5]]])

    def project_points(self, world_points: np.ndarray) -> np.ndarray:
        """Project world coordinates to image pixels.

        .. note::
            Multiple points can be efficiently converted by providing a [N, 2/3] matrix.

        :param world_points: 2D/3D points in world coordinates
        :return: Integer pixel coordinates
        """
        if len(world_points.shape) == 1:
            world_points = world_points.reshape(1, -1)
        else:
            n = world_points.shape[1]
            world_points = world_points.reshape((-1, n))
        points = world_points[:, :2]
        points += self.world_origin
        points -= self.origin
        points /= self.scale
        points -= self.crop
        points = np.round(points).astype(int)
        return np.squeeze(points)

    def backproject_pixels(self, pixels: np.ndarray) -> np.ndarray:
        """Restore ground coordinates from pixel coordinates.

        .. note::
            Multiple points can be efficiently converted by providing a [N, 2] matrix.

        :param pixels: Integer pixel coordinates
        :return: 3D world coordinates.
        """
        return (pixels + self.crop) * self.scale + self.origin - self.world_origin

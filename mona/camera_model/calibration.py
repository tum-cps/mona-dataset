import pickle
from pathlib import Path
from typing import Tuple

import cv2
import numpy as np
import pandas as pd
from omegaconf import DictConfig
from scipy.interpolate import LinearNDInterpolator

from mona.camera_model import CameraModel


def get_calibration_points_interactive(
    config: DictConfig,
    cam_img: np.ndarray,
    sat_img: np.ndarray,
    trans: np.ndarray,
    with_terrain: bool,
) -> Tuple[CameraModel, pd.DataFrame]:
    """
    Calibrate the camera model interactively.

    :param config: Configuration dictionary
    :param cam_img: Image taken from the camera
    :param sat_img: Orthographic aerial image
    :param trans: GeoTIFF transformation of the aerial image
    :param with_terrain: Calibrate with terrain model
    :return: Calibrated camera model and DataFrame of calibration points
    """
    origin = [trans[0], trans[3]]
    location = config.video.location
    if with_terrain:
        # Read terrain model
        get_elevation_path = Path("get_elevation.pkl")
        if not get_elevation_path.exists():
            terrain_model = pd.read_csv(
                config.calib.terrain_model_name,
                sep=" ",
                names=["easting", "northing", "elevation"],
            )
            terrain_model.easting = terrain_model.easting - origin[0]
            terrain_model.northing = terrain_model.northing - origin[1]
            terrain_model.elevation = terrain_model.elevation - 505.0
            get_elevation = LinearNDInterpolator(
                terrain_model[["easting", "northing"]].values,
                terrain_model.elevation,
            )
            with get_elevation_path.open("wb") as f:
                pickle.dump(get_elevation, f)
        else:
            with get_elevation_path.open("rb") as f:
                get_elevation = pickle.load(f)
    else:
        get_elevation = lambda _, __: 0  # noqa: E731
    print(
        "Select corresponding points in both images by clicking\n",
        "press space bar for each point to confirm\n",
        "press q to finish and execute calibration!",
    )
    cam_model = [None]

    def mouse_callback(event, x, y, _, param):
        coords, img, win_name, other_img = param
        if event == cv2.EVENT_LBUTTONDOWN:
            coords[0] = x
            coords[1] = y
            print(x, y)
            img = cv2.drawMarker(img.copy(), (x, y), (0, 255, 0), cv2.MARKER_CROSS)
            cv2.imshow(win_name, img)
        elif cam_model[0] is not None:
            if win_name == "Sat":
                pix = cam_model[0].project_points(
                    np.array([x, y, 0.0]) * [trans[1], trans[5], 0.0]
                )
                img_vid = cv2.drawMarker(
                    other_img.copy(), pix.tolist(), (255, 0, 0), cv2.MARKER_CROSS
                )
                cv2.imshow("Cam", img_vid)
            else:
                world = cam_model[0].backproject_pixels(np.array([x, y]))[:-1] / [
                    trans[1],
                    trans[5],
                ]
                img_sat = cv2.drawMarker(
                    other_img.copy(),
                    world.astype(int).tolist(),
                    (255, 0, 0),
                    cv2.MARKER_CROSS,
                )
                cv2.imshow("Sat", img_sat)

    cv2.namedWindow("Cam", cv2.WINDOW_NORMAL)
    cv2.namedWindow("Sat", cv2.WINDOW_NORMAL)
    pix_sat = [0, 0]
    pix_cam = [0, 0]
    cv2.setMouseCallback("Sat", mouse_callback, (pix_sat, sat_img, "Sat", cam_img))
    cv2.setMouseCallback("Cam", mouse_callback, (pix_cam, cam_img, "Cam", sat_img))
    cv2.imshow("Sat", sat_img)
    cv2.imshow("Cam", cam_img)
    points = []
    while True:
        key = cv2.waitKey(0) & 0xFF

        if key == ord("q"):
            break
        points.append(pix_cam + pix_sat)

        if len(points) >= 4:
            df = pd.DataFrame(points, columns=["pixel_x", "pixel_y", "pos_x", "pos_y"])
            # Adjust to spatial resolution. Resulting unit is meter.
            df[["pos_x", "pos_y"]] = df[["pos_x", "pos_y"]].values * [
                trans[1],
                trans[5],
            ]
            df["pos_z"] = get_elevation(df.pos_x.values, df.pos_y.values)
            df.to_csv(f"camera_calib_{location}.csv", index=False)
            cam_model[0] = CameraModel.calibrate(df, cam_img.shape[:2], origin)

        sat_img = cv2.drawMarker(sat_img, pix_sat, (0, 0, 255), cv2.MARKER_CROSS)
        cam_img = cv2.drawMarker(cam_img, pix_cam, (0, 0, 255), cv2.MARKER_CROSS)

        cv2.imshow("Sat", sat_img)
        cv2.imshow("Cam", cam_img)
    cv2.destroyWindow("Sat")
    cv2.destroyWindow("Cam")
    cam_model = cam_model[0]
    return cam_model, df

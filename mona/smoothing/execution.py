import copy
import logging
from pathlib import Path
from typing import Dict, List

import hydra.utils
import numpy as np
import pandas as pd

import ray
from omegaconf import DictConfig
from ruamel.yaml import YAML
from tqdm import tqdm

from mona.association import Tracklet
from mona.pose_estimation.pose import PoseTrack
from mona.pose_estimation.orientation_estimation import MapOrientationEstimator
from mona.serialization.trajectory_serializer import InteractionTrajectorySerializer
from mona.smoothing.smoothing import ModelBasedRtsSmoother, DummySmoother
from mona.utils.filtering import CompoundFilter
from mona.utils.util import read_trajectories, read_bz2_pickle, get_git_commit

logger = logging.getLogger(__name__)


@ray.remote
class SmootherActor:
    def __init__(self, config, debug):
        dt = 1.0 / config.video.fps
        self.smoother = ModelBasedRtsSmoother(
            dt=dt,
            num_init_states=config.smoothing.min_num_states,
            debug=debug,
            config=config.smoothing.kf_model,
            orientation_estimator=MapOrientationEstimator.create_from_config(config),
        )
        self.dsmoother = DummySmoother(
            dt=dt, num_init_states=config.smoothing.min_num_states
        )

    def get_metrics(self):
        return {
            "none_pose_count": self.smoother.none_pose_count,
            "dropped_tracks_count": self.smoother.dropped_tracks_count,
            "auc_kappa_min_vio": self.smoother.auc_kappa_min_vio,
            "auc_kappa_max_vio": self.smoother.auc_kappa_max_vio,
            "auc_acc_min_vio": self.smoother.auc_acc_min_vio,
            "auc_acc_max_vio": self.smoother.auc_acc_max_vio,
            "distance_positions": self.smoother.distance_positions,
        }

    def smooth(self, pose_track: PoseTrack):
        return self.smoother.smooth(pose_track), self.dsmoother.smooth(pose_track)


def smooth(config, tracks=None):
    output_dir = Path(config["output_dir"])
    trajectories_path = output_dir / f"trajectories.{config.trajectory_output_format}"
    if trajectories_path.exists() and config.get("skip_existing", False):
        logger.info(
            f"Found previous results {trajectories_path.absolute()}. Skipping "
            f"smoothing!"
        )
        return read_trajectories(trajectories_path)
    if tracks is None:
        tracks = read_bz2_pickle(output_dir / "tracks.pkl")
    if len(tracks) == 0:
        return
    debug = False

    pose_tracks = assemble_pose_tracks(config, output_dir, tracks)

    num_threads = config.num_threads
    actors = [SmootherActor.remote(config, debug) for _ in range(num_threads)]
    pool = ray.util.ActorPool(actors)

    it = pool.map_unordered(lambda a, v: a.smooth.remote(v), pose_tracks.values())
    trajectories, dtrajectories = zip(
        *filter(
            lambda t: t[0] is not None and t[1] is not None,
            tqdm(it, total=len(pose_tracks), desc="Smoothing", unit="track"),
        )
    )

    dserializer = InteractionTrajectorySerializer(dtrajectories)
    dserializer.save(output_dir / f"trajectories_raw.{config.trajectory_output_format}")

    filters = CompoundFilter(
        [hydra.utils.instantiate(cfg) for cfg in config.filtering.smoothing]
    )
    trajectories = filters(trajectories)

    differentiated_trajectories = [copy.deepcopy(t) for t in trajectories]
    [t.differentiate() for t in differentiated_trajectories]
    rmse_v_err = 0
    rmse_theta_err = 0
    for idx in range(len(trajectories)):
        rmse_v_err += np.sqrt(
            (
                (
                    trajectories[idx].traj_points["v"]
                    - differentiated_trajectories[idx].traj_points["v"]
                )
                ** 2
            ).mean()
        )
        rmse_theta_err += np.sqrt(
            (
                (
                    trajectories[idx].traj_points["psi_rad"]
                    - differentiated_trajectories[idx].traj_points["psi_rad"]
                )
                ** 2
            ).mean()
        )

    serializer = InteractionTrajectorySerializer(trajectories)
    serializer.save(trajectories_path)

    metrics_df: pd.DataFrame = pd.DataFrame(
        [ray.get(a.get_metrics.remote()) for a in actors]
    )
    metrics_df = metrics_df.sum()
    metrics = {
        "git_commit": get_git_commit(),
        "num_trajectories": len(trajectories),
        "num_pose_tracks": len(pose_tracks),
        "percent_filtered": (1.0 - len(trajectories) / len(pose_tracks)) * 100.0,
    }
    metrics.update(metrics_df.to_dict())
    logging.info("Metrics: %s", metrics)
    YAML().dump(metrics, output_dir / "smoothing_metrics.yaml")
    return serializer.df


def refine_position(pose_track: PoseTrack):
    for p in pose_track.poses.values():
        if p.heading_orthogonal is None:
            # Pose not created by L-shape matching
            return
        direction_vector = np.array([np.cos(p.psi), np.sin(p.psi)])
        orthogonal = np.array(
            [np.cos(p.heading_orthogonal), np.sin(p.heading_orthogonal)]
        )
        xy = (
            direction_vector * pose_track.box_size.length
            + orthogonal * pose_track.box_size.width
        ) * 0.5 + p.tracked_corner
        p.x = float(xy[0])
        p.y = float(xy[1])


def assemble_pose_tracks(
    config: DictConfig, output_dir: Path, tracks: List[Tracklet]
) -> Dict[int, PoseTrack]:
    poses = read_bz2_pickle(output_dir / "poses.pkl")
    # Create pose tracks from tracks and poses
    pose_tracks = {
        t.track_id: PoseTrack(
            t.track_id,
            {
                frame_idx: poses[frame_idx][det_id]
                for frame_idx, det_id in t.detections.items()
                if frame_idx in poses and det_id in poses[frame_idx]
            },
            t.agent_type,
        )
        for t in tracks
    }
    orientation_estimator = MapOrientationEstimator.create_from_config(config)
    for track in pose_tracks.values():
        track.k_closest_box_size(config.location.camera.camera_position[:2])
        refine_position(track)
        for p in track.poses.values():
            p.psi = orientation_estimator.correct_orientation(
                np.array([p.x, p.y]), p.psi
            )
    return pose_tracks

from dataclasses import dataclass
from typing import ClassVar, List

import numpy as np
import pandas as pd

from mona.detection.detection import DetectionLabel
from mona.pose_estimation.pose import PoseTrack


@dataclass
class Trajectory:
    """Vehicle trajectory representation as a time-discrete sequence of states."""

    traj_points: np.recarray
    length: float
    width: float
    traj_id: int
    vehicle_type: DetectionLabel = DetectionLabel.car
    columns: ClassVar[List[str]] = [
        "frame_id",
        "agent_type",
        "x",
        "y",
        "v",
        "psi_rad",
        "yaw_rate",
        "length",
        "width",
        "timestamp_sec",
    ]

    @classmethod
    def create_trajectory(
        cls,
        track: PoseTrack,
        x: np.ndarray,
        y: np.ndarray,
        speed: np.ndarray,
        theta: np.ndarray,
        timestamp: np.ndarray,
        yaw_rate: np.ndarray,
    ) -> "Trajectory":
        """Create a trajectory from separate state component arrays."""
        time_steps = np.arange(0, stop=len(x)) + track.start_time_step
        stacked_states = list(zip(x, y, speed, theta, timestamp, time_steps, yaw_rate))
        states = np.core.records.fromrecords(
            stacked_states,
            names=["x", "y", "v", "psi_rad", "timestamp_sec", "frame_id", "yaw_rate"],
        )
        trajectory = cls(
            states,
            track.box_size.length,
            track.box_size.width,
            track.track_id,
            track.vehicle_type,
        )
        return trajectory

    def differentiate(self) -> None:
        """Calculate and store velocity and orientation by finite differences."""
        positions = np.array(self.traj_points[["x", "y"]].tolist())
        v = np.divide(
            (positions[1:] - positions[:-1]).transpose(),
            np.diff(self.traj_points["timestamp_sec"]),
        )
        v = v.transpose()
        self.traj_points["v"][1:] = np.linalg.norm(v, axis=-1)
        self.traj_points["psi_rad"][1:] = np.arctan2(v[:, 1], v[:, 0])

    @property
    def dataframe(self) -> pd.DataFrame:
        """Return vehicle trajectory as a pandas dataframe."""
        d = [
            (
                p.frame_id,
                self.vehicle_type.name,
                p.x,
                p.y,
                p.v,
                p.psi_rad,
                p.yaw_rate,
                self.length,
                self.width,
                p.timestamp_sec,
            )
            for p in self.traj_points
        ]
        return pd.DataFrame(d, columns=self.columns)

    @property
    def start_time_step(self) -> int:
        """Return the start time step of the trajectory."""
        return self.traj_points.time_step[0]

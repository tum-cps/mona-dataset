import logging

import filterpy.kalman
import numpy as np
from abc import abstractmethod, ABCMeta

logger = logging.getLogger(__name__)


class BaseFilter(filterpy.kalman.ExtendedKalmanFilter, metaclass=ABCMeta):
    """Base Extended Kalman Filter class.

    Extends ExtendedKalmanFilter class of
    filterpy package.
    """

    def __init__(self, dt, dim_x, dim_z):
        """Initialize BaseFilter class.

        :param dt: time increment of input data
        :param dim_x: dimension of state vector
        :param dim_z: dimension of measurement vector
        """
        super().__init__(dim_x=dim_x, dim_z=dim_z)
        self.dt = dt

    @abstractmethod
    def init_x(self, pos_x: float, pos_y: float, theta: float, vel: float) -> None:
        """Initializes state."""
        raise NotImplementedError()

    @abstractmethod
    def calculate_F_jacobian_at(self, x: np.ndarray) -> np.ndarray:
        """Calculates Jacobian F = df/dx of discrete state transition function f.

        x[k+1] = f(x[k])
        """
        raise NotImplementedError()

    @abstractmethod
    def calculate_H_jacobian_at(self, x: np.ndarray) -> np.ndarray:
        """Returns Jacobian dh/dx of the h matrix (measurement function)."""
        raise NotImplementedError()

    @abstractmethod
    def calculate_hx(self, x: np.ndarray) -> np.ndarray:
        """Maps state variable to corresponding measurement z."""
        raise NotImplementedError()

    @abstractmethod
    def propagate_x(self, x: np.ndarray) -> np.ndarray:
        """Predicts the next state of X."""
        raise NotImplementedError()

    @abstractmethod
    def get_pos_x_center(self, x):
        """Getter that retrieves x-position of model's center from state x."""
        raise NotImplementedError()

    @abstractmethod
    def get_pos_y_center(self, x):
        """Getter that retrieves y-position of model's center from state x."""
        raise NotImplementedError()

    @staticmethod
    @abstractmethod
    def get_vel(x):
        """Getter that retrieves velocity from state x."""
        raise NotImplementedError()

    @staticmethod
    @abstractmethod
    def get_theta(x):
        """Getter that retrieves orientation from state x."""
        raise NotImplementedError()

    def update_from_measurement(self, z, measurement_noise=None):
        """Updates the kalman filter state matrices from measurement z."""
        self.update(
            z, self.calculate_H_jacobian_at, self.calculate_hx, R=measurement_noise
        )

    def predict_x(self, u=0):
        """Predicts the next state of X."""
        self.F = self.calculate_F_jacobian_at(self.x)
        x_next = self.propagate_x(self.x)
        self.x = x_next

    def rts_smoother(self, Xs, Ps, inv=np.linalg.inv):
        """Runs the Rauch-Tung-Striebel Kalman smoother.

        Smoother runs on a set of means and covariances computed by EKF.

        Parameters
        ----------
        Xs : numpy.array
           array of the means (state variable x) of the output of a Kalman
           filter.

        Ps : numpy.array
            array of the covariances of the output of a kalman filter.

        inv : function, default numpy.linalg.inv
            If you prefer another inverse function, such as the Moore-Penrose
            pseudo inverse, set it to that instead: kf.inv = np.linalg.pinv

        Returns
        -------
        x : numpy.ndarray
           smoothed means

        G : numpy.ndarray
           smoothed state covariances

        K : numpy.ndarray
            smoother gain at each step

        Pp : numpy.ndarray
           Predicted state covariances

        """
        if len(Xs) != len(Ps):
            raise ValueError("length of Xs and Ps must be the same")

        n = Xs.shape[0]
        dim_x = Xs.shape[1]

        # smoother gain
        G = np.zeros((n, dim_x, dim_x))

        x, xp, P, Pp = Xs.copy(), Xs.copy(), Ps.copy(), Ps.copy()
        for k in range(n - 2, -1, -1):
            F = self.calculate_F_jacobian_at(x[k])
            # predicted mean
            # xp[k+1] = np.dot(F, x[k])
            xp[k + 1] = self.propagate_x(x[k])  # nonlinear equation
            # predicted covariance
            Pp[k + 1] = np.dot(np.dot(F, P[k]), F.T) + self.Q

            # Gain matrix
            G[k] = np.dot(np.dot(P[k], F.T), inv(Pp[k + 1]))
            # smoothed mean
            x[k] = x[k] + np.dot(G[k], x[k + 1] - xp[k + 1])
            # smoothed covariance
            P[k] = P[k] + np.dot(np.dot(G[k], P[k + 1] - Pp[k + 1]), G[k].T)

        return x, P, G, Pp

    def calculate_H_jacobian_pseudo(self, x: np.ndarray):
        """Calculate the Jacobian of the measurement matrix for a pseudo measurement.

        :param x: State vector
        """
        pass

    def calculate_hx_pseudo(self, x: np.ndarray):
        """Project state x into the pseudo measurement space.

        :param x: State vector
        """
        pass

    def update_pseudo_measurement(
        self, v_pseudo: float = 0.0, yaw_rate_pseudo: float = 0.0
    ):
        """Perform a pseudo measurement update to attenuate yaw noise.

        :param v_pseudo: Velocity of the pseudo measurement
        :param yaw_rate_pseudo: Yaw rate of the pseudo measurement
        """
        logger.warning("Pseudo measurement not implemented!")

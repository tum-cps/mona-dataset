import logging
import os
from abc import abstractmethod, ABC
from dataclasses import dataclass
from pathlib import Path
from typing import List, Optional

import hydra.utils
import matplotlib.pyplot as plt
import numpy as np
from omegaconf import DictConfig

from mona.pose_estimation.orientation_estimation import MapOrientationEstimator
from mona.pose_estimation.pose import PoseTrack
from mona.utils.util import _draw_trajectory
from mona.smoothing.common import (
    get_speed_from_position,
    get_acc_from_positions,
    get_theta_from_position,
    get_kappa_from_position,
)
from mona.smoothing.base_filter import BaseFilter
from mona.smoothing.trajectory import Trajectory

logger = logging.getLogger(__name__)


@dataclass
class VehicleLimits:
    """Limits for vehicle inputs."""

    acc_max: float = 4.0
    acc_min: float = -8.0
    kappa_max: float = 0.3
    kappa_min: float = -0.3


class BaseSmoother(ABC):
    """Base smoother class that processes pose track."""

    def __init__(
        self,
        dt: float,
        num_init_states: int,
        debug: bool = False,
        output_dir: str = "/tmp/",
    ):
        """Initialize BaseSmoother class.

        :param dt: time increment of input data
        :param num_init_states: number of states the track must have to not be dropped
        :param debug: flag that will trigger debug plots
        :param output_dir: output directory for debug plots
        """
        self.dt = dt
        self.min_num_states = num_init_states
        self.debug = debug
        self.output_dir = output_dir
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        self.vehicle_limits = VehicleLimits()
        self.none_pose_count = 0
        self.dropped_tracks_count = 0
        self.auc_kappa_min_vio = 0
        self.auc_kappa_max_vio = 0
        self.auc_acc_min_vio = 0
        self.auc_acc_max_vio = 0
        self.distance_positions = 0

    @abstractmethod
    def smooth(self, track: PoseTrack) -> Optional[Trajectory]:
        """Smooth respective track and return the smoothed trajectory.

        :param track: Track to be smoothed
        """
        raise NotImplementedError()

    def process_all_tracks(self, pose_tracks: List[PoseTrack]) -> List[Trajectory]:
        """Smooth all tracks.

        :param pose_tracks: List of tracks to be smoothed
        """
        trajectories = []
        for t in pose_tracks:
            if len(t) < self.min_num_states:
                logger.debug(
                    "Track {} is too short, skipping smoothing".format(t.track_id)
                )
                smoothed_traj = None
            else:
                smoothed_traj = self.smooth(t)
            if smoothed_traj is not None:
                trajectories.append(smoothed_traj)
            else:
                logger.debug("Smoothing of track returned nothing")
                self.dropped_tracks_count += 1

        return trajectories

    def debug_plots(
        self, track: PoseTrack, pos_filtered: np.ndarray, pos_smoothed: np.ndarray
    ) -> None:
        """Plot debug information for raw, filtered, and smoothed trajectory."""
        if self.debug:
            path = Path(self.output_dir)
            os.makedirs(path, exist_ok=True)
            f, axs = plt.subplots(6, figsize=(8, 8), sharex="none")
            f.suptitle(f"Trajectory {track.track_id}")
            f.tight_layout()
            pos_raw = track.positions
            traj_raw = np.column_stack(
                (
                    *pos_raw.T,
                    get_speed_from_position(pos_raw, self.dt),
                    get_theta_from_position(pos_raw, self.dt),
                    np.zeros_like(pos_raw[:, 0]),
                )
            )
            _draw_trajectory(axs, traj_raw, "raw")
            _draw_trajectory(axs, pos_filtered, "filtered")
            _draw_trajectory(axs, pos_smoothed, "smoothed")
            f.savefig(path / f"trajectory_{track.track_id}.png", bbox_inches="tight")
            plt.close(f)


class DummySmoother(BaseSmoother):
    """Dummy smoother which does not perform any smoothing."""

    def smooth(self, track: PoseTrack) -> Optional[Trajectory]:
        """Return the track as a trajectory.

        :param track: Track to be smoothed
        """
        if len(track) < 2:
            return None
        speed = get_speed_from_position(track.positions, self.dt)
        theta = get_theta_from_position(track.positions, self.dt)
        start_time_step = track.start_time_step
        pose_0 = track.poses[start_time_step]
        time_stamps = np.arange(0.0, speed.shape[0]) * self.dt + pose_0.time
        traj = Trajectory.create_trajectory(
            track,
            *track.positions.T,
            speed,
            theta,
            time_stamps,
            np.zeros(speed.shape[0]),
        )
        return traj


class ModelBasedRtsSmoother(BaseSmoother):
    """Model-based Rauch-Tung-Striebal (RTS) Smoother.

    Extends BaseSmoother class.
    """

    def __init__(
        self,
        dt: float,
        num_init_states: int,
        config: DictConfig,
        orientation_estimator: MapOrientationEstimator,
        debug: bool = False,
        output_dir: str = "/tmp/smoother",
    ):
        """Initialize ModelBasedRtsSmoother class.

        :param dt: time increment of input data
        :param num_init_states: number of states the track must have to not be dropped
        :param debug: flag that will trigger debug plots
        :param output_dir: output directory for debug plots
        """
        super().__init__(
            dt=dt, num_init_states=num_init_states, debug=debug, output_dir=output_dir
        )
        self.config = config
        self.orientation_estimator = orientation_estimator

    def create_and_initialize_filter(self, track: PoseTrack) -> BaseFilter:
        """Create a filter object and initialize it.

        :param track: Track to be smoothed
        """
        it = iter(track.poses.items())

        idx_0, pose_0 = next(it)
        init_states = np.array(
            [
                [i, p.x, p.y]
                for _, (i, p) in zip(range(self.min_num_states), track.poses.items())
            ]
        )
        velocities = np.diff(init_states, axis=0)
        velocities = velocities[:, 1:] / velocities[:, 0:1] / self.dt
        v = np.mean(velocities, axis=0)

        l_r = track.box_size.length * 0.2  # estimate for distance from center to
        # rear axis
        l2w = track.box_size.length / track.box_size.width
        w2l = 1 / l2w
        logger.debug(
            "id: {}, box length: {:.2f}, box width: {:.2f}, l2w: {:.2f}, "
            "w2l: {:.2f}, l_r: {:.2f}".format(
                track.track_id,
                track.box_size.length,
                track.box_size.width,
                l2w,
                w2l,
                l_r,
            )
        )

        # Use orientation from map or pose for initialization
        # Use mean of init states for map, as single states might be out of
        # projection domain.
        orientation = self.orientation_estimator.get_lane_orientation(
            init_states.mean(axis=0)[1:]
        )
        orientation = orientation or pose_0.psi
        filter_obj = hydra.utils.instantiate(self.config, dt=self.dt, l_r=l_r)
        filter_obj.init_x(
            pos_x=pose_0.x,
            pos_y=pose_0.y,
            theta=orientation,
            vel=np.linalg.norm(v, axis=-1),
        )
        return filter_obj

    def smooth(self, track: PoseTrack) -> Optional[Trajectory]:
        """Smooth respective track and return the smoothed trajectory.

        :param track: Track to be smoothed
        """
        if len(track) < self.min_num_states:
            logger.debug(
                "Track {} is too short, skipping smoothing".format(track.track_id)
            )
            return None
        filter_object = self.create_and_initialize_filter(track=track)

        time_step_iter = iter(range(track.start_time_step, track.end_time_step + 1))
        idx_0 = next(time_step_iter)
        pose_0 = track.poses[idx_0]

        xs = [filter_object.x]
        ps = [filter_object.P]
        for k in time_step_iter:
            filter_object.predict()
            pose = track.poses.get(k)
            if pose is not None:
                z = [pose.x, pose.y]
                shape_deviation_rmse = np.linalg.norm(
                    [
                        pose.box.length - track.box_size.length,
                        pose.box.width - track.box_size.width,
                    ]
                )
                scaling = 1 + shape_deviation_rmse
                filter_object.update_from_measurement(z, filter_object.R * scaling)
                filter_object.update_pseudo_measurement()
            else:
                self.none_pose_count += 1
            xs.append(filter_object.x)
            ps.append(filter_object.P)

        xs_filtered = np.array(xs)
        cov_filtered = np.array(ps)
        xs_smoothed, _, _, _ = filter_object.rts_smoother(xs_filtered, cov_filtered)

        # transform smoothed values to center
        pos_x = filter_object.get_pos_x_center(xs_smoothed)
        pos_y = filter_object.get_pos_y_center(xs_smoothed)
        speed = filter_object.get_vel(xs_smoothed)
        theta = filter_object.get_theta(xs_smoothed)
        # Normalize angle to [-pi, -pi]
        theta = np.arctan2(np.sin(theta), np.cos(theta))
        time_stamps = (
            np.arange(0.0, xs_smoothed.shape[0]) * filter_object.dt + pose_0.time
        )
        yaw_rate = xs_smoothed[:, -1]
        traj = Trajectory.create_trajectory(
            track, pos_x, pos_y, speed, theta, time_stamps, yaw_rate
        )

        # transform filtered values to center
        xs_filtered[:, 0] = filter_object.get_pos_x_center(xs_filtered)
        xs_filtered[:, 1] = filter_object.get_pos_y_center(xs_filtered)

        self.debug_plots(
            track, xs_filtered, np.column_stack((pos_x, pos_y, speed, theta, yaw_rate))
        )

        # calculate metrics
        pos_smoothed = np.column_stack((pos_x, pos_y))
        kappa = get_kappa_from_position(pos_smoothed, self.dt)
        kappa_n = kappa - self.vehicle_limits.acc_min
        kappa_n[kappa_n > 0] = 0
        kappa_p = kappa - self.vehicle_limits.acc_max
        kappa_p[kappa_p < 0] = 0
        acc = get_acc_from_positions(pos_smoothed, self.dt)
        acc_n = acc - self.vehicle_limits.acc_min
        acc_n[acc_n > 0] = 0
        acc_p = acc - self.vehicle_limits.acc_max
        acc_p[acc_p < 0] = 0
        self.auc_kappa_min_vio += np.sum(kappa_n * self.dt)
        self.auc_kappa_max_vio += np.sum(kappa_p * self.dt)
        self.auc_acc_min_vio += np.sum(acc_n * self.dt)
        self.auc_acc_max_vio += np.sum(acc_p * self.dt)
        self.distance_positions += np.sum(
            np.linalg.norm(pos_smoothed - track.positions, axis=-1)
        )
        return traj

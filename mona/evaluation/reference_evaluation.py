from pathlib import Path

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from omegaconf import OmegaConf
from ruamel.yaml import YAML
from shapely.geometry import LineString, Point
from tqdm import tqdm

from mona.utils.util import read_trajectories, angle_difference, _draw_trajectory


def _track_error(track_df):
    df = track_df
    cc_coords = []
    ref_vertices = df[["x_ref", "y_ref"]].values
    # extrapolate start and end
    extra_start = ref_vertices[0] + (
        ref_vertices[0] - ref_vertices[1]
    ) * 2 / np.linalg.norm(ref_vertices[0] - ref_vertices[1])
    ref_vertices = np.insert(ref_vertices, 0, extra_start, axis=0)
    extra_end = ref_vertices[-1] + (
        ref_vertices[-1] - ref_vertices[-2]
    ) * 2 / np.linalg.norm(ref_vertices[-1] - ref_vertices[-2])
    ref_vertices = np.append(ref_vertices, extra_end.reshape((1, 2)), axis=0)
    ref_path = LineString(ref_vertices.tolist())

    for p, p_ref in zip(
        df[["x", "y"]].values.reshape(-1, 2, 1),
        df[["x_ref", "y_ref"]].values.reshape(-1, 2, 1),
    ):
        p = Point(*p)
        p_ref = Point(*p_ref)
        s = ref_path.project(p)
        s_ref = ref_path.project(p_ref)
        d = ref_path.distance(p)
        d_ref = ref_path.distance(p_ref)
        cc_coords.append([s, d, s_ref, d_ref])

    cc_coords = np.array(cc_coords)
    df[["s", "d", "s_ref", "d_ref"]] = cc_coords
    df = df.copy()
    df["s_err"] = (df.s - df.s_ref).abs()
    df["d_err"] = df.d.abs()
    df.dropna(inplace=True)
    return {
        "longitudinal_error": float(df.s_err.mean()),
        "longitudinal_rmse": float((df.s_err**2).mean() ** 0.5),
        "lateral_error": float(df.d_err.mean()),
        "lateral_rmse": float((df.d_err**2).mean() ** 0.5),
    }


def _draw_trajectories(path, traj, original=None, labels=None):
    if labels is None:
        labels = [None] * (len(traj) if original is None else len(traj) + 1)
    f, axs = plt.subplots(
        6, 2 if original is not None else 1, figsize=(8, 16), sharex="none"
    )
    f.suptitle(f"{path.parent.name}")
    f.tight_layout()
    if original is not None:
        orig = original[["x", "y", "v", "psi_rad", "yaw_rate"]].values
        for d, l in zip(traj, labels):
            df = d[["x", "y", "v", "psi_rad", "yaw_rate"]].values
            error = np.abs(orig - df)
            axs[0, 1].plot(np.linalg.norm(error[:, :2], axis=1))
            axs[0, 1].set_ylabel("error")
            for i, ax in enumerate(axs[1:, 1]):
                ax.set_xlabel("t [s]")
                ax.plot(error[:, i], label=l)
                ax.set_ylabel("error")
        traj = [original] + traj

    for t, l in zip(traj, labels):
        df = t[["x", "y", "v", "psi_rad", "yaw_rate"]].values
        _draw_trajectory(axs[:, 0], df, l)

    f.savefig(path, bbox_inches="tight")
    plt.close(f)


def _calculate_metrics(assumed_fortuna_df):
    displacement = assumed_fortuna_df.dist.mean()
    displacement_rmse = (assumed_fortuna_df.dist**2).mean() ** 0.5
    v_rmse = (assumed_fortuna_df.vel_diff**2).mean() ** 0.5
    psi_rmse = (assumed_fortuna_df.psi_diff**2).mean() ** 0.5
    width_error = assumed_fortuna_df.width - assumed_fortuna_df.width_ref
    length_error = assumed_fortuna_df.length - assumed_fortuna_df.length_ref
    width_rmse = (width_error**2).mean() ** 0.5
    length_rmse = (length_error**2).mean() ** 0.5
    width_mape = (width_error / assumed_fortuna_df.width_ref).abs().mean()
    length_mape = (length_error / assumed_fortuna_df.length_ref).abs().mean()
    metrics = {
        "mean_displacement_error": float(displacement),
        "displacement_rmse": float(displacement_rmse),
        "velocity_rmse": float(v_rmse),
        "orientation_rmse": float(psi_rmse),
        "length_error": float(length_error.abs().mean()),
        "width_error": float(width_error.abs().mean()),
        "width_rmse": float(width_rmse),
        "length_rmse": float(length_rmse),
        "width_mape": float(width_mape),
        "length_mape": float(length_mape),
    }
    metrics.update(_track_error(assumed_fortuna_df))
    return metrics


def reference_evaluation(config):
    """Compare extracted trajectories with the reference drives of fortuna."""
    output_dir = Path(config["output_dir"])
    OmegaConf.set_struct(config, False)
    traj_files = list(
        output_dir.rglob(f"trajectories.{config.trajectory_output_format}")
    )
    records = []
    for p in tqdm(traj_files):
        traj_df = read_trajectories(p)
        traj_df.set_index("frame_id", inplace=True)
        vid_config = OmegaConf.load(p.with_name("config.yaml"))
        config.video = vid_config.video
        video_path = Path(vid_config.video.path)
        ref_df = pd.read_csv(
            p.parent
            / video_path.with_name(f"{video_path.stem}_fortuna_localization.csv")
        )
        ref_df.set_index("frame_id", inplace=True)
        # Merge with reference data
        df = traj_df.join(
            ref_df[["x", "y", "v", "psi_rad", "yaw_rate", "width", "length"]],
            rsuffix="_ref",
            how="inner",
        )
        # Calculate mean displacement error by track id
        df["dist"] = np.linalg.norm(
            df[["x", "y"]].values - df[["x_ref", "y_ref"]].values, axis=-1
        )
        df["vel_diff"] = df.v.values - df.v_ref.values
        df["psi_diff"] = angle_difference(df.psi_rad.values, df.psi_rad_ref.values)
        mde = df.groupby(["track_id"]).mean()["dist"]
        # Get track id with minimum error
        track_id = mde.idxmin()

        track_df = df[df.track_id == track_id].copy()

        metrics = _calculate_metrics(track_df)
        track_df.to_csv(p.with_name("displacement.csv"))

        metrics["reference_track"] = int(track_id)
        YAML().dump(metrics, p.with_name("displacement.yaml"))
        records.append(metrics)

        # Compare feasibility results
        feas_df = read_trajectories(
            p.with_name(f"feasible_trajectories.{config.trajectory_output_format}")
        )
        feas_df = feas_df[feas_df.track_id == track_id]
        if len(feas_df.index) > 0:
            feas_df.set_index("frame_id", inplace=True, drop=False)
            feas_df = feas_df.join(
                ref_df[["x", "y", "v", "psi_rad", "width", "length"]],
                rsuffix="_ref",
                how="inner",
            )
            feas_df["dist"] = np.linalg.norm(
                feas_df[["x", "y"]].values - feas_df[["x_ref", "y_ref"]].values, axis=-1
            )
            feas_df["vel_diff"] = feas_df.v.values - feas_df.v_ref.values
            feas_df["psi_diff"] = angle_difference(
                feas_df.psi_rad.values, feas_df.psi_rad_ref.values
            )
            feas_metrics = _calculate_metrics(feas_df)
            _draw_trajectories(
                p.with_name("evaluation.png"),
                [track_df.loc[feas_df.index], feas_df],
                ref_df.loc[feas_df.index],
                ["ground truth", "smoothed", "feasible"],
            )
            YAML().dump(feas_metrics, p.with_name("feasibility_displacement.yaml"))
        else:
            _draw_trajectories(
                p.with_name("evaluation.png"),
                [track_df],
                ref_df.loc[track_df.index],
                ["ground truth", "smoothed"],
            )
            YAML().dump({}, p.with_name("feasibility_displacement.yaml"))

    if len(records) > 1:
        df = pd.DataFrame(records)
        mean_dict = df.mean().to_frame().to_dict()[0]
        print(mean_dict)
        YAML().dump(mean_dict, output_dir / "reference_evaluation.yaml")
    elif len(records) > 0:
        print(records[0])

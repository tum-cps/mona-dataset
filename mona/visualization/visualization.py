from pathlib import Path
from typing import List, Union

import cv2
import hydra
import numpy as np
import pandas as pd
from omegaconf import DictConfig
from tqdm import tqdm

from mona.camera_model import CameraModel
from mona.serialization.detection_serializer import Hdf5Deserializer
from mona.serialization.video_dataset import VideoDataset
from mona.utils.filtering import CompoundFilter, DetectionZone
from mona.utils.util import (
    load_config_from_output,
    get_colormap,
    draw_time_stamp,
    read_bz2_pickle,
    Colormap,
)
from .writers import VideoSaver, ImageSaver


def visualize_detections(config: DictConfig) -> None:
    """Visualize instances of :py:class:`.Detection` in a video recording."""
    output_dir = Path(config["output_dir"])
    config = load_config_from_output(config, output_dir)
    output_file = output_dir / "detections.mkv"
    det_path = output_dir / "detections.hd5"
    filters = [hydra.utils.instantiate(cfg) for cfg in config.filtering.pose_estimation]
    compound_filter = CompoundFilter(filters)
    deserializer = Hdf5Deserializer(det_path, filter_fn=compound_filter)
    ds = VideoDataset.from_config(config)
    video_writer = VideoSaver(output_file, ds)
    color_map = get_colormap(500)
    detection_zone = DetectionZone.create_from_config(config)
    with deserializer as f:
        with video_writer as writer:
            for frame_dict in tqdm(ds, desc="Visualize detections", unit="frame"):
                image = frame_dict["image"]
                if config.visualization.draw_detection_zone:
                    detection_zone.display_on_image(image, thickness=2)
                if config.visualization.draw_timestamp:
                    draw_time_stamp(frame_dict, image)
                frame_idx = frame_dict["frame_index"]
                detections = f.deserialize_frame_dict(frame_idx)
                for detection in detections.values():
                    detection.display_on_image(
                        image, color_map(detection.det_id), no_mask=True
                    )
                writer.write(image)


def visualize_pose(config: DictConfig) -> None:
    """Visualize instances of :py:class:`.Pose` in a video recording."""
    output_dir = Path(config["output_dir"])
    config = load_config_from_output(config)
    output_file = output_dir / "poses.mkv"
    pose_path = output_dir / "poses.pkl"
    poses = read_bz2_pickle(pose_path)
    ds = VideoDataset.from_config(config)
    video_writer = VideoSaver(output_file, ds)
    cam_model = CameraModel.create_from_config(config)
    color_map = get_colormap(500)
    detection_zone = DetectionZone.create_from_config(config)
    with video_writer as writer:
        for frame_dict in tqdm(ds, desc="Visualize poses", unit="frames"):
            image = frame_dict["image"]
            image = cam_model.display_coordinate_frame(image)
            if config.visualization.draw_detection_zone:
                detection_zone.display_on_image(image, thickness=2)
            if config.visualization.draw_timestamp:
                draw_time_stamp(frame_dict, image)
            frame_idx = frame_dict["frame_index"]
            frame_poses = poses.get(frame_idx)
            if frame_poses is None:
                continue
            for i, pose in enumerate(frame_poses.values()):
                pose.display_on_image(image, cam_model, color_map(i))
            writer.write(image)


def visualize_tracks(config: DictConfig) -> None:
    """Visualize instances of :py:class:`.Tracklet` in a video recording."""
    output_dir = Path(config["output_dir"])
    config = load_config_from_output(config)
    tracks_path = output_dir / "tracks.pkl"
    output_file = output_dir / "tracks.mkv"
    det_path = output_dir / "detections.hd5"
    filters = [hydra.utils.instantiate(cfg) for cfg in config.filtering.pose_estimation]
    compound_filter = CompoundFilter(filters)
    deserializer = Hdf5Deserializer(det_path, filter_fn=compound_filter)
    ds = VideoDataset.from_config(config)
    tracks = read_bz2_pickle(tracks_path)
    video_writer = VideoSaver(output_file, ds)
    detection_zone = DetectionZone.create_from_config(config)
    color_map = get_colormap(max(map(lambda x: x.track_id, tracks)))
    with deserializer as f:
        with video_writer as writer:
            for frame_dict in tqdm(ds, desc="Visualize tracks", unit="frames"):
                image = frame_dict["image"]
                if config.visualization.draw_detection_zone:
                    detection_zone.display_on_image(image, thickness=2)
                if config.visualization.draw_timestamp:
                    draw_time_stamp(frame_dict, image)
                frame_idx = frame_dict["frame_index"]
                detections = f.deserialize_frame_dict(frame_idx)
                for track in tracks:
                    det_id = track.detections.get(frame_idx)
                    if det_id is not None and det_id in detections:
                        detection = detections[det_id]
                        detection.display_on_image(
                            image,
                            color_map(track.track_id),
                            custom_text=f"D: {detection.det_id} T: {track.track_id}",
                            no_mask=True,
                        )
                writer.write(image)


def _draw_traj_point(
    track_point: pd.DataFrame,
    image: np.ndarray,
    cam_model: CameraModel,
    cmap: Colormap,
    marker_type: int = cv2.MARKER_CROSS,
) -> None:
    points = np.array(
        [
            [0.5, +0.5],
            [0.5, -0.5],
            [-0.5, -0.5],
            [-0.5, +0.5],
        ]
    )
    points[:, 0] *= track_point.length
    points[:, 1] *= track_point.width
    points = np.append(points, np.full((points.shape[0], 1), 1.0), axis=-1)
    sin = np.sin(track_point.psi_rad)
    cos = np.cos(track_point.psi_rad)
    transform = np.eye(3)
    transform[0, 0] = cos
    transform[1, 1] = cos
    transform[0, 1] = -sin
    transform[1, 0] = sin
    transform[:2, 2] = track_point[["x", "y"]].values
    points = points @ transform.T
    points[:, 2] = 0.0
    pixel_points = np.array(cam_model.project_points(points))
    overlay = np.zeros_like(image)
    color = tuple([int(x) for x in cmap(track_point.track_id)])
    cv2.fillPoly(overlay, [pixel_points], color)
    cv2.addWeighted(image, 1.0, overlay, 0.5, 0.0, image)
    center_point = cam_model.project_points(
        np.array([track_point.x, track_point.y, 0.0])
    )
    cv2.drawMarker(image, center_point, (0, 0, 255), markerType=marker_type)
    cv2.putText(
        image,
        f"{track_point.track_id}",
        pixel_points[0],
        cv2.FONT_HERSHEY_SIMPLEX,
        2,
        (0, 0, 255),
        2,
    )


def _draw_shape_batch(
    track_points: pd.DataFrame,
    cam_model: CameraModel,
    image: np.ndarray,
    cmap: Colormap = lambda _: (0, 0, 255),
    track_id: bool = True,
    draw_marker: bool = True,
) -> np.ndarray:
    """Draw all shapes at once, which is faster."""
    transform = np.eye(3)
    poly_points: List[np.ndarray] = []
    full_overlay = np.zeros_like(image)
    current_frame = track_points[track_points.frame_id == track_points.frame_id.max()]

    for _, track_point in current_frame.iterrows():
        width = track_point.width
        length = track_point.length
        points = np.array(
            [
                [0.5 * length, +0.5 * width],
                [0.5 * length, -0.5 * width],
                [-0.5 * length, -0.5 * width],
                [-0.5 * length, +0.5 * width],
            ]
        )
        points = np.append(points, np.full((points.shape[0], 1), 1.0), axis=-1)
        sin = np.sin(track_point.psi_rad)
        cos = np.cos(track_point.psi_rad)
        transform[0, 0] = cos
        transform[1, 1] = cos
        transform[0, 1] = -sin
        transform[1, 0] = sin
        transform[:2, 2] = track_point[["x", "y"]].values
        points = points @ transform.T
        poly_points.append(points)
    if len(poly_points) == 0:
        return image
    np_poly_points = np.concatenate(poly_points)
    # Convert homSet z-coordinate
    np_poly_points[:, 2] = 0.0
    np_poly_points = cam_model.project_points(np_poly_points)
    centers = cam_model.project_points(current_frame[["x", "y"]].values).reshape(-1, 2)
    directions = cam_model.project_points(
        current_frame[["x", "y"]].values
        + np.column_stack(
            (
                2 * np.cos(current_frame.psi_rad.values),
                2 * np.sin(current_frame.psi_rad.values),
            )
        )
    )
    directions.shape = -1, 2
    poly_points = list(np_poly_points.reshape((-1, 4, 2)))
    overlay = np.zeros_like(image)

    # Draw tracks
    for t in track_points.track_id.unique():
        df = track_points[track_points.track_id == t]
        if len(df.index) < 2:
            continue
        uv = cam_model.project_points(df[["x", "y"]])
        full_overlay = cv2.polylines(full_overlay, [uv], False, cmap(t), thickness=2)

    # Draw current frame shapes
    for (_, t), v, c, d in zip(
        current_frame.iterrows(), poly_points, centers, directions
    ):
        if track_id:
            full_overlay = cv2.putText(
                full_overlay,
                f"{t.track_id}",
                v[0],
                cv2.FONT_HERSHEY_SIMPLEX,
                2.0,
                (0, 0, 255),
                2,
            )
        if draw_marker:
            full_overlay = cv2.arrowedLine(
                full_overlay, c, d, (0, 0, 255), 4, tipLength=0.3
            )
        overlay = cv2.fillPoly(overlay, [v], cmap(t.track_id))

    cv2.addWeighted(image, 1.0, overlay, 0.5, 0.0, image)
    if track_id or draw_marker:
        image = cv2.copyTo(
            full_overlay, np.any(full_overlay, axis=-1).astype(np.uint8), image
        )
    return image


def visualize_trajectories(
    config: DictConfig,
    trajs: pd.DataFrame,
    output_file: Union[str, Path, None] = None,
) -> None:
    """Visualize instances of :py:class:`.Trajectory` in a video recording."""
    video_path = Path(config.video.path)
    ref_df_path = video_path.with_name(f"{video_path.stem}_fortuna_localization.csv")
    if config.visualization.trajectories.fortuna and ref_df_path.exists():
        ref_df_loca = pd.read_csv(ref_df_path)
        ref_df_loca.set_index("frame_id", inplace=True)
    else:
        ref_df_loca = None

    perception_df_path = video_path.with_name(
        f"{video_path.stem}_fortuna_perception.csv"
    )
    if config.visualization.trajectories.perception and perception_df_path.exists():
        perception_df = pd.read_csv(perception_df_path)
        perception_df["psi_rad"] = np.arctan2(perception_df.vy, perception_df.vx)
    else:
        perception_df = None

    output_file = output_file or Path(config.output_dir) / "trajectories.mkv"
    output_file = Path(output_file)
    ds = VideoDataset.from_config(config)
    cam_model = CameraModel.create_from_config(config)
    if config.get("write_images", False):
        video_writer = ImageSaver(output_file.parent, config.output_dir.name)
    else:
        video_writer = VideoSaver(output_file, ds)
    cmap = get_colormap(trajs.track_id.max())
    detection_zone = DetectionZone.create_from_config(config)
    with video_writer as writer:
        for frame_dict in tqdm(ds, desc="Visualize trajectories", unit="frames"):
            image = frame_dict["image"]
            if config.visualization.draw_detection_zone:
                detection_zone.display_on_image(image, thickness=2)
            if config.visualization.draw_timestamp:
                draw_time_stamp(frame_dict, image)
            frame_idx = frame_dict["frame_index"]

            # Draw fortuna
            if ref_df_loca is not None:
                try:
                    ref_traj_point = ref_df_loca.loc[frame_idx]
                    _draw_traj_point(
                        ref_traj_point,
                        image,
                        cam_model,
                        lambda _: (0, 0, 255),
                        cv2.MARKER_STAR,
                    )
                except KeyError:
                    pass

            # Draw perception
            if perception_df is not None:
                image = _draw_shape_batch(
                    perception_df[perception_df.frame_id == frame_idx], cam_model, image
                )

            df_frame = trajs[trajs.frame_id == frame_idx]
            _draw_shape_batch(df_frame, cam_model, image, cmap)
            writer.write(image)

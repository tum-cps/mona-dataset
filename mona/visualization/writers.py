from contextlib import contextmanager
from pathlib import Path

import cv2
import numpy as np

from mona.serialization.video_dataset import VideoDataset


@contextmanager
def VideoSaver(output_file: Path, ds: VideoDataset):
    """Context-manager wrapper for `cv2.VideoWriter`."""
    fourcc = cv2.VideoWriter_fourcc(*"XVID")
    video_writer = cv2.VideoWriter(
        str(output_file.absolute()), fourcc, ds.fps, ds.frame_size
    )
    yield video_writer
    video_writer.release()


class ImageSaver:
    """Drop-in replacement for VideoWriter saving frames as images."""

    def __init__(self, output_dir: Path, name: str):
        """
        Create an ImageSaver.

        :param output_dir: Path to the image output directory
        :param name: Name prefix of the output images
        """
        assert output_dir.is_dir()
        self.output_dir = output_dir
        self.name = name
        self.counter = 0

    def __enter__(self):
        self.counter = 0
        return self

    def write(self, image: np.ndarray) -> None:
        """Write an image to disk."""
        path = self.output_dir / f"{self.name}_{self.counter}.jpg"
        cv2.imwrite(str(path.absolute()), image)
        self.counter += 1

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

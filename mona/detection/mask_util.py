from itertools import groupby
from typing import Tuple, Dict, Union, List

import cv2
import numba
import numpy as np
from pycocotools import mask as masktools
from pycocotools.mask import frPyObjects

from mona.detection.bounding_box import BoundingBox
from mona.utils.util import Color

"""
Various utility methods to work with encoded run-length encoded mask from the
pycocotools package.
"""

PyCocoMask = Dict[str, Union[Tuple[int, int], bytes]]


def _decode_counts(encoded: np.ndarray) -> np.ndarray:
    encoded = encoded - 48
    c_x_s = encoded & 0x1F
    c_more_s = encoded & 0x20
    c_sign_s = encoded & 0x10
    c_sign_s = np.where(~c_more_s.astype(bool) & c_sign_s.astype(bool), -1, 0x00)
    cnts = []
    x = 0
    k = 0
    for c_x, c_more, c_sign in zip(c_x_s, c_more_s, c_sign_s):
        x |= c_x << k
        k += 5
        if not c_more:
            x |= c_sign << k
            cnts.append(x)
            k = 0
            x = 0
    cnts_array = np.array(cnts)
    cnts_array[1::2] = np.cumsum(cnts_array[1::2])
    cnts_array[2::2] = np.cumsum(cnts_array[2::2])
    return cnts_array


def decode_counts(mask: PyCocoMask) -> np.ndarray:
    """Decode the counts of encoded RLE objects from pycoco tools.

    :param mask: Encoded pycoco mask object (dictionary with 'counts' and 'size' keys)
    :return: 1D numpy array of the decoded counts.
    """
    encoded = np.frombuffer(mask["counts"], dtype=np.uint8)
    cnts = _decode_counts(encoded)
    return cnts


def invert_mask(mask: PyCocoMask) -> PyCocoMask:
    """Inverts a binary RLE mask (prepends a zero to the counts).

    :param mask: Mask object to invert
    :return: The inverted encoded RLE mask.
    """
    cnts = decode_counts(mask).tolist()
    mask = {"counts": [0] + cnts, "size": mask["size"]}
    return frPyObjects(mask, *mask["size"])


def find_contours(mask: PyCocoMask) -> np.ndarray:
    """Find contours in an encoded RLE mask.

    :param mask: Encoded RLE mask.
    :return: Numpy array with shape [N, 2] containing the (unordered!) contour points.
    """
    # Decode LEB128 encoded counts
    cnts = decode_counts(mask)
    rows, cols = mask["size"]
    return _find_contours(cnts, rows)


@numba.njit
def _find_contours(cnts: np.ndarray, rows: int) -> np.ndarray:
    contours = []
    # Flattened index
    idx = 0
    # If the current run is a 0 run
    zero = True
    # Append two 0 columns to flush buffer
    for run_length in cnts:
        run_length = int(run_length)
        while run_length > 0:
            # Divide run into columns
            col = idx // rows
            # Row index of the beginning of the run
            row_run_begin = idx % rows
            # Row index of the end of the run. Don't run over row borders
            row_run_end = row_run_begin + np.fmin(run_length, rows - row_run_begin) - 1
            if not zero:
                contours.append((col, row_run_begin))
                if row_run_end != row_run_begin:
                    contours.append((col, row_run_end))
            col_run_length = row_run_end - row_run_begin + 1
            # Advance flattened index
            idx += col_run_length
            # Calculate remaining run length
            run_length -= col_run_length
        zero = not zero
    # Bring into OpenCV format
    return np.array(contours).reshape((-1, 1, 2))


def roi_mask_to_rle(
    roi_mask: np.ndarray,
    roi: BoundingBox,
    size: Tuple[int, int],
    threshold: float = 0.5,
) -> PyCocoMask:
    """Convert an unprocessed mask output by detectron2 into an RLE.

    Works without creating the full size mask.

    :param roi_mask: Floating point mask inside the ROI.
    :param roi: The bounding box of the region of interest.
    :param size: Size of the full-size image
    :param threshold: Threshold for the binary mask.
    :return: Encoded RLE mask
    """
    mask_shape = np.squeeze(roi_mask).shape
    # Scale the mask to the size of the ROI bounding box.
    mask = cv2.resize(
        roi_mask.reshape((mask_shape[0], mask_shape[1], 1)),
        (roi.width, roi.height),
        interpolation=cv2.INTER_CUBIC,
    )
    cnts = _roi_mask_counts(mask, roi.top_left, roi.bottom_right, size, threshold)
    encoded = frPyObjects({"counts": cnts, "size": size[::-1]}, *size[::-1])
    return encoded


@numba.njit
def _roi_mask_counts(
    mask: np.ndarray,
    tl: np.ndarray,
    br: np.ndarray,
    size: Tuple[int, int],
    threshold: float,
) -> List[int]:
    mask = (mask >= threshold).astype(np.uint8)
    cols = size[0]
    rows = size[1]
    x_1, y_1 = tl
    x_2, y_2 = br
    # Column major
    # Leading zeros until the ROI
    lead_zero = x_1 * rows + y_1
    # Trailing zeros after the ROI
    trail_zero = (cols - x_2 - 1) * rows + (rows - y_2 - 1)
    assert lead_zero >= 0
    assert trail_zero >= 0
    # Zeros between each column in the ROI
    crlf = rows - (y_2 - y_1 + 1)
    cnts = [lead_zero]
    zero = True
    for mask_col in range(0, mask.shape[1]):
        for mask_row in range(0, mask.shape[0]):
            v = mask[mask_row, mask_col]
            if zero ^ v:
                cnts[-1] += 1
            else:
                cnts.append(1)
                zero = not zero
        if zero:
            cnts[-1] += crlf
        elif crlf > 0:
            cnts.append(crlf)
            zero = True
    if not zero:
        cnts.append(0)  # zero = True
    cnts[-1] += trail_zero - crlf
    return cnts


def binary_mask_to_rle(binary_mask: np.ndarray) -> PyCocoMask:
    """Convert a full size binary mask into a RLE mask.

    :param binary_mask: 2D binary segmentation mask.
    :return: RLE mask  in COCO format.
    """
    counts = binary_mask_to_counts(binary_mask)
    rle = {
        "counts": np.array(counts, dtype=np.uint).tobytes(),
        "size": tuple(binary_mask.shape),
    }
    return rle


def binary_mask_to_counts(binary_mask: np.ndarray) -> List[int]:
    """Convert a binary mask to RLE counts.

    :param binary_mask: 2D binary segmentation mask
    :return: List of RLE counts starting with a zero run
    """
    counts = []
    for i, (value, elements) in enumerate(groupby(binary_mask.ravel(order="F"))):
        if i == 0 and value:
            counts.append(0)
        counts.append(len(list(elements)))
    return counts


def intersection_over_union_mask(
    det_1: "Detection", det_2: "Detection"  # noqa: F821
) -> float:
    """Calculate the IoU between two detections without inflating.

    :param det_1: First detection
    :param det_2: Second detection
    :return: IoU percentage
    """
    return masktools.iou([det_1.rle_mask], [det_2.rle_mask], [False])[0][0]


def intersection_area_mask(det_1: "Detection", det_2: "Detection") -> int:  # noqa: F821
    """Calculate the intersection area between two detections without inflating.

    :param det_1: First detection
    :param det_2: Second detection
    :return: Intersecting area in pixels
    """
    return masktools.area(
        masktools.merge((det_1.rle_mask, det_2.rle_mask), intersect=True)
    )


def draw_mask(mask: np.ndarray, color: Color, image: np.ndarray) -> None:
    """Draw an opaque mask onto an image.

    :param mask: 2D binary mask with same size as image.
    :param color: Mask color in BGR format (values in [0, 254].
    :param image: Image (modified in-place)
    """
    color_mask = np.zeros_like(image)
    color_mask[mask] = color
    cv2.addWeighted(image, 1.0, color_mask, 0.5, 0.0, image)

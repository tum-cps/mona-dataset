import enum
from typing import Optional, Union

import cv2
import numpy as np
import pycocotools.mask as masktools

from mona.utils.util import Color
from .bounding_box import BoundingBox, intersection_over_union_rect
from .mask_util import (
    find_contours,
    draw_mask,
    PyCocoMask,
    intersection_over_union_mask,
)


class DetectionLabel(enum.IntEnum):
    """Label of the class detected on the frame."""

    person = 0
    bicycle = enum.auto()
    car = enum.auto()
    motorcycle = enum.auto()
    airplane = enum.auto()
    bus = enum.auto()
    train = enum.auto()
    truck = enum.auto()
    boat = enum.auto()
    traffic_light = enum.auto()
    fire_hydrant = enum.auto()
    street_sign = enum.auto()
    stop_sign = enum.auto()
    parking_meter = enum.auto()
    bench = enum.auto()
    bird = enum.auto()
    cat = enum.auto()
    dog = enum.auto()
    horse = enum.auto()
    sheep = enum.auto()
    cow = enum.auto()
    elephant = enum.auto()
    bear = enum.auto()
    zebra = enum.auto()
    giraffe = enum.auto()
    hat = enum.auto()
    backpack = enum.auto()
    umbrella = enum.auto()
    shoe = enum.auto()
    eye_glasses = enum.auto()
    handbag = enum.auto()
    tie = enum.auto()
    suitcase = enum.auto()
    frisbee = enum.auto()
    skis = enum.auto()
    snowboard = enum.auto()
    sports_ball = enum.auto()
    kite = enum.auto()
    baseball_bat = enum.auto()
    baseball_glove = enum.auto()
    skateboard = enum.auto()
    surfboard = enum.auto()
    tennis_racket = enum.auto()
    bottle = enum.auto()
    plate = enum.auto()
    wine_glass = enum.auto()
    cup = enum.auto()
    fork = enum.auto()
    knife = enum.auto()
    spoon = enum.auto()
    bowl = enum.auto()
    banana = enum.auto()
    apple = enum.auto()
    sandwich = enum.auto()
    orange = enum.auto()
    broccoli = enum.auto()
    carrot = enum.auto()
    hot_dog = enum.auto()
    pizza = enum.auto()
    donut = enum.auto()
    cake = enum.auto()
    chair = enum.auto()
    couch = enum.auto()
    potted_plant = enum.auto()
    bed = enum.auto()
    mirror = enum.auto()
    dining_table = enum.auto()
    window = enum.auto()
    desk = enum.auto()
    toilet = enum.auto()
    door = enum.auto()
    tv = enum.auto()
    laptop = enum.auto()
    mouse = enum.auto()
    remote = enum.auto()
    keyboard = enum.auto()
    cell_phone = enum.auto()
    microwave = enum.auto()
    oven = enum.auto()
    toaster = enum.auto()
    sink = enum.auto()
    refrigerator = enum.auto()
    blender = enum.auto()
    book = enum.auto()
    clock = enum.auto()
    vase = enum.auto()
    scissors = enum.auto()
    teddy_bear = enum.auto()
    hair_drier = enum.auto()
    toothbrush = enum.auto()
    hair_brush = enum.auto()
    other = 999

    @classmethod
    def _missing_(cls, value: object) -> "DetectionLabel":
        return DetectionLabel.other


class Detection:
    """Object that holds a detection on an image."""

    def __init__(
        self,
        det_id: int,
        label: DetectionLabel,
        det_2Dbox: BoundingBox,
        confidence: float,
        det_mask: Optional[Union[np.ndarray, PyCocoMask]] = None,
        frame_id: Optional[int] = None,
        time: Optional[float] = None,
    ):
        """Create detection.

        :param det_id: Index of the detection. Unique within one frame.
        :param label: Class of the detection e.g. car, truck, ...
        :param det_2Dbox: 2D axis-aligned bounding box of the detection in the image
            plane.
        :param confidence: Confidence score of the detection within [0, 1].
        :param det_mask: Segmentation mask of the detection. Can be either passed as
            run-length encoded mask or as binary mask with the same size as the frame.
        :param frame_id: Frame number on which the detection was found. Together with
            det_id  yields a primary key.
        :param time: Time of the frame recording.
        """
        self.label = label
        self.det_2Dbox = det_2Dbox
        self.rle_mask: Optional[dict]
        if isinstance(det_mask, dict):
            self.rle_mask = det_mask
        elif isinstance(det_mask, np.ndarray):
            self.det_mask = det_mask
        else:
            self.rle_mask = None
        self.frame_id = frame_id
        self.det_id = det_id
        self.confidence = confidence
        self.time = time

    @property
    def det_mask(self) -> Optional[np.ndarray]:
        """Image size binary detection mask.

        :return:
        """
        if self.rle_mask is None:
            return None
        else:
            return masktools.decode(self.rle_mask).astype(bool)

    @det_mask.setter
    def det_mask(self, value: np.ndarray) -> None:
        self.rle_mask = masktools.encode(np.asfortranarray(value))

    @property
    def mask_area(self) -> int:
        """Area of the mask (number of non-zero pixels).

        :return:
        """
        if self.rle_mask is None:
            return self.det_2Dbox.area
        else:
            return masktools.area(self.rle_mask)

    @property
    def min_area_2d_box(self) -> np.ndarray:
        """Rotated bounding box of the detection.

        :return: Array of the corner points of shape [4, 1, 2]
        """
        assert self.rle_mask is not None
        contours = find_contours(self.rle_mask)
        rect = cv2.minAreaRect(contours)
        box_points = cv2.boxPoints(rect)
        return box_points

    def merge(self, other: "Detection") -> None:
        """Merge another detection into the current on (in-place).

        :param other: Other detection object
        """
        if self.rle_mask is not None:
            self.rle_mask = masktools.merge((self.rle_mask, other.rle_mask))
        self.det_2Dbox.union(other.det_2Dbox)

    def iou(self, other: "Detection") -> float:
        """Calculate the Intersection-over-Union metric.

        Metric is calculated for this object's segmentation mask and other.

        :param other: Other detection object
        :return: Intersection-over-Union percentage
        """
        iou_rect = intersection_over_union_rect(self.det_2Dbox, other.det_2Dbox)
        if self.rle_mask is None:
            return iou_rect
        if iou_rect > 0.0:
            return intersection_over_union_mask(self, other)
        else:
            return 0.0

    def display_on_image(
        self,
        image: np.ndarray,
        color: Optional[Color] = None,
        color_text: Optional[Color] = (0, 0, 255),
        no_label: bool = False,
        no_2Dbox: bool = False,
        no_mask: bool = False,
        custom_text: Optional[str] = None,
    ) -> np.ndarray:
        """Display the detection on an image.

        :param image: Base image
        :param color: Mask and bounding box color (BGR format)
        :param color_text: Text color (BGR format)
        :param no_label: Suppress label
        :param no_2Dbox: Suppress 2D bounding box
        :param no_mask: Suppress mask
        :param custom_text: Change the default text ({detection id} {label} conf
            {confidence}).
        :return: Modified image (in-place)
        """
        # Define color
        if color is None:
            color = (
                np.random.randint(low=0, high=255),
                np.random.randint(low=0, high=255),
                np.random.randint(low=0, high=255),
            )

        if not (self.det_2Dbox is None):

            # Display 2D bounding box
            image = np.ascontiguousarray(image, dtype=np.uint8)
            if not no_2Dbox:
                image = self.det_2Dbox.draw_on_image(image, color)

            # Draw 2D box center
            # cv2.drawMarker(image, self.det_2Dbox.center, (0, 0, 255))
            # Display custom text
            text = ""
            if custom_text:
                text = custom_text

            # Display label
            elif not no_label:
                text = f"ID: {self.det_id}, {self.confidence:.2f} {self.label.name}"

            text_pos = self.det_2Dbox.top_left
            text_pos[1] -= 2
            image = cv2.putText(
                image, text, text_pos, cv2.FONT_HERSHEY_COMPLEX, 1, color_text, 1
            )

        # Draw key points
        # box = self.min_area_2d_box
        # box = [0.5 * (box[0] + box[1]), 0.5 * (box[2] + box[3]), 0.5 * (box[1] +
        # box[2]), 0.5 * (box[0] + box[-1])]
        # for p in box:
        #     cv2.drawMarker(image, p.astype(int).tolist(), (0, 0, 255))
        # Display mask
        if self.rle_mask is not None and not no_mask:
            contours = find_contours(self.rle_mask).reshape(-1, 2)
            pts = [
                contours[np.argmin(contours[:, 0])],
                contours[np.argmax(contours[:, 0])],
                contours[np.argmin(contours[:, 1])],
                contours[np.argmax(contours[:, 1])],
            ]
            for p in pts:
                image = cv2.drawMarker(image, p.astype(int).tolist(), (0, 0, 255))
            # Draw rotated bounding box
            # image = cv2.drawContours(image, [np.int0(self.min_area_2d_box)], 0,
            # (255, 255, 255), 2)
            if self.rle_mask is not None:
                draw_mask(self.det_mask, color, image)

        return image

    def __repr__(self) -> str:
        return f"Frame ID: {self.frame_id}, Det ID: {self.det_id}"

import logging
import multiprocessing.managers
import queue
import signal
from itertools import repeat
from typing import Tuple, Dict, Any, Generator

import detectron2
import numpy as np
import torch
import torch.multiprocessing as mp
from detectron2.checkpoint import DetectionCheckpointer
from detectron2.config import CfgNode, instantiate
from detectron2.export import scripting_with_instances
from detectron2.model_zoo import get_config
from detectron2.modeling import build_model
from omegaconf import DictConfig
from pycocotools.mask import area
from tqdm import tqdm

from mona.detection.bounding_box import BoundingBox
from mona.detection.detection import (
    Detection,
    DetectionLabel,
)
from mona.detection.mask_util import (
    roi_mask_to_rle,
)
from mona.serialization.detection_serializer import Hdf5Serializer
from mona.serialization.video_dataset import make_dataset

logger = logging.getLogger(__name__)

DetectionTuple = Tuple[
    int, float, Tuple[int, int], torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor
]


def _get_model(
    config_path: str, device: str, score_threshold: float
) -> Tuple[Any, Any]:
    cfg = get_config(config_path, trained=True)

    if device is None and not torch.cuda.is_available():
        device = "cpu"
    if device is not None and isinstance(cfg, CfgNode):
        cfg.MODEL.DEVICE = device

    max_num_detections = 250
    if isinstance(cfg, CfgNode):
        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = score_threshold
        cfg.TEST.DETECTIONS_PER_IMAGE = max_num_detections
        model = build_model(cfg)
        DetectionCheckpointer(model).load(cfg.MODEL.WEIGHTS)
    else:
        cfg.model.roi_heads.box_predictor.test_score_thresh = score_threshold
        cfg.model.roi_heads.box_predictor.test_topk_per_image = max_num_detections
        model = instantiate(cfg.model)
        if device is not None:
            model = model.to(device)
        if "train" in cfg and "init_checkpoint" in cfg.train:
            DetectionCheckpointer(model).load(cfg.train.init_checkpoint)
    model.eval()
    if not (config_path.endswith(".py") or "cascade" in config_path):
        fields = {
            "proposal_boxes": detectron2.structures.Boxes,
            "objectness_logits": torch.Tensor,
            "pred_boxes": detectron2.structures.Boxes,
            "scores": torch.Tensor,
            "pred_classes": torch.Tensor,
            "pred_masks": torch.Tensor,
        }
        model = scripting_with_instances(model, fields)

    return model, cfg


def _detect(
    config: DictConfig, device: str, model_str: str, score_threshold: float
) -> Generator[DetectionTuple, None, None]:
    crop = config.locations[config.video.location].get("crop")
    if crop is not None:
        crop = BoundingBox(np.array(crop))
    try:
        model, _ = _get_model(model_str, device, score_threshold)
        ds = make_dataset(config)
        for batch in tqdm(ds, desc="Detection", unit="frame"):
            with torch.no_grad():
                image_batch = []
                for frame_dict in batch:
                    image = frame_dict["image"]
                    if crop is not None:
                        image = image[:, crop.y1 : crop.y2 + 1, crop.x1 : crop.x2 + 1]
                    image_batch.append({"image": image})

                predictions = model.inference(image_batch, do_postprocess=False)
                for frame_dict, frame_pred in zip(batch, predictions):

                    # Clip boxes
                    if crop is not None:
                        frame_pred.pred_boxes.clip((crop.height - 1, crop.width - 1))
                    else:
                        frame_pred.pred_boxes.clip(
                            (frame_dict["height"] - 1, frame_dict["width"] - 1)
                        )

                    scores = frame_pred.scores
                    boxes_tensor = frame_pred.pred_boxes.tensor

                    size = (frame_dict["width"], frame_dict["height"])
                    boxes_tensor = boxes_tensor.cpu().numpy()
                    if crop is not None:
                        boxes_tensor[:, :2] += crop.top_left
                        boxes_tensor[:, 2:4] += crop.top_left

                    det_tuple = (
                        frame_dict["frame_index"],
                        frame_dict["time"],
                        size,
                        boxes_tensor,
                        scores.cpu(),
                        frame_pred.pred_classes.cpu(),
                        frame_pred.pred_masks.cpu(),
                    )
                    yield det_tuple
                    # Free memory
                    del frame_pred
        logger.debug("Inference finished!")
    except KeyboardInterrupt:
        return


def _process_detections(args: Tuple[DetectionTuple, mp.Queue, Dict[str, int]]) -> None:
    item, writing_queue, metrics = args
    # List of result
    det_object_list = []
    frame_index, timestamp, size, pred_boxes, scores, pred_classes, pred_masks = item
    logger.debug("Creating detections for frame %d", frame_index)
    scores = scores.numpy()
    pred_classes = pred_classes.numpy()
    if pred_masks is not None:
        pred_masks = pred_masks.numpy()
    for det_id, pred_box in enumerate(pred_boxes):
        bbox = BoundingBox(pred_box)
        confidence = scores[det_id]
        label = DetectionLabel(pred_classes[det_id])
        if pred_masks is not None:
            det_mask = roi_mask_to_rle(pred_masks[det_id], bbox, size)
            if area(det_mask) == 0:
                metrics["num_dropped_detections"] += 1
                continue
        else:
            det_mask = None
        # Create det object
        det_object = Detection(
            det_id,
            label,
            bbox,
            confidence,
            det_mask=det_mask,
            frame_id=frame_index,
            time=timestamp,
        )
        det_object_list.append(det_object)
    spin_lock_iterations = 0
    while True:
        try:
            writing_queue.put((frame_index, det_object_list), timeout=1)
        except queue.Full:
            spin_lock_iterations += 1
            if spin_lock_iterations < 5:
                logger.warning("Writing queue full! Waiting!")
            continue
        break
    del item


def _detections_writer(
    serializer: Hdf5Serializer, writing_queue: mp.Queue, metrics: Dict[str, int]
) -> None:
    # Do not open file until the first item arrives
    while writing_queue.empty():
        pass
    with serializer:
        try:
            while True:
                item = writing_queue.get()
                if item is None:
                    break
                serializer.serialize_frame(*item)
                metrics["num_detections"] += len(item[1])
        except KeyboardInterrupt:
            pass
    logger.debug("Writer process quit!")


class MpDetectronDetection:
    """Multi-processing implementation of object detection using Detectron2.

    A single process runs batch inference on the GPU or CPU and writes the raw
    results to a queue. Multiple processes take all detections of a single frame from
    the queue and create a detection object for each (Single producer-multi
    consumer). The detection objects are afterward put in a queue, from which a
    single writer process takes and serializes them.
    """

    @classmethod
    def create_from_config(cls, config: DictConfig) -> "MpDetectronDetection":
        """Create a detector from config.

        :param config: Config containing key "detection" with parameters.
        :return: Newly created detector object.
        """
        config = config.detection
        return cls(
            config.device,
            config.weights,
            config.score_threshold,
            config.cpu_threads,
            config.inference_queue,
            config.writing_queue,
        )

    def __init__(
        self,
        device: str = "cuda",
        model_str: str = "Misc/cascade_mask_rcnn_X_152_32x8d_FPN_IN5k_gn_dconv.yaml",
        score_threshold: float = 0.5,
        detection_threads: int = 5,
        detections_queue: int = 4,
        writing_queue: int = 100,
    ):
        """Create a Detector.

        :param device: Processing device for the neural network (either 'cpu' or
            'cuda').
        :param model_str: Configuration string from the detectron2 model zoo
        :param score_threshold: Cut-off value for the detection confidence (Smaller
            numbers yield more detections, but lower the runtime performance).
        :param detection_threads: Number of threads to use for creating detection
            objects from raw detections.
        :param detections_queue: Maximum queue length for raw detections. Each
            element contains all detections within a single frame.
        :param writing_queue: Maximum queue length for processed detections in one
            frame.
        """
        self.num_dropped_detections = 0
        self.device = device
        self.model_str = model_str
        self.score_threshold = score_threshold
        self.detection_threads = detection_threads
        self.detections_queue = detections_queue
        self.writing_queue = writing_queue

    @staticmethod
    def _init_manager() -> None:
        """Make the manager of the metrics dict ignore the keyboard interrupt signal."""
        signal.signal(signal.SIGINT, signal.SIG_IGN)

    def detect_dataset(
        self, dataset_config: DictConfig, serializer: Hdf5Serializer
    ) -> None:
        """Detect all objects in the dataset and save to disk.

        :param dataset_config: Dataset configuration
        :param serializer: Serializer for saving detections.
        """
        manager = multiprocessing.managers.SyncManager()
        manager.start(self._init_manager)
        with manager:
            metrics = manager.dict()
            metrics["num_dropped_detections"] = 0
            metrics["num_detections"] = 0

            writing_queue = manager.Queue(self.writing_queue)
            writer = mp.Process(
                target=_detections_writer, args=(serializer, writing_queue, metrics)
            )
            writer.start()
            with mp.Pool(self.detection_threads, self._init_manager) as pool:
                try:
                    it = pool.imap_unordered(
                        _process_detections,
                        zip(
                            _detect(
                                dataset_config,
                                self.device,
                                self.model_str,
                                self.score_threshold,
                            ),
                            repeat(writing_queue),
                            repeat(metrics),
                        ),
                    )
                    for _ in it:
                        pass
                except KeyboardInterrupt:
                    logger.warning("User interrupt! Shutting down!")
                    pool.terminate()
                    raise
                finally:
                    writing_queue.put(None)
                    writer.join()
                    self.num_dropped_detections = metrics["num_dropped_detections"]
                    serializer.num_detections = metrics["num_detections"]

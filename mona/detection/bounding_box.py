from typing import Tuple

import cv2
import numpy as np


class BoundingBox:
    """Representation of a 2D bounding box with integer coordinates."""

    def __init__(self, x1y1x2y2: np.ndarray):
        """Create a new BoundingBox object.

        :param x1y1y2y2: Array with size 4 containing the top left and bottom right
        x, y coordinates.
        Floating point values will be truncated.
        """
        self.x1y1x2y2 = x1y1x2y2.reshape((2, 2)).astype(int)
        assert self.width > 0
        assert self.height > 0

    @property
    def x1(self) -> int:
        """X coordinate of the top left corner.

        :return:
        """
        return int(self.x1y1x2y2[0, 0])

    @property
    def y1(self) -> int:
        """Y coordinate of the top left corner.

        :return:
        """
        return int(self.x1y1x2y2[0, 1])

    @property
    def x2(self) -> int:
        """X coordinate of the bottom right corner.

        :return:
        """
        return int(self.x1y1x2y2[1, 0])

    @property
    def y2(self) -> int:
        """Y coordinate of the bottom right corner.

        :return:
        """
        return int(self.x1y1x2y2[1, 1])

    @property
    def width(self) -> int:
        """Width of the bounding box (X dimension).

        :return:
        """
        return self.x1y1x2y2[1, 0] - self.x1y1x2y2[0, 0] + 1

    @property
    def height(self) -> int:
        """Height of the boundinx box (Y dimension).

        :return:
        """
        return self.x1y1x2y2[1, 1] - self.x1y1x2y2[0, 1] + 1

    @property
    def top_left(self) -> np.ndarray:
        """X,Y coordinates of the top left corner.

        :return:
        """
        return self.x1y1x2y2[0]

    @property
    def bottom_right(self) -> np.ndarray:
        """X,Y coordinates of the bottom right corner.

        :return:
        """
        return self.x1y1x2y2[1]

    @property
    def corners(self) -> np.ndarray:
        """Get array of the corner points of the bounding box.

        :return: Array of corner points with shape [4,2] and order top left,
        top right, bottom right, bottom left.
        """
        return np.array(
            [self.top_left, [self.x2, self.y1], self.bottom_right, [self.x1, self.y2]]
        )

    @property
    def center(self) -> np.ndarray:
        """Coordinates of the center point of the bounding box.

        :return:
        """
        return (np.sum(self.x1y1x2y2, axis=0) * 0.5).astype(int)

    @property
    def area(self) -> int:
        """Get area of the bounding box in pixels.

        :return:
        """
        return self.width * self.height

    def draw_on_image(
        self, image: np.ndarray, color: Tuple[int, int, int]
    ) -> np.ndarray:
        """Draw the bounding box on the provided image.

        :param image: Image to draw on (openCV format)
        :param color: 3-tuple of the color in BGR format
        :return: Modified image
        """
        return cv2.rectangle(
            image, self.top_left.astype(int), self.bottom_right.astype(int), color, 3
        )

    def union(self, other: "BoundingBox") -> None:
        """Compute the union with the other bounding box in-place.

        :param other: Bounding box to merge with
        """
        self.x1y1x2y2 = np.array(
            [
                np.fmin(self.top_left, other.top_left),
                np.fmax(self.bottom_right, other.bottom_right),
            ]
        )

    def within(self, pt: np.ndarray) -> bool:
        """Check whether a point is within the bounding box.

        :param pt: Point to check
        :return: True if the point is within the bounding box.
        """
        return np.all((self.top_left <= pt) & (pt <= self.bottom_right), axis=-1)


def intersection_over_union_rect(rect_1: BoundingBox, rect_2: BoundingBox) -> float:
    """Compute the IoU of two rectangles efficiently.

    :param rect_1: First rectangle
    :param rect_2: Second rectangle
    :return: IoU
    """
    over = intersection_rect(rect_1, rect_2)

    union = intersection_rect(rect_1, rect_1) + intersection_rect(rect_2, rect_2) - over

    result = 0.0
    if float(union) > 0.01:
        result = float(over) / float(union)

    return result


def intersection_rect(rect_1: BoundingBox, rect_2: BoundingBox) -> float:
    """Compute the intersection area of two rectangles efficiently.

    :param rect_1: First rectangle
    :param rect_2: Second rectangle
    :return: Intersection area
    """
    dx = min(rect_1.x2, rect_2.x2) - max(rect_1.x1, rect_2.x1)
    dy = min(rect_1.y2, rect_2.y2) - max(rect_1.y1, rect_2.y1)
    if dx >= 0 and dy >= 0:
        return dx * dy
    else:
        return 0.0

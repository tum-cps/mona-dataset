import logging
import os
from pathlib import Path
from time import perf_counter
from typing import Optional

from omegaconf import OmegaConf, DictConfig
from ruamel.yaml import CommentedMap, YAML


from mona.serialization.detection_serializer import Hdf5Serializer
from mona.serialization.video_dataset import VideoDataset
from mona.utils.util import get_git_commit
from mona.detection.detector import MpDetectronDetection


def detect(config: DictConfig, detector: Optional[MpDetectronDetection] = None) -> None:
    """Detect object in a video stream.

    :param config: Configuration
    :param detector: Optional detector object to avoid recreation.
    """
    if detector is None:
        from mona.detection.detector import MpDetectronDetection

        detector = MpDetectronDetection.create_from_config(config)
    output_dir = Path(config.output_dir)
    os.makedirs(output_dir, exist_ok=True)
    det_path = output_dir / "detections.hd5"
    tick = perf_counter()
    serializer = Hdf5Serializer(det_path)
    try:
        detector.detect_dataset(config, serializer)
    finally:
        elapsed_sec = perf_counter() - tick
        frames = len(VideoDataset.from_config(config))
        logging.info(
            "Elapsed: %.2f s; Throughput: %.2f s/frame; FPS: %.2f frames/s",
            elapsed_sec,
            elapsed_sec / frames,
            frames / elapsed_sec,
        )
        metrics = CommentedMap()
        metrics.insert(
            0,
            "detection_time",
            elapsed_sec,
            "Elapsed time in seconds from start to finish of detection",
        )
        metrics.insert(
            0, "detection_fps", frames / elapsed_sec, "Processed frames per second"
        )
        metrics.insert(
            0, "num_detections", serializer.num_detections, "Number of saved detections"
        )
        metrics.insert(
            0,
            "num_dropped_detections",
            detector.num_dropped_detections,
            "Number of detections dropped e.g. because of excluded label",
        )
        metrics.insert(0, "git_commit", get_git_commit())
        YAML().dump(metrics, output_dir / "detection_metrics.yaml")
        config.video.path = os.path.relpath(config.video.path, start=output_dir)
        cfg = OmegaConf.create()
        cfg.video = config.video
        OmegaConf.save(cfg, output_dir / "config.yaml")

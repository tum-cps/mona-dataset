import logging
from pathlib import Path
from typing import List, Optional

import hydra
from ruamel.yaml import YAML

from mona.association import DeepSortAssociator, Tracklet
from mona.serialization.detection_serializer import Hdf5Deserializer
from mona.utils.filtering import CompoundFilter
from mona.utils.util import read_bz2_pickle, write_bz2_pickle, get_git_commit

logger = logging.getLogger(__name__)


def associate(config, poses=None, progress=True) -> Optional[List[Tracklet]]:
    output_dir = Path(config["output_dir"])
    track_path = output_dir / "tracks.pkl"

    if track_path.exists() and config.get("skip_existing", False):
        logger.info(
            f"Found previous results {track_path.absolute()}. Skipping association!"
        )
        return None
    if poses is None:
        poses = read_bz2_pickle(output_dir / "poses.pkl")
    output_dir = Path(config["output_dir"])
    det_path = output_dir / "detections.hd5"
    det_asso = DeepSortAssociator.create_from_config(config)
    det_compound_filter = CompoundFilter(
        [hydra.utils.instantiate(cfg) for cfg in config.filtering.pose_estimation]
    )
    deserializer = Hdf5Deserializer(det_path, filter_fn=det_compound_filter)

    # Filter poses
    compound_filter = CompoundFilter(
        [hydra.utils.instantiate(cfg) for cfg in config.filtering.association]
    )
    poses = compound_filter(poses)

    tracks = det_asso.associate(deserializer, poses, progress)
    write_bz2_pickle(track_path, tracks)
    metrics = {
        "git_commit": get_git_commit(),
        "num_tracks": len(tracks),
        "num_filtered_detections": det_asso.num_detections,
    }
    logger.info("metrics: %s", metrics)
    YAML().dump(metrics, output_dir / "association_metrics.yaml")
    return tracks

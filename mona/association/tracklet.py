from collections import Counter, deque
from typing import Optional, Dict, Any, Deque

import cv2
import hydra.utils
import numpy as np

from mona.association.util import project_kf
from mona.detection.detection import Detection, DetectionLabel
from mona.camera_model import CameraModel
from mona.pose_estimation.pose import Pose
from mona.smoothing.base_filter import BaseFilter


class Tracklet:
    """DeepSORT companion class representing a tracked object."""

    def __init__(
        self,
        track_id: int,
        detection: Detection,
        filter_params: Dict[str, Any],
        max_time_since_update: int = 25,
        num_init_detections: int = 3,
    ):
        """Create a tracklet object.

        :param track_id: Unique ID of the object
        :param detection: Initial detection
        :param filter_params: Parameters to instantiate KF filter object as
            `hydra.utils.instantiate(filter_params)`
        :param max_time_since_update: Maximum number of consecutive misses until
            inactivation
        :param num_init_detections: Number of consecutive detections to confirm the
            tracklet
        """
        self.track_id = track_id
        self.detections = {detection.frame_id: detection.det_id}
        self.detection_label_counts: Counter = Counter()
        self.hits = 0
        self.time_since_update = 0
        self.max_time_since_update = max_time_since_update
        self.num_init_detections = num_init_detections
        self.kf: BaseFilter = hydra.utils.instantiate(filter_params)
        # Keep last detections for visual descriptor comparison
        self.last_det: Deque[Detection] = deque(maxlen=self.max_time_since_update)
        # Keep first poses to get the initial state estimate for KF
        self.last_pose: Deque[Pose] = deque(maxlen=self.num_init_detections)
        self.mean_cov: Dict[int, Any] = {}
        self.update(detection)

    def update(
        self, detection: Detection, measurement_noise: Optional[np.ndarray] = None
    ) -> None:
        """Update the tracklet with a new detection.

        :param detection: Newly associated detection
        :param measurement_noise: Measurement noise for new detection
        """
        position = detection.position
        self.hits += 1
        self.time_since_update = 0
        self.last_det.appendleft(detection)
        self.detection_label_counts[detection.label] += 1
        self.detections[detection.frame_id] = detection.det_id
        if self.is_confirmed:
            self.kf.update_from_measurement(position, measurement_noise)
            self.mean_cov[max(self.mean_cov.keys())] = project_kf(self.kf)
        elif self.is_tentative:
            self.last_pose.append(position)
        if self.is_tentative and self.hits == self.num_init_detections:
            vel = np.stack(self.last_pose)
            vel = (np.diff(vel, axis=0) / self.kf.dt).mean(axis=0)
            self.kf.init_x(
                *position,
                theta=np.arctan2(vel[1], vel[0]),
                vel=np.linalg.norm(vel),
            )
            self.mean_cov[detection.frame_id] = project_kf(self.kf)

    def predict(self) -> None:
        """Predict position of object for next time step and update filter state."""
        self.time_since_update += 1
        if self.is_confirmed:
            self.kf.predict()
            self.mean_cov[max(self.mean_cov.keys()) + 1] = project_kf(self.kf)

    def iou(self, detection: Detection) -> float:
        """Compute IoU of tracklet's latest detection with other detection.

        :param detection: Other detection
        :return: IoU percentage
        """
        return self.last_det[0].iou(detection)

    @property
    def last_frame(self) -> int:
        """Get the latest frame id of an associated detection.

        :return: Frame id of the latest detection
        """
        return max(self.detections.keys())

    @property
    def initial_frame(self) -> int:
        """Get the first frame id of an associated detection.

        :return: Frame id of the first detection
        """
        return min(self.detections.keys())

    @property
    def agent_type(self) -> DetectionLabel:
        """Get the most common associated agent type of the tracklet.

        :return: Most common associated agent type
        """
        return self.detection_label_counts.most_common(1)[0][0]

    def __len__(self) -> int:
        return self.last_frame - self.initial_frame + 1

    @property
    def is_tentative(self) -> bool:
        """Check if tracklet is in tentative state."""
        return self.hits <= self.num_init_detections

    @property
    def dropped(self) -> bool:
        """Check if tracklet should be dropped."""
        return self.is_tentative and self.time_since_update > 0

    @property
    def is_confirmed(self) -> bool:
        """Check if tracklet is confirmed."""
        return not self.is_tentative and not self.inactive

    @property
    def inactive(self) -> bool:
        """Check if tracklet is inactive."""
        return self.time_since_update > self.max_time_since_update

    def display_kf_on_image(
        self,
        image: np.ndarray,
        frame_id: int,
        cam_model: CameraModel,
        deviations: float = 3.0,
    ) -> np.ndarray:
        """Draw visualization of KF on the camera image.

        The KF state is visualized as an uncertainty ellipse with `deviations` as the
        ellipse's outline.

        :param image: Camera image in BGR format
        :param frame_id: Camera image's frame id
        :param cam_model: Camera model to map from world to image coordinates
        :param deviations: Number of standard deviations for ellipse
        :return: Modified camera image
        """
        if frame_id not in self.mean_cov:
            return image

        mean, cov = self.mean_cov[frame_id]
        cov_2 = np.eye(3)
        cov_2[:2, :2] = cov
        mean, cov = cam_model.project_covariance(np.append(mean, [0]), cov_2)

        U, s, _ = np.linalg.svd(cov)
        orientation = np.arctan2(U[1, 0], U[0, 0])
        width = deviations * np.sqrt(s[1])
        height = deviations * np.sqrt(s[0])
        image = cv2.ellipse(
            image,
            mean,
            (int(width), int(height)),
            -np.rad2deg(orientation),
            0,
            360,
            (0, 0, 255),
            2,
        )
        image = cv2.drawMarker(image, mean, (0, 0, 255))
        image = cv2.putText(
            image,
            f"T: {self.track_id}",
            mean,
            cv2.FONT_HERSHEY_COMPLEX,
            2,
            (255, 0, 0),
        )
        return image

    def __repr__(self) -> str:
        return f"Track ID: {self.track_id}"

import functools
import importlib.resources
import logging
from typing import List, Dict, Callable, Any, Tuple, Set, Iterable

import numpy as np
import scipy.optimize
import scipy.spatial.transform
import scipy.stats
from commonroad.common.file_reader import CommonRoadFileReader
from commonroad.scenario.scenario import Scenario
from omegaconf import DictConfig
from tqdm import tqdm

from mona.association.tracklet import Tracklet
from mona.association.util import project_kf, squared_mahalanobis
from mona.detection.detection import Detection
from mona.camera_model import CameraModel
from mona.pose_estimation.pose import Pose
from mona.serialization.detection_serializer import Hdf5Deserializer
from mona.utils.util import get_transform_to_scenario

logger = logging.getLogger(__name__)


class DeepSortAssociator:
    """Use DeepSORT-like association for mult-object tracking.

    Compared to the original DeepSORT, this implementation

    * Does not use a re-identification network (subject to future work)
    * The Kalman filter tracks object in the world frame using a first-order
        kinematic model.
    * Uses different measurement noise characteristics for IoU and KF association
        updates.
    """

    @classmethod
    def create_from_config(cls, config: DictConfig) -> "DeepSortAssociator":
        """Create associator object from config.

        :param config: Configuration
        :return: Associator object
        """
        # Threshold to associate measurement: Need to overlap by at least this threshold
        threshold_overlap = config.association.threshold_overlap
        # Number of frame we look into the past to associate measurement to a track
        cascade_length = config.association.cascade_length
        iou_measurement_noise = np.eye(2) * config.association.var_meas_pos_iou
        filter_params = config.association.kf_params
        with importlib.resources.path(
            "mona.maps", config.location.map_file
        ) as map_path:
            scenario = CommonRoadFileReader(str(map_path)).open()[0]
        cam_model = CameraModel.create_from_config(config)
        return cls(
            threshold_overlap,
            cascade_length,
            config.association.init_frames,
            iou_measurement_noise,
            filter_params,
            scenario,
            cam_model,
        )

    def __init__(
        self,
        min_iou: float,
        cascade_length: int,
        n_init: int,
        iou_measurement_noise: np.ndarray,
        filter_params: Dict[str, Any],
        scenario: Scenario,
        cam_model: CameraModel,
    ):
        """Create associator object.

        :param min_iou: Minimum IoU value to consider pairs
        :param cascade_length: Number of time steps to look back for association
        :param n_init: Number of confirmation time steps
        :param iou_measurement_noise: Measurement noise for IoU association update.
        :param filter_params: kwargs parameters for KF filter instantiation
        :param scenario: CommonRoad scenario object with lanelet network
        :param cam_model: Camera model, mapping between image and world coordinates.
        """
        self.tracks: List[Tracklet] = []
        self.inactive_tracks: List[Tracklet] = []
        self.num_detections = 0
        self.num_dropped = 0
        self.num_iou_updates = 0
        self.num_kf_updates = 0
        # Constants
        self.min_iou = min_iou
        self.max_distance = scipy.stats.chi2.ppf(0.95, df=2)
        logger.debug("Maximum admissible distance: %.3f", self.max_distance)
        self.cascade_length = cascade_length
        self.n_init = n_init
        self.track_id_counter = 0
        self.frame_counter = -1
        self.iou_measurement_noise = iou_measurement_noise
        self.filter_params = filter_params
        self.lanelet_network = scenario.lanelet_network
        self.adjacency = {
            lanelet.lanelet_id: set(
                filter(
                    lambda x: x is not None,
                    lanelet.predecessor
                    + lanelet.successor
                    + [lanelet.adj_left, lanelet.adj_right],
                )
            )
            for lanelet in self.lanelet_network.lanelets
        }
        self.map_transform = get_transform_to_scenario(scenario, cam_model.origin)
        self.cam_model = cam_model

    def _update(self, all_detections: List[Detection], poses: Dict[int, Pose]) -> None:
        self.frame_counter += 1
        # Check detections with valid pose
        detections = []
        all_lanelet_ids = {
            lanelet.lanelet_id for lanelet in self.lanelet_network.lanelets
        }
        for detection in all_detections:
            pose = poses.get(detection.det_id)
            if pose is not None:
                detection.position = np.array([pose.x, pose.y])
                lanelets = self.lanelet_network.find_lanelet_by_position(
                    [detection.position]
                )[0]
                if len(lanelets) > 0:
                    detection.adj_lanelets = set(
                        sum([list(self.adjacency[lanelet]) for lanelet in lanelets], [])
                        + lanelets
                    )
                else:
                    detection.adj_lanelets = all_lanelet_ids
                detections.append(detection)

        for track in self.tracks:
            track.predict()

        kf_matches, iou_matches, _, unmatched_detections = self._match(detections)

        # Update track set.
        self.num_kf_updates += len(kf_matches)
        for track, detection in kf_matches:
            track.update(detection)
        self.num_iou_updates += len(iou_matches)
        for track, detection in iou_matches:
            track.update(detection, self.iou_measurement_noise)
        assert len(detections) == 0 or detections[0].frame_id == self.frame_counter
        logger.debug(
            "Frame %d: Creating %d new tracks",
            self.frame_counter,
            len(unmatched_detections),
        )
        for detection in sorted(unmatched_detections, key=lambda d: d.det_id):
            new_track_id = self.track_id_counter
            self.track_id_counter += 1
            self.tracks.append(
                Tracklet(
                    new_track_id,
                    detection,
                    self.filter_params,
                    self.cascade_length,
                    self.n_init,
                )
            )
        self.inactive_tracks.extend(filter(lambda t: t.inactive, self.tracks))
        num_dropped = len(list(filter(lambda t: t.dropped, self.tracks)))
        logger.debug("Frame %d: Dropped %d tracks", self.frame_counter, num_dropped)
        self.num_dropped += num_dropped
        self.tracks = list(
            filter(
                lambda t: t.is_confirmed or t.is_tentative and not t.dropped,
                self.tracks,
            )
        )

    def associate(
        self,
        detections: Hdf5Deserializer,
        poses: Dict[int, Iterable[Pose]],
        progress: bool = True,
    ) -> List[Tracklet]:
        """Track detections with estimated poses over multiple frames.

        :param detections: Deserializer object for detections
        :param poses: Mapping from frame id to an iterable of Pose objects in the
            corresponding frame.
        :param progress: Display progress bar
        :return: List of tracklet objects
        """
        with detections:
            for frame_id, det_object_list in tqdm(
                detections,
                desc="Association",
                unit="frame",
                total=len(set(poses.keys())),
                disable=not progress,
            ):
                self.num_detections += len(det_object_list)
                pose = poses.get(frame_id, dict())
                self._update(det_object_list, pose)
            logger.debug(
                "KF updates: %d; IOU updates: %d",
                self.num_kf_updates,
                self.num_iou_updates,
            )
        return [t for t in self.tracks if t.is_confirmed] + self.inactive_tracks

    @staticmethod
    def _threshold_match(
        track_set: Set[Tracklet],
        measurement_set: Set[Detection],
        cost_fn: Callable[[Tracklet, Iterable[Detection]], np.ndarray],
        threshold: float,
    ) -> Tuple[Set[Tuple[int, int]], Set[Tracklet], Set[Detection]]:
        measurements = np.array(list(measurement_set))
        tracks = np.array(list(track_set))
        cost_matrix = np.stack([cost_fn(t, measurements) for t in tracks])
        cost_matrix[cost_matrix > threshold] = threshold + 1e-5
        matched_tracks, matched_measurements = scipy.optimize.linear_sum_assignment(
            cost_matrix
        )
        # Check maximum distance -> those are considered unmatched
        valid = cost_matrix[matched_tracks, matched_measurements] <= threshold
        matched_tracks = tracks[matched_tracks[valid]]
        matched_measurements = measurements[matched_measurements[valid]]
        unmatched_tracks = track_set.difference(matched_tracks)
        unmatched_measurements = measurement_set.difference(matched_measurements)
        matches = set(zip(matched_tracks, matched_measurements))
        return matches, unmatched_tracks, unmatched_measurements

    @staticmethod
    def _matching_cascade(
        tracks: Iterable[Tracklet],
        detections: Iterable[Detection],
        cascade_length: int,
        matching_function: Callable[
            [Iterable[Tracklet], Set[Detection]],
            Tuple[Set[Tuple[int, int]], Set[Tracklet], Set[Detection]],
        ],
    ) -> Tuple[Set[Tuple[int, int]], Set[Tracklet], Set[Detection]]:
        unmatched_detections = set(detections)
        unmatched_tracks = set(tracks)
        matches = set()
        for level in range(1, cascade_length + 1):
            if len(unmatched_detections) == 0:
                break
            # Get tracks according to level
            level_tracks = {k for k in unmatched_tracks if k.time_since_update == level}

            if len(level_tracks) == 0:
                continue

            unmatched_tracks.difference_update(level_tracks)
            (
                level_matches,
                level_unmatched_tracks,
                unmatched_detections,
            ) = matching_function(level_tracks, unmatched_detections)
            unmatched_tracks.update(level_unmatched_tracks)
            matches.update(level_matches)
        return matches, unmatched_tracks, unmatched_detections

    def _get_kf_cost_matrix(
        self, track: Tracklet, detections: List[Detection]
    ) -> np.ndarray:
        measurements = np.stack([p.position for p in detections])
        mean, cov = project_kf(track.kf)
        cov += track.kf.R
        distances = squared_mahalanobis(mean, cov, measurements)
        map_pos = np.array(self.map_transform.transform(*mean))
        track_lanelet = self.lanelet_network.find_lanelet_by_position([map_pos])[0]
        if len(track_lanelet) > 0:
            track_adj_lanelets = set(
                sum([list(self.adjacency[lanelet]) for lanelet in track_lanelet], [])
                + track_lanelet
            )
            invalids = [
                len(d.adj_lanelets.intersection(track_adj_lanelets)) == 0
                for d in detections
            ]
            distances[invalids] = np.inf

        for j, detection in enumerate(detections):
            logger.debug(
                "Frame %d: KF cost track %d detection %d: %.3f",
                detection.frame_id,
                track.track_id,
                detection.det_id,
                distances[j],
            )
        return distances

    @staticmethod
    def _get_iou_cost_matrix(
        track: Tracklet, detections: List[Detection]
    ) -> np.ndarray:
        return -1 * np.array([track.iou(d) for d in detections])

    def _get_position_from_detection(self, detection: Detection) -> np.ndarray:
        return self.cam_model.backproject_pixels(
            detection.det_2Dbox.center, z_constraint=1.5
        )[:2]

    def _match(
        self, detections: List[Detection]
    ) -> Tuple[
        Set[Tuple[int, int]], Set[Tuple[int, int]], Set[Tracklet], Set[Detection]
    ]:
        if len(self.tracks) == 0:
            return set(), set(), set(), set(detections)
        elif len(detections) == 0:
            return set(), set(), set(self.tracks), set()

        # Split track set into confirmed and unconfirmed tracks.
        confirmed_tracks = {t for t in self.tracks if t.is_confirmed}
        unconfirmed_tracks = {t for t in self.tracks if not t.is_confirmed}

        logger.debug(
            "Frame %d: Confirmed tracks %d (%.2f %%)",
            detections[0].frame_id,
            len(confirmed_tracks),
            len(confirmed_tracks) / len(self.tracks) * 100.0,
        )

        # Matching cascade
        unmatched_tracks = set(confirmed_tracks)
        unmatched_detections = set(detections)
        (
            kf_matches,
            unmatched_tracks,
            unmatched_detections,
        ) = DeepSortAssociator._matching_cascade(
            unmatched_tracks,
            unmatched_detections,
            self.cascade_length,
            functools.partial(
                DeepSortAssociator._threshold_match,
                cost_fn=self._get_kf_cost_matrix,
                threshold=self.max_distance,
            ),
        )
        logger.debug(
            "Frame %d: Matched %d tracks by KF", detections[0].frame_id, len(kf_matches)
        )

        # Associate remaining tracks together with unconfirmed tracks using IOU.
        unmatched_tracks.update(unconfirmed_tracks)
        if len(unmatched_detections) == 0 or len(unmatched_tracks) == 0:
            return kf_matches, set(), unmatched_tracks, unmatched_detections

        (
            iou_matches,
            unmatched_tracks,
            unmatched_detections,
        ) = DeepSortAssociator._threshold_match(
            unmatched_tracks,
            unmatched_detections,
            self._get_iou_cost_matrix,
            -self.min_iou,
        )
        logger.debug(
            "Frame %d: Matched %d tracks by IOU",
            detections[0].frame_id,
            len(iou_matches),
        )
        return kf_matches, iou_matches, unmatched_tracks, unmatched_detections

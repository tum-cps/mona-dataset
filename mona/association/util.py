from typing import Tuple

import numpy as np
import scipy

from mona.smoothing.base_filter import BaseFilter


def project_kf(kf: BaseFilter) -> Tuple[np.ndarray, np.ndarray]:
    """Project mean and covariance of KF to measurement space.

    :param kf: Kalman filter object
    :return: Projected mean and covariance
    """
    proj_mean = kf.calculate_hx(kf.x)
    h = kf.calculate_H_jacobian_at(np.squeeze(kf.x))
    proj_covariance = np.linalg.multi_dot((h, kf.P, h.T))
    return proj_mean, proj_covariance


def squared_mahalanobis(
    mean: np.ndarray, covariance: np.ndarray, measurements: np.ndarray
) -> np.ndarray:
    """Calculate (vectorized) squared Mahalanobis distance.

    :param mean: Mean vector of KF in measurement space
    :param covariance: Covariance matrix of KF in measurement space
    :param measurements: Measurements in same space as mean.
    :return: squared Mahalanobis distance
    """
    cholesky_factor = np.linalg.cholesky(covariance)
    d: np.ndarray = measurements - mean.T
    z = scipy.linalg.solve_triangular(
        cholesky_factor, d.T, lower=True, check_finite=False, overwrite_b=True
    )
    squared_maha = np.sum(z * z, axis=0)
    return squared_maha

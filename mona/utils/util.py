import bz2
import importlib.machinery
import logging
import os
import pickle
import re
from pathlib import Path
from typing import Union, Optional, Tuple, Any, Dict, Callable, List

import cv2
import hydra.utils
import matplotlib.axes
import matplotlib.colors
import numpy as np
import pandas as pd
import pyproj
import scipy.spatial
from commonroad.scenario.scenario import Scenario
from git import Repo, InvalidGitRepositoryError
from hydra.core.global_hydra import GlobalHydra
from matplotlib import cm
from omegaconf import OmegaConf, DictConfig
from pyproj import Transformer

Color = Tuple[int, int, int]
Colormap = Callable[[int], Color]


def angle_difference(b1: np.ndarray, b2: np.ndarray) -> np.ndarray:
    """Compute the [-pi, pi] normalized signed angle difference b2 - b1.

    From https://rosettacode.org/wiki/Angle_difference_between_two_bearings#Python

    :param b1:
    :param b2:
    :return:
    """
    r: np.ndarray = (b2 - b1) % (2.0 * np.pi)
    r_shape = r.shape
    r = np.reshape(r, -1)
    # Python modulus has same sign as divisor, which is positive here,
    # so no need to consider negative case
    r[r >= np.pi] -= 2 * np.pi
    r.shape = r_shape
    return r


def write_bz2_pickle(path: Path, obj: Any) -> None:
    """Write a bzip2 compressed pickle file of an object to disk."""
    logging.info("Writing compressed pickle to '%s'", path.absolute())
    with bz2.BZ2File(path, "w") as f:
        pickle.dump(obj, f)


def read_bz2_pickle(path: Path) -> Any:
    """Read a bzip2 compressed pickle file from disk."""
    with bz2.BZ2File(path, "r") as f:
        obj = pickle.load(f)
    return obj


def draw_time_stamp(
    frame_dict: Dict[str, Union[str, int, float]], image: np.ndarray
) -> None:
    """Draw the time stamp to the lower left corner of the image"""
    time_str = f"{frame_dict['time']} frame: {frame_dict['frame_index']}"
    cv2.putText(
        image,
        time_str,
        np.array([0, image.shape[0] - 5]),
        cv2.FONT_HERSHEY_SIMPLEX,
        1,
        (0, 0, 255),
        2,
    )


def get_colormap(max_x: int) -> Colormap:
    """Get a color map based on matplotlib prism from 0 to max_x."""
    norm = matplotlib.colors.Normalize(vmin=0, vmax=max_x)
    cmap = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap("prism"))
    # Closure
    return lambda x: tuple([int(c) for c in cmap.to_rgba(x, bytes=True)[:3]])


def read_pandas(path: Union[Path, str]) -> pd.DataFrame:
    """Read data in HDF5, CSV, Feather, or Apache Parquet format into a Dataframe."""
    path = Path(path)
    assert path.exists(), f"Path {path.resolve().absolute()} does not exist!"
    assert path.is_file(), f"Path {path.resolve().absolute()} is not a file!"
    if path.suffix == ".hd5":
        df = pd.read_hdf(path)
    elif path.suffix == ".csv":
        df = pd.read_csv(path)
    elif path.suffix == ".feather":
        df = pd.read_feather(path)
    elif path.suffix == ".parquet":
        df = pd.read_parquet(path)
    else:
        raise ValueError(f"Unrecognized format {path.suffix}!")
    return df


def read_trajectories(path: Union[str, Path]) -> pd.DataFrame:
    """Read a trajectory file and rename columns into unified names."""
    df = read_pandas(path)
    df.rename(columns={"time_step": "frame_id", "theta": "psi_rad"}, inplace=True)
    return df


def get_git_commit() -> str:
    """Get the checked out git commit of the git repo in the working directory."""
    if GlobalHydra.instance().is_initialized():
        cwd = hydra.utils.get_original_cwd()
    else:
        cwd = os.getcwd()
    try:
        repo = Repo(cwd, search_parent_directories=True)
        commit_info = f"{repo.head.object.hexsha}"
        commit_info += "-dirty" if repo.is_dirty() else ""
        return commit_info
    except InvalidGitRepositoryError:
        return "N/A"


def load_config_from_output(
    base_config: DictConfig,
    path: Optional[Union[Path, str]] = None,
    overrides: Optional[List[str]] = None,
) -> DictConfig:
    """Merge base config with video config and fix video path."""
    if path is None:
        path = base_config.output_dir
    OmegaConf.set_struct(base_config, False)
    path = Path(path)
    if path.is_dir():
        path = path / "config.yaml"
    elif path.is_file() and path.name != "config.yaml":
        raise ValueError(f"Cannot open config file {path.absolute()}!")
    config = OmegaConf.load(path)
    configs = [base_config, config]
    if overrides is not None:
        configs.append(overrides)
    config = OmegaConf.merge(*configs)
    config.video.path = str(path.parent / config.video.path)
    return config


def get_module_path(module: str = "mona") -> Path:
    """Get the file path of a module."""
    return Path(
        importlib.machinery.PathFinder().find_module(module).get_filename()
    ).parent


def get_transform_to_scenario(
    map_scenario: Scenario, source_origin: np.ndarray, rotation: float = 0.0
) -> pyproj.Transformer:
    """Get a transformer object to convert from trajectory to scenario coordinates.

    :param map_scenario: CommonRoad scenario containing the map
    :param source_origin: Origin of the trajectories
    :return: pyproj.Transformer from trajectory coordinates to scenario coordinates
    """
    map_proj_string = map_scenario.location.geo_transformation.geo_reference
    match = re.findall(r"lat_0=(\d+\.\d+)\s.*lon_0=(\d+\.\d+)", map_proj_string)[0]
    map_proj_string = f"+proj=tmerc +lat_0={match[0]} +lon_0={match[1]} +datum=WGS84"
    rot = scipy.spatial.transform.Rotation.from_euler("z", rotation).as_matrix()

    trans = Transformer.from_pipeline(
        f"+proj=pipeline +step +proj=affine +xoff={source_origin[0]} +yoff="
        f"{source_origin[1]} +step +proj=utm inv +zone=32 +step {map_proj_string} "
        f"+step +proj=affine +s11={rot[0, 0]} +s12={rot[0, 1]} +s21="
        f"{rot[1, 0]} +s22={rot[1, 1]}"
    )
    return trans


def _draw_trajectory(
    axs: matplotlib.axes.Axes, trajectory: np.ndarray, label: Optional[str] = None
) -> None:
    labels = ["x [m]", "y [m]", "v [m/s]", "psi [rad]", "yaw rate [rad/s]"]
    axs[0].set_xlabel("x [m]")
    axs[0].set_ylabel("y [m]")
    axs[0].plot(trajectory[:, 0], trajectory[:, 1], label=label)
    axs[0].legend()
    for i, ax in enumerate(axs[1:]):
        ax.set_xlabel("t [s]")
        ax.set_ylabel(labels[i])
        ax.plot(trajectory[:, i], label=label)
        ax.legend()

import logging
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Optional, Sequence, List, Union, TypeVar, Generic, Dict

import cv2
import numba
import numpy as np
import scipy.spatial
from omegaconf import DictConfig
from shapely import geometry

from mona.camera_model import CameraModel
from mona.detection.bounding_box import BoundingBox
from mona.detection.detection import Detection, DetectionLabel
from mona.pose_estimation.pose import Pose
from mona.smoothing.trajectory import Trajectory
from mona.utils.util import Color

T = TypeVar("T")
K = TypeVar("K")
logger = logging.getLogger(__name__)


@numba.njit()
def _is_inside_sm(polygon: np.ndarray, point: np.ndarray) -> int:
    """Determine if a point is inside a polygon.

    from https://stackoverflow.com/questions/36399381/whats-the-fastest-way-of-checking
    -if-a-point-is-inside-a-polygon-in-python

    .. warning::
        Requires closed polygon!

    :param polygon:
    :param point:
    :return:
    """
    length = len(polygon) - 1
    dy2 = point[1] - polygon[0][1]
    intersections = 0
    ii = 0
    jj = 1

    while ii < length:
        dy = dy2
        dy2 = point[1] - polygon[jj][1]

        # consider only lines which are not completely above/bellow/right from the point
        if dy * dy2 <= 0.0 and (
            point[0] >= polygon[ii][0] or point[0] >= polygon[jj][0]
        ):

            # non-horizontal line
            if dy < 0 or dy2 < 0:
                F = dy * (polygon[jj][0] - polygon[ii][0]) / (dy - dy2) + polygon[ii][0]

                if point[0] > F:
                    # if line is left from the point - the ray moving towards left,
                    # will intersect it
                    intersections += 1
                elif point[0] == F:  # point on line
                    return 2

            # point on upper peak (dy2=dx2=0) or horizontal line
            # (dy=dy2=0 and dx*dx2<=0)
            elif dy2 == 0 and (
                point[0] == polygon[jj][0]
                or (
                    dy == 0
                    and (point[0] - polygon[ii][0]) * (point[0] - polygon[jj][0]) <= 0
                )
            ):
                return 2

        ii = jj
        jj += 1

    # print 'intersections =', intersections
    return intersections & 1


@numba.njit(parallel=False)
def _is_inside_sm_parallel(points: np.ndarray, polygon: np.ndarray) -> np.ndarray:
    ln = len(points)
    result = np.empty(ln, dtype=numba.boolean)
    for i in numba.prange(ln):
        result[i] = _is_inside_sm(polygon, points[i])
    return result


class Zone:
    """Representation of a detection zone as polygon in pixel coordinates."""

    corners = np.array(np.meshgrid([-0.5, 0.5], [-0.5, 0.5])).T.reshape(-1, 2)

    @staticmethod
    def _box_to_poly(bounding_box: BoundingBox) -> geometry.Polygon:
        """Converts a bounding box object into a shapely polygon.

        :param bounding_box: Bounding box
        :return: Bounding box rectangle as shapely polygon
        """
        bbox = bounding_box.corners
        bbox = geometry.Polygon(bbox)
        return bbox

    def __init__(self, detection_poly_points: np.ndarray):
        """
        Create a zone.

        :param detection_poly_points: (N, 2) array of polygon vertices
        """
        detection_poly_points = np.array(detection_poly_points)
        self.detection_poly = geometry.Polygon(detection_poly_points)
        self.poly_points = detection_poly_points
        # Close polygon
        if np.any(self.poly_points[0] != self.poly_points[-1]):
            self.poly_points = np.append(
                self.poly_points, self.poly_points[0].reshape(1, 2), axis=0
            )

    def within(self, obj: Detection) -> bool:
        """Check whether a detection object is enclosed within the detection zone.

        :param obj: Detection object
        :return: True, if the object is within the detection zone.
        """
        bbox = self._box_to_poly(obj.det_2Dbox)
        within = bbox.within(self.detection_poly)
        return within

    def intersects(self, obj: Detection) -> bool:
        """Checks whether a detection object intersects with the detection zone.

        :param obj: Detection object
        :return: True, if the object intersects the detection zone.
        """
        corners = obj.det_2Dbox.corners
        return np.any(self.point_within(corners))

    def rectangle_intersects(self, obj: np.ndarray) -> bool:
        """Checks whether a rectangle is completely inside the detection zone.

        :param obj: (N, 4) array containing x, y, orientation, length, width of the
            rectangles
        :return: (N,) array of type bool
        """
        obj = obj.reshape(-1, 5)
        rot_mat = scipy.spatial.transform.Rotation.from_euler(
            "z", -obj[:, 2]
        ).as_matrix()[:, :-1, :-1]
        corners = np.tile(Zone.corners, (obj.shape[0], 1))
        rects = np.repeat(obj, repeats=Zone.corners.shape[0], axis=0)
        corners *= rects[:, 3:]
        corners = np.matmul(corners.reshape((-1, 4, 2)), rot_mat).reshape(-1, 2)
        corners += rects[:, :2]
        within = self.point_within(corners)
        within.shape = (-1, 4)
        return np.any(within, axis=1)

    def point_within(self, obj: np.ndarray) -> np.ndarray:
        """Checks if a point is within the detection zone.

        Also works with multiple points in (N, 2) array.

        :param obj: (2,) or (N, 2) array of points
        :return: Boolean or array of Booleans, true if the corresponding points are
            within the zone.
        """
        return np.squeeze(
            _is_inside_sm_parallel(obj.reshape((-1, 2)), self.poly_points)
        )

    def bbox_overlap_percentage(self, obj: Detection) -> float:
        """
        Get the intersection percentage of the detection with the zone.

        :param obj: Detection object
        :return: intersection area / detection object area
        """
        bbox = self._box_to_poly(obj.det_2Dbox)
        intersection_area = self.detection_poly.intersection(bbox).area
        if intersection_area == 0.0:
            return 0.0
        return intersection_area / bbox.area

    def display_on_image(
        self, image: np.ndarray, color: Color = (0, 0, 255), thickness: int = 1
    ) -> np.ndarray:
        """Displays the detection zone on the image.

        :param image: (H, W, 3) image in BGR format
        :param color: Line color (BGR format)
        :param thickness: Line thickness
        :return: Modified image (H, W, 3) in BGR format
        """
        pt_img_np = np.array(self.detection_poly.boundary.coords).reshape((-1, 1, 2))
        pt_img_np = pt_img_np.astype(int)

        cv2.polylines(image, [pt_img_np], True, color, thickness=thickness)

        return image


class FilterIterMixin(ABC, Generic[T]):
    """Mixin for applying filter operations to lists and dicts."""

    def filter_list(self, in_list: List[T]) -> List[T]:
        """Apply filter to the given list."""
        return [elem for elem in in_list if self(elem)]

    def filter_dict(self, in_dict: Dict[K, T]) -> Dict[K, T]:
        """Apply filter to the given dictionary values."""
        out_dict = {}
        for key, val in in_dict.items():
            if isinstance(val, dict):
                out_dict[key] = self.filter_dict(val)
            elif self(val):
                out_dict[key] = val
        return out_dict

    def __call__(
        self, obj: Union[List[T], Dict[K, T], T]
    ) -> Union[List[T], Dict[K, T], bool]:
        """Apply filter to lists, dicts, and objects."""
        if isinstance(obj, dict):
            return self.filter_dict(obj)
        elif isinstance(obj, (list, tuple)):
            return self.filter_list(obj)
        else:
            return self.apply_filter(obj)

    @abstractmethod
    def apply_filter(self, arg: T) -> bool:
        """
        Apply the filter.

        :param arg: Object to apply filter to
        :return: True, if the object should be kept.
        """
        pass


class DetectionZone(FilterIterMixin[Union[Detection, Pose]]):
    """Filter objects, if they overlap with the detection zone."""

    @classmethod
    def create_in_world_frame(
        cls,
        detection_poly: np.ndarray,
        ignore_polys: Optional[Sequence[np.ndarray]],
        cam_model: CameraModel,
    ) -> "DetectionZone":
        """
        Create a detection zone filter in world coordinates.

        :param detection_poly: (N, 2) vertices of the detection zone polygon
        :param ignore_polys: List of ignore zone polygons
        :param cam_model: Camera model to translate form image to world coordinates
        :return: Detection zone filter in world coordinates
        """
        polys = [
            cam_model.backproject_pixels(np.array(p))[:, :-1]
            for p in [detection_poly] + list(ignore_polys)
        ]
        return cls(polys[0], polys[1:])

    @classmethod
    def create_from_config(cls, config: DictConfig) -> "DetectionZone":
        """
        Create a detection zone filter from a config object.

        :param config: Configuration object
        :return: Detection zone filter
        """
        location = config["video"]["location"]
        loc_config = config["locations"][location]
        points = loc_config["detection_zone"]
        polys = [
            np.array(p) for p in [points] + list(loc_config.get("ignore_zones", []))
        ]
        ignore_iou_threshold = loc_config.get("ignore_iou_threshold", 0.4)
        return cls(polys[0], polys[1:], ignore_iou_threshold)

    def __init__(
        self,
        detection_poly: np.ndarray,
        ignore_polys: Sequence[np.ndarray] = tuple(),
        overlap_threshold: float = 0.4,
    ):
        """
        Create a detection zone filter.

        :param detection_poly: (N, 2) vertices of the detection zone polygon
        :param ignore_polys: List of ignore zone polygons
        :param overlap_threshold: Overlap percentage threshold. If overlap percentage
            with an ignore zone is higher than the threshold, an object is filtered.
        """
        self.detection_zone = Zone(detection_poly)
        self.ignore_zones = [Zone(p) for p in ignore_polys]
        self.overlap_threshold = overlap_threshold

    def display_on_image(
        self,
        image: np.ndarray,
        color: Color = (0, 0, 255),
        thickness: int = 1,
        draw_ignore_zone: bool = False,
    ) -> np.ndarray:
        """
        Display detection on camera image.

        :param image: (H, W, 3) camera image in BGR-format
        :param color: Color of the detection polygon in BGR-format
        :param thickness: Thickness of the detection zone polygon
        :param draw_ignore_zone: Draw also the ignore zones
        :return: (H, W, 3) modified camera image in BGR-format
        """
        image = self.detection_zone.display_on_image(image, color, thickness)
        if draw_ignore_zone:
            for z in self.ignore_zones:
                image = z.display_on_image(image, (0, 255, 0), thickness)
        return image

    def intersects(self, obj: Detection) -> bool:
        """
        Check if a detection object intersects with the detection zone.

        :param obj: Detection object
        :return: True if the object's bbox intersects with the detection zone and does
            not overlap with any ignore zone by more than the threshold.
        """
        in_det_zone = self.detection_zone.intersects(obj)
        if len(self.ignore_zones) > 0:
            ignore_overlap = np.max(
                [z.bbox_overlap_percentage(obj) for z in self.ignore_zones]
            )
        else:
            ignore_overlap = 0.0
        return in_det_zone and ignore_overlap < self.overlap_threshold

    def apply_filter(self, obj: Union[Detection, Pose]) -> bool:
        """
        Check if an object is in the detection zone.

        If the object is a :py:class:`~mona.detection.Detection` the result is equal to
        :py:meth:`~mona.filtering.DetectionZone.intersects`. If the object is a
        :py:class:`~mona.pose_estimation.Pose`, we check if the world coordinate
        position is within the detection zone.

        :param obj: Detection or pose object
        :return: True if the object should be kept.
        """
        if isinstance(obj, Detection):
            return self.intersects(obj)
        elif isinstance(obj, Pose):
            return bool(self.detection_zone.point_within(np.array([obj.x, obj.y])))
        else:
            raise TypeError()


@dataclass
class CompoundFilter(FilterIterMixin[T]):
    """Concatenation of multiple filters.

    An object is accepted if it is accepted by all containing filters.
    """

    filters: List[FilterIterMixin[T]]

    def apply_filter(self, arg: T) -> bool:
        """
        Apply the filter.

        :param arg: Object to apply filter to
        :return: True, if the object should be kept.
        """
        for filt in self.filters:
            keep: bool = filt(arg)
            if not keep:
                logger.debug("%s: filtered due to %s", arg, str(type(filt)))
                return False
        return True


class DetectionTypeFilter(FilterIterMixin[Union[Detection, Trajectory]]):
    """Only keep detections with specific detection type."""

    def __init__(self, allowed_labels: List[str]):
        """
        Create a DetectionTypeFilter.

        :param allowed_labels: List of allowed detection label values
        """
        self.allowed_labels = {DetectionLabel[label] for label in allowed_labels}

    def apply_filter(self, obj: Union[Detection, Trajectory]) -> bool:
        """
        Apply the filter.

        :param obj: Object to apply filter to
        :return: True, if the object should be kept.
        """
        if hasattr(obj, "label"):
            label = obj.label
        else:
            label = obj.vehicle_type
        return label in self.allowed_labels


@dataclass
class DetectionAreaFilter(FilterIterMixin[Detection]):
    """Only keep detections with a bbox area in a specified range."""

    min_area: float = -np.inf
    max_area: float = np.inf

    def apply_filter(self, obj: Detection) -> bool:
        """
        Apply the filter.

        :param obj: Object to apply filter to
        :return: True, if the object should be kept.
        """
        box_area = obj.det_2Dbox.area
        result = self.min_area <= box_area <= self.max_area
        if not result:
            logger.debug(
                "ID %d: %f <= %f <= %f",
                obj.det_id,
                self.min_area,
                box_area,
                self.max_area,
            )
        return result


@dataclass
class PoseSizeFilter(FilterIterMixin[Union[Pose, Trajectory]]):
    """Only keep poses with length, width, and height in a specified range."""

    max_length: float = np.inf
    min_length: float = -np.inf
    max_width: float = np.inf
    min_width: float = -np.inf
    max_height: float = np.inf
    min_height: float = -np.inf

    def apply_filter(self, pose: Union[Pose, Trajectory]) -> bool:
        """
        Apply the filter.

        :param pose: Object to apply filter to
        :return: True, if the object should be kept.
        """
        if hasattr(pose, "box"):
            length = pose.box.length
            width = pose.box.width
            height = pose.box.height
        else:
            length = pose.length
            width = pose.width
            height = 0.0
        return (
            self.min_length <= length <= self.max_length
            and self.min_width <= width <= self.max_width
            and self.min_height <= height <= self.max_height
        )


@dataclass
class TrajectoryLength(FilterIterMixin[Trajectory]):
    """Only keep trajectories with a number of states in a specified range."""

    min_num_states: int
    max_num_states: int

    def apply_filter(self, trajectory: Trajectory) -> bool:
        """
        Apply the filter.

        :param trajectory: Object to apply filter to
        :return: True, if the object should be kept.
        """
        return self.min_num_states <= len(trajectory.traj_points) <= self.max_num_states


@dataclass
class TrajectoryMeanVelocity(FilterIterMixin[Trajectory]):
    """Only keep trajectories with a mean velocity in a specified range."""

    min_velocity: float
    max_velocity: float

    def apply_filter(self, trajectory: Trajectory) -> bool:
        """
        Apply the filter.

        :param trajectory: Object to apply filter to
        :return: True, if the object should be kept.
        """
        return (
            self.min_velocity <= np.mean(trajectory.traj_points.v) <= self.max_velocity
        )


@dataclass
class TrajectoryMaxAbsVelocity(FilterIterMixin[Trajectory]):
    """Only keep trajectories with a absolute velocity less than or equal to a value."""

    max_velocity: float

    def apply_filter(self, trajectory: Trajectory) -> bool:
        """
        Apply the filter.

        :param trajectory: Object to apply filter to
        :return: True, if the object should be kept.
        """
        return np.max(np.abs(trajectory.traj_points.v)) <= self.max_velocity


@dataclass
class TrajectoryStationary(FilterIterMixin[Trajectory]):
    """Only keep trajectories with a minimum displacement."""

    stationary_threshold: float

    def apply_filter(self, trajectory: Trajectory) -> bool:
        """
        Apply the filter.

        :param trajectory: Object to apply filter to
        :return: True, if the object should be kept.
        """
        dist = (
            (trajectory.traj_points[0].x - trajectory.traj_points[-1].x) ** 2
            + (trajectory.traj_points[0].y - trajectory.traj_points[-1].y) ** 2
        ) ** 0.5
        return dist >= self.stationary_threshold


@dataclass
class TrajectoryTraveledDistance(FilterIterMixin[Trajectory]):
    """Only keep trajectories with a minimum traveled distance."""

    traveled_distance_threshold: float

    def apply_filter(self, trajectory: Trajectory) -> bool:
        """
        Apply the filter.

        :param trajectory: Object to apply filter to
        :return: True, if the object should be kept.
        """
        diff = np.column_stack(
            (np.diff(trajectory.traj_points.x), np.diff(trajectory.traj_points.y))
        )
        dist = np.sum(np.linalg.norm(diff, axis=1))
        return dist >= self.traveled_distance_threshold

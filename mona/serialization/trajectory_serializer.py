from pathlib import Path
from typing import Optional, Union, Iterable

import numpy as np
import pandas as pd

from mona.smoothing.trajectory import Trajectory


class TrajectorySerializer:
    """Serializer for trajectory objects."""

    def __init__(
        self,
        trajectories: Optional[Iterable[Trajectory]] = None,
        dataframe: Optional[pd.DataFrame] = None,
    ):
        """Create serializer object.

        :param trajectories: Optional initialization with trajectories
        :param dataframe: Optional initialization with serialized trajectories in a
        dataframe
        """
        self._df: Optional[pd.DataFrame] = dataframe or pd.DataFrame(
            columns=["track_id"] + Trajectory.columns, dtype=np.int64
        )
        if trajectories is not None:
            dfs = [self._df]
            for t in trajectories:
                df = t.dataframe.round(6)
                df["track_id"] = t.traj_id
                dfs.append(df)
            self._df = pd.concat(dfs)

    @property
    def df(self) -> pd.DataFrame:
        """Get a post-processed copy of the serialized trajectory.

        :return: Dataframe copy
        """
        return self._post_process()

    def serialize_trajectory(self, traj: Trajectory) -> None:
        """Serialize a trajectory.

        :param traj: Trajectory to serialize
        """
        df = traj.dataframe.round(6)
        df["track_id"] = traj.traj_id
        if self._df is None:
            self._df = df
        else:
            self._df = self._df.append(df)

    def save(self, file_path: Union[Path, str]) -> None:
        """Save serialized trajectories to file.

        :param file_path: Path to output file.
        """
        if self._df is not None:
            df = self._post_process()
            if isinstance(file_path, str):
                file_path = Path(file_path)
            if file_path.suffix == ".csv":
                df.to_csv(file_path, index=False)
            elif file_path.suffix == ".feather":
                df.to_feather(file_path)
            elif file_path.suffix == ".hd5":
                df.to_hdf(str(file_path), key="trajectories", format="table")
            elif file_path.suffix == ".parquet":
                df.to_parquet(file_path)
            else:
                raise ValueError(f"Unknown format: '{file_path.suffix}'!")

    def _post_process(self) -> pd.DataFrame:
        assert self._df is not None
        df = self._df.copy()
        df.sort_values(["frame_id", "track_id"], inplace=True)
        return df


class InteractionTrajectorySerializer(TrajectorySerializer):
    """Serializer with columns, compatible to the INTERACTION dataset."""

    def __init__(
        self,
        trajectories: Optional[Iterable[Trajectory]] = None,
        dataframe: Optional[pd.DataFrame] = None,
    ):
        """Create serializer object.

        :param trajectories: Optional initialization with trajectories
        :param dataframe: Optional initialization with serialized trajectories in a
            dataframe
        """
        super().__init__(trajectories, dataframe)

    def _post_process(self) -> pd.DataFrame:
        df = super()._post_process()
        df["timestamp_ms"] = np.round(
            ((df.timestamp_sec - df.timestamp_sec.min()) * 1000)
        ).astype(int)
        df["vx"] = df.v.values * np.cos(df.psi_rad.values)
        df["vy"] = df.v.values * np.sin(df.psi_rad.values)
        df.rename(
            columns={"time_step": "frame_id", "type": "agent_type", "theta": "psi_rad"},
            inplace=True,
        )
        columns = [
            "track_id",
            "frame_id",
            "timestamp_ms",
            "agent_type",
            "x",
            "y",
            "vx",
            "vy",
            "psi_rad",
            "length",
            "width",
            "v",
            "yaw_rate",
        ]
        df = df[columns]
        return df

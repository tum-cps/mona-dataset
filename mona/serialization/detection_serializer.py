from logging import getLogger
from pathlib import Path
from typing import Optional, Dict, List, Union, Any, Callable, Iterator, Tuple

import h5py
import numpy as np
import scipy
from scipy.sparse import coo_matrix

from mona.detection.bounding_box import BoundingBox, intersection_rect
from mona.detection.detection import (
    Detection,
    DetectionLabel,
)
from mona.detection.mask_util import (
    intersection_over_union_mask,
    intersection_area_mask,
)

_logger = getLogger(__name__)


class Hdf5Serializer:
    """Serializer strategy for saving detection objects to an HDF5 file.

    This object can be used as a context manager for managing the internal file object.
    Internally, an HDF5 file is created with a group ``/frames/{frame_idx}`` for each
    frame. This group contains to datasets ``detections`` and ``boxes``,
    where ``boxes`` holds the top-left and bottom-right coordinates of the bounding
    boxes and ``detections`` holds the corresponding remaining attributes.
    """

    _dtype_structure = [
        ("det_id", np.uint16),
        ("label_idx", np.uint8),
        ("confidence", np.float32),
        ("mask_dim_x", np.uint16),
        ("mask_dim_y", np.uint16),
        ("rle_mask", h5py.vlen_dtype(np.uint8)),
    ]

    def __init__(self, file_name: Union[Path, str], append: bool = True):
        """Create a serializer object.

        :param file_name: File name of output file
        :param append: If existing file should be appended or overwritten.
        """
        if isinstance(file_name, str):
            file_name = Path(file_name)
        self.mode = "a"
        if file_name.exists():
            if append:
                _logger.info("%s exists! Appending!", file_name.absolute())
            else:
                _logger.warning("%s exists! Overwriting!", file_name.absolute())
                self.mode = "w"
        self.file_name = file_name
        self.file: Optional[h5py.File] = None
        self.num_detections = 0

    def __enter__(self) -> "Hdf5Serializer":
        self.file = h5py.File(self.file_name, mode=self.mode)
        return self

    def __exit__(self, exc_type: Any, exc_val: Any, exc_tb: Any) -> None:
        assert self.file is not None
        self.file.close()

    def serialize_batch(self, detections: Dict[int, List[Detection]]) -> None:
        """Serialize detections of a batch of frames.

        :param detections: Mapping from frame index to list of detections
        """
        for item in detections.items():
            self.serialize_frame(*item)

    def serialize_frame(self, frame_idx: int, detections: List[Detection]) -> None:
        """Serialize detections in a frame.

        :param frame_idx: Frame index (group key)
        :param detections: List of detections in the frame
        """
        assert self.file is not None
        frame_ids = {int(fid) for fid in self.file.get("frames", dict()).keys()}
        if frame_idx in frame_ids:
            _logger.warning("Detections for frame %d exist! Skipping!", frame_idx)
            return
        if len(detections) == 0:
            return
        self.num_detections += len(detections)
        tuples = list(
            map(
                lambda d: (
                    d.det_id,
                    d.label.value,
                    d.confidence,
                    d.rle_mask["size"][0],
                    d.rle_mask["size"][1],
                    np.frombuffer(d.rle_mask["counts"], dtype=np.uint8),
                ),
                detections,
            )
        )
        boxes = np.stack([d.det_2Dbox.x1y1x2y2.ravel() for d in detections])
        array = np.array(tuples, dtype=self._dtype_structure)
        frame_group = self.file.create_group(f"/frames/{frame_idx}")
        frame_group.create_dataset("detections", data=array)
        frame_group.create_dataset("boxes", data=boxes)
        frame_group.attrs["time"] = detections[0].time


class Hdf5Deserializer:
    """Deserializing counter-part to Hdf5Serializer.

    Deserializes detection objects from a HDF5 file. This object can be used as a
    context manager for managing the internal file object.
    """

    def __init__(
        self,
        file_name: Union[str, Path],
        filter_fn: Optional[Callable[[Detection], bool]] = None,
        merge_threshold: float = 0.7,
    ):
        """Create a deserializer.

        :param file_name: File name of the HDF5 file.
        :param filter_fn: Filter object to filter the deserialized objects e.g. by
        detection zone.
        :param merge_threshold Overlap threshold required to merge to detections in
        the same frame.
        """
        file_name = Path(file_name)
        assert file_name.exists()
        self.file_name = file_name
        self.file: Optional[h5py.File] = None
        self.filter_fn = filter_fn
        self.merge_threshold = merge_threshold

    def __enter__(self) -> "Hdf5Deserializer":
        self.file = h5py.File(self.file_name, mode="r")
        return self

    def __exit__(self, exc_type: Any, exc_val: Any, exc_tb: Any) -> None:
        assert self.file is not None
        self.file.close()

    def __len__(self) -> int:
        return len(self.frame_ids())

    def __iter__(self) -> Iterator[Tuple[int, List[Detection]]]:
        return map(
            lambda idx: (idx, self.deserialize_frame(idx)), range(max(self.frame_ids()))
        )

    def frame_ids(self) -> List[int]:
        """Get frame ids for which detections are contained in the HDF5 file.

        :return: List of frame ids
        """
        assert self.file is not None
        frame_ids = [int(fid) for fid in self.file.get("frames", dict()).keys()]
        frame_ids.sort()
        return frame_ids

    def deserialize_frame(self, frame_idx: int) -> List[Detection]:
        """Deserializes all detections in the frame with the given index.

        :param frame_idx: Frame index of the frame to deserialize.
        :return: List of detection objects in the frame.
        """
        assert self.file is not None
        if str(frame_idx) not in self.file["/frames"].keys():
            return []
        frame_group = self.file[f"/frames/{frame_idx}"]
        ds = frame_group["detections"]
        boxes = frame_group["boxes"]
        time = frame_group.attrs["time"]
        detections = [
            Detection(
                det_id=d["det_id"],
                label=DetectionLabel(d["label_idx"]),
                confidence=d["confidence"],
                det_mask={
                    "size": (d["mask_dim_x"], d["mask_dim_y"]),
                    "counts": d["rle_mask"].tobytes(),
                },
                frame_id=frame_idx,
                time=time,
                det_2Dbox=BoundingBox(box),
            )
            for d, box in zip(ds, boxes)
        ]
        self._remap_labels(detections)
        if self.filter_fn is not None:
            detections = list(filter(self.filter_fn, detections))
        detections = _merge_detections(detections, self.merge_threshold)
        return detections

    def deserialize_frame_dict(self, frame_idx: int) -> Dict[int, Detection]:
        """Deserialize as a mapping from detection id to detection object.

        :param frame_idx: Frame index of the frame to deserialize.
        :return: Dictionary that maps from detection id to its detection object.
        """
        return {d.det_id: d for d in self.deserialize_frame(frame_idx)}

    @staticmethod
    def _remap_labels(detections: List[Detection]) -> None:
        for detection in detections:
            if detection.label == DetectionLabel.window:
                detection.label = DetectionLabel.car
            elif detection.label == DetectionLabel.train:
                detection.label = DetectionLabel.truck


def _merge_detections(
    detections: List[Detection], merge_threshold: float
) -> List[Detection]:
    """Merge overlapping detections into one.

    :param detections: List of detections in the same frame
    :param merge_threshold: Overlap threshold in [0,1]
    :return: A modified list of detection objects
    """
    det_out = []
    rows = []
    cols = []
    val = []
    for i, det_a in enumerate(detections):
        for j, det_b in enumerate(detections[i + 1 :], start=i + 1):
            if intersection_rect(det_a.det_2Dbox, det_b.det_2Dbox) > 0:
                iou = intersection_over_union_mask(det_a, det_b)
                overlap = _subset_size(det_a, det_b)
                iou = max(overlap, iou)
                _logger.debug(
                    "Frame %d: iou %.2f %d and %d",
                    det_a.frame_id,
                    iou,
                    det_a.det_id,
                    det_b.det_id,
                )
            else:
                iou = 0
            if iou > merge_threshold:
                val.append(1.0)
                rows.append(i)
                cols.append(j)
    adj_mat = coo_matrix((val, (rows, cols)), shape=(len(detections), len(detections)))
    n_comp, labels = scipy.sparse.csgraph.connected_components(adj_mat, directed=False)
    indices = np.arange(start=0, stop=len(detections))
    for i in range(n_comp):
        merge_idx = indices[labels == i]
        det = detections[merge_idx[0]]
        classes = []
        scores = []
        for j in merge_idx[1:]:
            det_j = detections[j]
            _logger.debug(
                "Frame %d: Merge detections %d and %d",
                det.frame_id,
                det.det_id,
                det_j.det_id,
            )
            det.merge(det_j)
            classes.append(det.label.value)
            scores.append(det.confidence)
        if len(classes) > 0:
            bins = np.bincount(classes, scores)
            max_class = np.argmax(bins)
            det.label = DetectionLabel(max_class)
            det.confidence = np.max(scores)
        det_out.append(det)
    return det_out


def _subset_size(det_a: Detection, det_b: Detection) -> float:
    r"""Calculate a score for set inclusion of detection masks.

    .. math::
    \\max \\{\\frac{A \\cap B}{A}, \\frac{A \\cap B}{B}\\}

    :param det_a: Detection A
    :param det_b: Detection B
    :return: Subset percentage
    """
    area_intersect = intersection_area_mask(det_a, det_b)
    return max(area_intersect / det_a.mask_area, area_intersect / det_b.mask_area)

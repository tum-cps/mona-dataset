from pathlib import Path
from typing import Tuple, Union, Optional, Iterator, List, FrozenSet

import cv2
import numpy as np
import torch
from omegaconf import DictConfig
from torch.utils.data import IterableDataset

from mona.serialization.detection_serializer import Hdf5Deserializer


class VideoDataset(IterableDataset):
    """Wrap a video file into a PyTorch dataset that can be iterated frame by frame."""

    @classmethod
    def from_config(cls, config: DictConfig, resume: bool = False) -> "VideoDataset":
        """Create a video dataset from config.

        :param config: configuration
        :param resume: Check if previous detections exist and skip frames
        :return: Created video dataset
        """
        detections_path = Path(config.output_dir) / "detections.hd5"
        skip = config["video"].get("skip", 0)
        if resume and detections_path.exists():
            deserializer = Hdf5Deserializer(detections_path)
            with deserializer:
                skip = max(deserializer.frame_ids() + [-1]) + 1
        return cls(
            config["video"]["path"],
            config["video"].get("frame_limit"),
            config["locations"][config["video"]["location"]].get("rotate", False),
            skip,
            config["video"].get("stride", 1),
            config["video"].get("scale", 1.0),
        )

    def __init__(
        self,
        path: Union[str, Path],
        frame_limit: Optional[int] = None,
        rotate: bool = False,
        skip: int = 0,
        stride: int = 1,
        scale: float = 1.0,
        skip_frame_ids: FrozenSet[int] = frozenset(),
    ):
        """Create a video dataset object.

        :param path: File path of the video file.
        :param frame_limit: Number of frames to be read from the video file.
        :param rotate: Flag, if the frame should be rotate 90 degree clockwise.
        :param skip: Number of frames to skip from the beginning of the video.
        :param stride: Number of frames to drop between each output frame.
        :param scale: Scale factor of the image size.
        :param skip_frame_ids: Frame ids to skip
        """
        super().__init__()
        assert Path(path).exists(), (
            f"Video path '{Path(path).resolve().absolute()}' does " f"not exist!"
        )
        self.path = path
        self.frame_limit = frame_limit
        self.rotate = rotate
        self.skip = skip
        self.stride = stride
        self.scale = scale
        self.skip_frame_ids = skip_frame_ids

    @property
    def frame_size(self) -> Tuple[int, int]:
        """Tuple containing width and height of the frame."""
        capture = cv2.VideoCapture(str(self.path))
        size = int(capture.get(cv2.CAP_PROP_FRAME_WIDTH)), int(
            capture.get(cv2.CAP_PROP_FRAME_HEIGHT)
        )
        if self.rotate:
            size = size[::-1]
        return np.int0(self.scale * np.array(size))

    @property
    def fps(self) -> float:
        """Frame rate of the video in frames per second."""
        capture = cv2.VideoCapture(str(self.path))
        return capture.get(cv2.CAP_PROP_FPS)

    def __len__(self) -> int:
        if self.frame_limit is not None:
            num_frames = self.frame_limit
        else:
            capture = cv2.VideoCapture(str(self.path))
            num_frames = int(capture.get(cv2.CAP_PROP_FRAME_COUNT))
        num_frames -= len(self.skip_frame_ids)
        num_frames -= self.skip
        num_frames = num_frames // self.stride
        return max(num_frames, 0)

    def __iter__(self) -> Iterator[dict]:
        capture = cv2.VideoCapture(str(self.path))
        capture.set(cv2.CAP_PROP_POS_FRAMES, self.skip)
        i = self.skip
        fps = self.fps
        while capture.isOpened() and self.frame_limit is None or i < self.frame_limit:
            # tick = time()
            assert i == capture.get(cv2.CAP_PROP_POS_FRAMES), (
                f"Expected frame {i} "
                f"but got frame "
                f"{capture.get(cv2.CAP_PROP_POS_FRAMES)}!"
            )
            ret, frame = capture.read()
            if not ret and i == 0:
                continue
            if i in self.skip_frame_ids:
                i += self.stride
                continue
            if not ret:
                break
            # Strange behavior e.g. start with negative time stamp
            # time_ms = int(capture.get(cv2.CAP_PROP_POS_MSEC))
            time_sec = i / fps
            if self.rotate:
                frame = cv2.rotate(frame, cv2.cv2.ROTATE_90_CLOCKWISE)
            if self.scale != 1.0:
                frame = cv2.resize(
                    frame,
                    (0, 0),
                    fx=self.scale,
                    fy=self.scale,
                    interpolation=cv2.INTER_AREA,
                )
            data = {
                "image": frame,
                "frame_index": i,
                "time": time_sec,
                "width": frame.shape[1],
                "height": frame.shape[0],
            }
            # print(f"Frame read time: {(time() - tick) * 1000:.2f} ms")
            yield data
            i += self.stride
            if self.stride > 1:
                capture.set(cv2.CAP_PROP_POS_FRAMES, i)


def _collate_fn(batch: List[dict]) -> List[dict]:
    for d in batch:
        image = d["image"]
        image = torch.from_numpy(image.astype(np.float32).transpose(2, 0, 1))
        d["image"] = image
    return batch


def make_dataset(config: DictConfig) -> torch.utils.data.DataLoader:
    """Create a dataset and loader for detectron2 based on the provided configuration.

    :param config: Program configuration.
    :return: A batched DataLoader object.
    """
    batch_size = config["detection"]["batch_size"]

    loader = torch.utils.data.DataLoader(
        VideoDataset.from_config(config, resume=True),
        batch_size=batch_size,
        collate_fn=_collate_fn,
        pin_memory=True,
        num_workers=1,
    )
    return loader

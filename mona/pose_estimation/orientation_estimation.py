import importlib.resources
from typing import Optional

import numpy as np
import shapely.geometry
from commonroad.common.file_reader import CommonRoadFileReader
from omegaconf import DictConfig

from mona.utils.util import get_transform_to_scenario


class MapOrientationEstimator:
    """Strategy to estimate a vehicle orientation from the lanelet orientation."""

    @classmethod
    def create_from_config(cls, config: DictConfig) -> "MapOrientationEstimator":
        """Create a MapOrientationEstimator from a config object."""
        map_path = config.location.map_file
        origin = config.location.camera.origin
        return cls(map_path, origin)

    def __init__(self, map_name: str, origin: np.ndarray):
        """
        Create a MapOrientationEstimator

        :param map_name: File name of the CommonRoad scenario with the respective
            lanelet network
        :param origin: UTM coordinates of the world frames origin
        """
        with importlib.resources.path("mona.maps", map_name) as map_path:
            scn, _ = CommonRoadFileReader(str(map_path)).open()
        center_lines = [
            lanelet.center_vertices for lanelet in scn.lanelet_network.lanelets
        ]
        trans = get_transform_to_scenario(scn, np.array(origin))
        # Transform into coordinate systems
        center_lines = [
            np.array(
                trans.transform(
                    center_line[:, 0], center_line[:, 1], direction="INVERSE"
                )
            ).T
            for center_line in center_lines
        ]
        self.center_ccs = [
            shapely.geometry.LineString(center_line) for center_line in center_lines
        ]
        self.segment_start_lon = [
            np.array(
                [
                    center_line.project(shapely.geometry.Point(p))
                    for p in center_line.coords
                ]
            )
            for center_line in self.center_ccs
        ]
        self.lane_orientations = [
            np.arctan2(
                np.diff(center_line, axis=0)[:, 1], np.diff(center_line, axis=0)[:, 0]
            )
            for center_line in center_lines
        ]

    def get_lane_orientation(self, ground_pos: np.ndarray) -> Optional[float]:
        """
        Get the orientation of the closest lanelet center line at the projected point.

        :param ground_pos: [2,] world coordinates
        :return: Orientation in radian
        """
        ground_pos = shapely.geometry.Point(ground_pos)
        curvi = [center_line.distance(ground_pos) for center_line in self.center_ccs]
        curvi = np.array(curvi)
        lane_idx = np.argmin(np.abs(curvi))
        ccs = self.center_ccs[lane_idx]
        proj = ccs.project(ground_pos)
        segment = np.argmax(self.segment_start_lon[lane_idx] >= proj)
        segment = np.fmin(segment, len(self.lane_orientations[lane_idx]) - 1)
        return self.lane_orientations[lane_idx][int(segment)]

    def correct_orientation(self, ground_pos: np.ndarray, yaw_angle: float):
        """
        Heurstic approach to adjust an orientation.

        The input orientation may include a 180° error. The angle difference between
        the closest lanelet orientation is checked. If the angle difference is greater
        than 90°, the orientation is flipped by 180° such that it is closer to the
        driving direction. Additionally, a sanity check is performed: If the angle
        difference between the lanelet orientation and the provided angle is larger
        than 50° we assume an invalid measurement and the returned value is set to the
        lanelet orientation.

        :param ground_pos: Position at which the yaw angle occurs.
        :param yaw_angle: Yaw angle
        :return: Corrected yaw angle
        """
        lane_orientation = self.get_lane_orientation(ground_pos)
        if lane_orientation is not None:
            # Compare estimated orientation with lane direction
            # If estimated orientation is more than 90 deg different, flip by 180 deg
            yaw_angle = (
                yaw_angle
                if np.abs(yaw_angle - lane_orientation) < np.pi / 2
                else np.arctan2(np.sin(yaw_angle + np.pi), np.cos(yaw_angle + np.pi))
            )
            # Now check for sanity
            yaw_angle = (
                yaw_angle
                if np.abs(yaw_angle - lane_orientation) < np.deg2rad(50)
                else lane_orientation
            )
        return yaw_angle

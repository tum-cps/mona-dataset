import logging
from abc import ABC, abstractmethod
from typing import Optional

import numpy as np
from cv2 import cv2
from omegaconf import DictConfig
from sklearn.linear_model import RANSACRegressor

from mona.detection.detection import Detection, DetectionLabel
from mona.camera_model import CameraModel
from .orientation_estimation import MapOrientationEstimator
from .shape_fit import (
    _ray_trace_polyline,
    LShapeEstimator,
    IShapeEstimator,
)
from .pose import BoxSize, Pose

logger = logging.getLogger(__name__)


class PoseEstimator(ABC):
    """Interface for pose estimation strategy of a detection object."""

    default_box_size = {
        DetectionLabel.car: BoxSize(4.0, 1.8, 1.6),
        DetectionLabel.bus: BoxSize(12.0, 2.6, 2.5),
        DetectionLabel.truck: BoxSize(6.0, 2.4, 2.0),
        DetectionLabel.motorcycle: BoxSize(1.2, 0.8, 1.6),
    }

    def __init__(self, cam_model: CameraModel):
        """Initialize pose estimator.

        :param cam_model: Camera model to translate from image to world coordinates
        """
        self.cam_model = cam_model

    @abstractmethod
    def estimate_pose(self, detection: Detection) -> Pose:
        """Estimates the pose of a detection object.

        :param detection: Detection object for pose estimation
        :return: Pose of the detection
        """
        pass


class BoxCenterEstimator(PoseEstimator):
    """Simple implementation for a pose estimation strategy.

    The position of the vehicle is estimated by projecting the center of the bounding
    box in the image plane to the ground plane via the camera model. The length and
    width of the vehicle are estimated by measuring the distance between the
    projected middle points of the minimum-area rotated bounding box. The longer
    distance is taken as the length and the shorter as the width of the vehicle. The
    height is estimated by a default value for the respective vehicle class e.g. car,
    truck, etc.
    """

    @classmethod
    def create_from_config(
        cls, config: DictConfig, cam_model: CameraModel = None
    ) -> "BoxCenterEstimator":
        """
        Create estimator from configuration.

        :param config: Configuration object
        :param cam_model: Camera model
        :return: Estimator object
        """
        if cam_model is None:
            cam_model = CameraModel.create_from_config(config)
        orientation_estimator = MapOrientationEstimator.create_from_config(config)
        return cls(cam_model, orientation_estimator)

    def __init__(
        self, cam_model: CameraModel, map_orientation_estimator: MapOrientationEstimator
    ):
        """Initialize pose estimator.

        :param cam_model: Camera model to translate from image to world coordinates
        :param map_orientation_estimator: Estimator for object orientation from map
        """
        super().__init__(cam_model)
        self.orientation_estimator = map_orientation_estimator

    def estimate_pose(self, detection: Detection) -> Pose:
        """Estimates the pose of a detection object.

        :param detection: Detection object for pose estimation
        :return: Pose of the detection
        """
        default_box_size = self.default_box_size.get(
            detection.label, self.default_box_size[DetectionLabel.car]
        )

        # Center position
        pix_center = detection.det_2Dbox.center
        ground_pos = self.cam_model.backproject_pixels(pix_center, 0)
        ground_pos = ground_pos.squeeze()

        # Box size
        box = detection.min_area_2d_box
        box = box.reshape(4, 2)
        box = [
            0.5 * (box[0] + box[1]),
            0.5 * (box[2] + box[3]),
            0.5 * (box[1] + box[2]),
            0.5 * (box[0] + box[-1]),
        ]
        ground_points = [
            np.squeeze(self.cam_model.backproject_pixels(p, 1.5)) for p in box
        ]
        side_a = np.linalg.norm(ground_points[0] - ground_points[1])
        side_b = np.linalg.norm(ground_points[2] - ground_points[3])
        length = max(side_a, side_b)
        width = min(side_a, side_b)
        if length == side_a:
            dir = ground_points[0] - ground_points[1]
        else:
            dir = ground_points[2] - ground_points[3]
        psi = np.arctan2(dir[1], dir[0])
        psi = self.orientation_estimator.correct_orientation(ground_pos, psi)
        return Pose(
            det_id=detection.det_id,
            pixel_u=pix_center[0],
            pixel_v=pix_center[1],
            x=ground_pos[0],
            y=ground_pos[1],
            psi=psi,
            frame_id=detection.frame_id,
            time=detection.time,
            box=BoxSize(length, width, default_box_size.height),
        )


class LShapePoseEstimator(PoseEstimator):
    """Pose estimation by matching an L-shape to the vehicle's lower contours.

    See the paper for further details.
    """

    def estimate_pose(self, detection: Detection) -> Optional[Pose]:
        """Estimates the pose of a detection object.

        :param detection: Detection object for pose estimation
        :return: Pose of the detection
        """
        mask = detection.det_mask.astype(np.uint8)
        contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        contours = np.array(contours[0]).reshape((-1, 2))
        if len(contours) < 10:
            return None
        bottom_contour = self._get_bottom_contour(contours)
        if len(bottom_contour) <= 10:
            return None

        estimator = LShapeEstimator(self.cam_model.camera_position[:2])
        reg = RANSACRegressor(
            base_estimator=estimator,
            loss=lambda x, y: y,
            min_samples=round(len(bottom_contour) * 0.8),
            max_trials=100,
            residual_threshold=0.25,
        )
        # y argument does not do anything but needs to be there
        reg.fit(bottom_contour, bottom_contour[:, 1])
        score_l = reg.estimator_.best_score_

        i_shape_estimator = IShapeEstimator()
        i_shape_estimator.fit(bottom_contour, None)
        score_i = i_shape_estimator.singular_value_
        score = score_l / score_i

        if score > 0.6:
            return None

        inliers = bottom_contour[reg.inlier_mask_]
        angles = reg.estimator_.angles(inliers)
        inliers_1 = inliers[angles <= reg.estimator_.sep_angle_]
        inliers_2 = inliers[angles >= reg.estimator_.sep_angle_]

        r, u, v, w = reg.estimator_.get_line()

        lmbda = np.dot(inliers_1 - r, u) / np.dot(u, u)
        p0 = r + u * np.min(lmbda)
        p1 = r + u * np.max(lmbda)

        lmbda = np.dot(inliers_2 - v, w) / np.dot(w, w)
        p2 = v + w * np.min(lmbda)
        p3 = v + w * np.max(lmbda)

        p = np.stack((p0, p1, p2, p3))

        intersect = LShapePoseEstimator._intersection(r, u, v, w)

        dist = np.linalg.norm(p - intersect, axis=1)
        a = np.max(dist[:2])
        b = np.max(dist[2:])

        len_a = a
        len_b = b
        u = p1 - intersect
        u /= np.linalg.norm(u)
        w = p3 - intersect
        w /= np.linalg.norm(w)

        length = np.fmin(np.fmax(len_a, len_b), 18.0)
        width = np.fmin(np.fmin(len_a, len_b), 2.55)
        box = BoxSize(
            length=length,
            width=width,
            height=(
                self.default_box_size.get(detection.label)
                or self.default_box_size[DetectionLabel.car]
            ).height,
        )
        p_sorted = p[np.argsort(dist)]
        heading = np.arctan2(*((p_sorted[-1] - intersect)[::-1]))
        heading_orthogonal = np.arctan2(*((p_sorted[-2] - intersect)[::-1]))
        center = (
            intersect
            + (
                np.array([np.cos(heading), np.sin(heading)]) * length
                + np.array([np.cos(heading_orthogonal), np.sin(heading_orthogonal)])
                * width
            )
            * 0.5
        )

        if not detection.det_2Dbox.within(
            self.cam_model.project_points(center.reshape(1, 2))
        ):
            return None

        pixel_pos = self.cam_model.project_points(np.append(center, 0))

        pose = Pose(
            x=float(center[0]),
            y=float(center[1]),
            psi=heading,
            box=box,
            pixel_u=pixel_pos[0],
            pixel_v=pixel_pos[1],
            det_id=detection.det_id,
            frame_id=detection.frame_id,
            time=detection.time,
            heading_orthogonal=heading_orthogonal,
            tracked_corner=intersect,
        )
        return pose

    @staticmethod
    def _intersection(
        r: np.ndarray, u: np.ndarray, v: np.ndarray, w: np.ndarray
    ) -> np.ndarray:
        # Cramer's rule
        t = np.linalg.det(np.column_stack((v - r, -w))) / np.linalg.det(
            np.column_stack((u, -w))
        )
        return r + t * u

    def _get_bottom_contour(self, contours: np.ndarray) -> np.ndarray:
        wbot = self.cam_model.backproject_pixels(contours, z_constraint=0.15)[:, :-1]
        directions = np.diff(wbot, axis=0, append=wbot[:1])
        bottom_contours = np.array(
            _ray_trace_polyline(self.cam_model.camera_position[:2], wbot, directions)
        )
        return bottom_contours

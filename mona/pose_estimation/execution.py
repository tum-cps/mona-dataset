import logging
import time
from pathlib import Path

import cv2
import hydra
import numpy as np
import ray
from tqdm import tqdm

from mona.camera_model import CameraModel
from mona.pose_estimation import LShapePoseEstimator
from mona.serialization.detection_serializer import Hdf5Deserializer
from mona.utils.filtering import CompoundFilter
from mona.utils.util import write_bz2_pickle

logger = logging.getLogger(__name__)


@ray.remote
class EstimatorActor:
    def __init__(self, config, detections_path):
        cv2.setNumThreads(1)
        cam_model = CameraModel.create_from_config(config)
        self.estimator = LShapePoseEstimator(cam_model)
        filters = [
            hydra.utils.instantiate(cfg) for cfg in config.filtering.pose_estimation
        ]
        compound_filter = CompoundFilter(filters)
        self.deserializer = Hdf5Deserializer(detections_path, compound_filter)
        self.deserializer.__enter__()

    def estimate_poses_frame(self, frame_idx):
        detections = self.deserializer.deserialize_frame(frame_idx)
        poses = {d.det_id: self.estimator.estimate_pose(d) for d in detections}
        poses = {k: v for k, v in poses.items() if v is not None}
        return frame_idx, poses


def estimate_pose(config):
    output_dir = Path(config["output_dir"])

    det_path = output_dir / "detections.hd5"
    pose_path = output_dir / "poses.pkl"
    if pose_path.exists() and config.get("skip_existing", False):
        logger.info(
            f"Found previous results {pose_path.absolute()}. Skipping pose estimation!"
        )
        return None
    num_threads = config.num_threads
    pool = ray.util.ActorPool(
        [EstimatorActor.remote(config, det_path) for _ in range(num_threads)]
    )

    # Only used to get the frame indices
    deserializer = Hdf5Deserializer(det_path)
    with deserializer as d:
        frame_ids = d.frame_ids()

    if config.video.get("frame_limit") is not None:
        frame_ids = np.array(frame_ids)
        frame_ids = frame_ids[frame_ids < config.video.frame_limit].tolist()
    if config.video.get("skip") is not None:
        frame_ids = np.array(frame_ids)
        frame_ids = frame_ids[frame_ids > config.video.skip].tolist()

    tick = time.perf_counter()
    it = pool.map_unordered(lambda a, v: a.estimate_poses_frame.remote(v), frame_ids)
    poses = {
        frame_idx: poses
        for frame_idx, poses in tqdm(
            it, total=len(frame_ids), desc="Pose estimation", unit="frame"
        )
    }
    tock = time.perf_counter()
    logger.info(
        "Time elapsed: %3f s, time per pose: %.3f ms",
        tock - tick,
        (tock - tick) * 1e3 / sum(map(lambda f: len(f), poses.values()), 0),
    )
    write_bz2_pickle(pose_path, poses)
    return poses

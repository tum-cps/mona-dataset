from typing import Tuple

import numba
import numba.np.extensions
import numpy as np
from sklearn.base import BaseEstimator, RegressorMixin


@numba.njit()
def _ray_trace_polyline(ray_origin, contours, directions):
    bottom_contours = []
    for p1 in contours:
        intersects = _vec_intersection(contours, directions, ray_origin, p1)
        if not intersects:
            bottom_contours.append(p1)
    return bottom_contours


@numba.njit()
def _vec_intersection(r, u, cam_pos, ref_pix):
    w = ref_pix - cam_pos
    broadcast_w = np.broadcast_to(w.reshape((-1, 2)), (r.shape[0], 2))
    t = numba.np.extensions.cross2d(
        cam_pos - r, -broadcast_w
    ) / numba.np.extensions.cross2d(u, -broadcast_w)
    intersection_pts = r + u * t.reshape(-1, 1)
    intersection_pts = intersection_pts[(t >= -1.0e-6) & (t <= (1.0 + 1.0e-6))]
    distances = intersection_pts - cam_pos
    distances = (distances * distances).sum(axis=1)
    ref_dist = cam_pos - ref_pix
    ref_dist = (ref_dist * ref_dist).sum()
    return np.any((distances - ref_dist) < -1.0e-6)


class LShapeEstimator:
    """Fit an L-shape to given contour points using least-squares algorithm."""

    def __init__(self, cam_pos: np.ndarray):
        """
        Create an LShapeEstimator

        :param cam_pos: x,y-position of the camera in world coordinates
        """
        self.coeff_ = None
        self.sep_angle_ = None
        self.best_score_ = None
        self.cam_pos = cam_pos

    def angles(self, points):
        """
        Compute the bearing angles of points form the camera position.

        :param points: [N, 2] array of points
        :return: [N,] array of angles within [0, 2*pi]
        """
        vec = points - self.cam_pos
        normed_angle = np.arctan2(vec[:, 1], vec[:, 0])
        denormed_angle = (normed_angle + 2 * np.pi) % (2 * np.pi)
        return denormed_angle

    def get_params(self, deep=False):
        """Save internal state as a dictionary."""
        return {"cam_pos": self.cam_pos}

    def set_params(self, params=None, random_state=None):
        """Restore internal state from a dictionary."""
        if params is not None:
            self.cam_pos = params["cam_pos"]

    def fit(self, xy: np.ndarray, y: np.ndarray) -> None:
        """
        Fit L-shape to a set of points.

        :param xy: [N, 2] array of points
        :param y: Ignored
        """
        best_coeff, best_score, corner_angle = self._fit_l_shape(xy)
        self.coeff_ = best_coeff
        self.sep_angle_ = corner_angle
        self.best_score_ = best_score

    def _fit_l_shape(self, xy):
        angles = self.angles(xy)
        xy = xy[np.argsort(angles)]
        angles = np.sort(angles)
        a_mat = np.append(
            xy[:, ::-1],
            np.column_stack((np.zeros_like(xy[:, 0]), np.ones_like(xy[:, 0]))),
            axis=1,
        )
        a_mat[1:, 0] *= -1
        a_mat = np.concatenate(
            (np.append(xy[0], np.array([1.0, 0.0]), axis=0).reshape((1, -1)), a_mat),
            axis=0,
        )
        a_mats = []
        for i in range(1, xy.shape[0]):
            a_mat[i, :2] = a_mat[i + 1, 1::-1]
            a_mat[i, 1] *= -1
            a_mat[i, 2:] = a_mat[i + 1, 3:1:-1]
            a_mats.append(a_mat.copy())

        s = np.linalg.svd(np.stack(a_mats), compute_uv=False)
        scores = s[:, 3]
        best_i = np.argmin(scores)
        best_score = scores[best_i]
        best_coeff = np.linalg.svd(
            a_mats[best_i], compute_uv=True, full_matrices=False
        )[2][3]
        corner_angle = angles[best_i]
        return best_coeff, best_score, corner_angle

    def score(self, xy: np.ndarray, y):
        """
        Calculate the negative MSE of a set of points to the fitted L-shape.

        :param xy: [N, 2] array of points
        :param y: Ignored
        :return: -MSE
        """
        dist = self._dist(xy)
        score = -np.mean(dist**2)
        return score

    def _dist(self, xy):
        angles = self.angles(xy)
        a_mat = np.column_stack(
            (
                np.where(angles <= self.sep_angle_, xy[:, 0], -xy[:, 1]),
                np.where(angles <= self.sep_angle_, xy[:, 1], xy[:, 0]),
                np.where(angles <= self.sep_angle_, 1, 0),
                np.where(angles <= self.sep_angle_, 0, 1),
            )
        )
        dist = (a_mat @ self.coeff_) / np.linalg.norm(self.coeff_[:2])
        return dist

    def predict(self, xy: np.ndarray) -> np.ndarray:
        """
        Calculate the squared distances of the given points to the L-shape.

        :param xy: [N, 2] array of points
        :return: [N,] array of squared L2-distance
        """
        return self._dist(xy) ** 2

    def get_line(self) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        r"""
        Get the line parameters of the L-shape in parameter form.

        :return: A tuple :math:`(r, u, v, \\hat{u})` where one line is defined as
            :math:`g: r + s \\cdot u` and the other as :math:`h: v + t \\cdot \\hat{u}`
            and where :math:`u \\cdot \\hat{u} = 0`
        """
        normed_coeff = self.coeff_ / np.linalg.norm(self.coeff_[:2])
        ud = normed_coeff[:2]
        u = ud.copy()[::-1]
        u[1] *= -1

        def c_to_pt(c, a):
            if np.abs(a[0]) > np.abs(a[1]):
                return np.array([-c / a[0], 0.0])
            else:
                return np.array([0.0, -c / a[1]])

        r = c_to_pt(normed_coeff[2], ud)
        v = c_to_pt(normed_coeff[3], u)
        return r, u, v, ud


class IShapeEstimator(BaseEstimator, RegressorMixin):
    """Fit a straight line to a set of contour points in a least-squares fashion."""

    def __init__(self):
        """Create an IShapeEstimator."""
        self.coeff_ = None
        self.singular_value_ = None

    def get_params(self, deep=True):
        """Save internal state as a dictionary."""
        return {}

    def set_params(self, **params):
        """Restore internal state from a dictionary."""
        pass

    def _distance_to_line(self, xy: np.ndarray):
        design_mat = np.append(xy, np.ones_like(xy[:, 0]), axis=1)
        distance_to_line = design_mat @ self.coeff_
        return distance_to_line

    def score(self, xy: np.ndarray, y, sample_weight=None) -> float:
        """
        Calculate the mean negative distance of all points to the fitted line.

        :param xy: [N, 2] array of points
        :param y: Ignored
        :param sample_weight: ignored
        :return: Negative mean of the distances from the given points to the line.
        """
        distance_to_line = self._distance_to_line(xy)
        return -distance_to_line.mean()

    def fit(self, xy: np.ndarray, _: np.ndarray) -> None:
        """
        Fit a line to a set of points.

        :param xy: [N, 2] array of points
        :param _: Ignored
        """
        design_mat = np.append(xy, np.ones_like(xy[:, 0:1]), axis=1)
        _, s, v = np.linalg.svd(design_mat, full_matrices=False)
        self.coeff_ = v[2] / np.linalg.norm(v[2, :2])
        self.singular_value_ = s[-1]

    def predict(self, xy: np.ndarray) -> np.ndarray:
        """
        Calculate the distances of the given points to the fitted line.

        :param xy: [N, 2] array of points
        :return: [N,] array of L2-distance of each point to the fitted line
        """
        return self._distance_to_line(xy)

import logging
from dataclasses import dataclass, field
from typing import Optional, Dict, Tuple

import cv2
import numpy as np
import scipy.spatial

from mona.detection.detection import DetectionLabel
from mona.camera_model import CameraModel

logger = logging.getLogger(__name__)


@dataclass
class BoxSize:
    """Size of an object approximated by a cuboid."""

    length: float
    width: float
    height: float


@dataclass
class Pose:
    """3D vehicle pose at one time instant."""

    """Position x"""
    x: float
    """Position y"""
    y: float
    """Heading angle"""
    psi: float
    """Size of vehicle cuboid"""
    box: BoxSize
    """Position z coordinate (0 means on the ground plane)"""
    z: float = 0.0
    """Corresponding detection id"""
    det_id: Optional[int] = None
    """Center pixel coordinate horizontal"""
    pixel_u: Optional[float] = None
    """Center pixel coordinate vertical"""
    pixel_v: Optional[float] = None
    """Frame id of occurrence"""
    frame_id: Optional[int] = None
    """Time stamp in seconds"""
    time: Optional[float] = None
    """Position of the L-shape corner"""
    tracked_corner: Optional[np.ndarray] = None
    """Direction of the secondary axis of the L-shape"""
    heading_orthogonal: Optional[float] = None

    @property
    def corners(self) -> np.ndarray:
        """Get the coordinates of each corner of the vehicle cuboid in the world frame.

        :return: 8x3 array of corner coordinates.
        """
        # Get the position of the center of the bottom side:
        bottom_center = np.array([self.x, self.y, self.z])

        rot_yaw = (
            scipy.spatial.transform.Rotation.from_euler("z", self.psi).as_matrix().T
        )

        # Generate the Corner points:
        corners = np.array(
            [
                [1, 1, 0],
                [1, -1, 0],
                [-1, -1, 0],
                [-1, 1, 0],
                [1, 1, 1.0],
                [1, -1, 1.0],
                [-1, -1, 1],
                [-1, 1, 1],
            ]
        ) * np.array(
            [
                0.5 * self.box.length,
                0.5 * self.box.width,
                self.box.height,
            ]
        )
        # Rotate yaw angle
        corners = corners @ rot_yaw
        # Translate
        corners += bottom_center
        return corners

    def project_box_image(self, cam_model: CameraModel) -> np.ndarray:
        """Project cuboid corners to pixel coordinates using the provided camera model.

        :param cam_model: Camera model used for projection
        :return: 8x2 array of corners in pixel coordinates
        """
        return cam_model.project_points(self.corners)

    def display_on_image(
        self,
        image: np.ndarray,
        cam_model: CameraModel,
        color: Tuple[int, int, int] = (255, 0, 0),
        thickness: int = 2,
    ) -> np.ndarray:
        """Draw the 3D Box in the camera image.

        :param image: NxMx2 BGR image
        :param cam_model: Camera model used for projection
        :param color: Color of the box edges
        :param thickness: Thicknes of the box edges in pixels
        :return: Annotated image.
        """
        # Project point on image plane
        pixel_corners = self.project_box_image(cam_model)

        # Draw Center of the bottom side point on image
        cv2.drawMarker(
            image, (self.pixel_u, self.pixel_v), (0, 0, 255), cv2.MARKER_CROSS
        )

        # Draw box
        cv2.polylines(
            image,
            [pixel_corners[:4]],
            isClosed=True,
            color=color,
            thickness=thickness,
        )

        if hasattr(self, "bot_contour"):
            bot_contour = cam_model.project_points(self.bot_contour)
            cv2.polylines(
                image, [bot_contour], isClosed=False, color=(0, 0, 255), thickness=2
            )

        image = cv2.putText(
            image,
            f"D: {self.det_id}",
            (self.pixel_u, self.pixel_v),
            cv2.FONT_HERSHEY_PLAIN,
            color=(0, 0, 255),
            fontScale=2,
        )
        return image


@dataclass
class PoseTrack:
    """Sequence of poses of one object in multiple frames."""

    track_id: int
    """Map from frame_index to pose"""
    poses: Dict[int, Pose] = field(default_factory=dict)
    vehicle_type: DetectionLabel = DetectionLabel.car
    _box_size: Optional[BoxSize] = None

    def add_pose(self, frame_id: int, pose: Pose) -> None:
        """Add a new pose the track.

        :param frame_id: Frame id of the new pose.
        :param pose: Pose object
        """
        if frame_id in self.poses:
            logger.debug("Replacing pose in frame %d", frame_id)
        self.poses[frame_id] = pose

    @property
    def box_size(self) -> BoxSize:
        """Get the median of the box siz throughout the entire track."""
        if self._box_size is None:
            width = float(np.median([p.box.width for p in self.poses.values()]))
            length = float(np.median([p.box.length for p in self.poses.values()]))
            height = float(np.median([p.box.height for p in self.poses.values()]))
            self._box_size = BoxSize(length, width, height)
        return self._box_size

    def k_closest_box_size(self, position, k=100) -> BoxSize:
        """Compute the mean box size for the k-closest poses to the given point.

        :param position: Reference position
        :param k: Number of the closest poses to consider
        :return: Mean box size
        """
        if self.__len__() > 0:
            distances = np.linalg.norm(
                np.array([[p.x, p.y] for p in self.poses.values()]) - position, axis=1
            )
            order = np.argsort(distances)[: np.fmin(k, len(distances))]
            length = np.array([p.box.length for p in self.poses.values()])[order].mean()
            width = np.array([p.box.width for p in self.poses.values()])[order].mean()
            height = np.array([p.box.height for p in self.poses.values()])[order].mean()
            self._box_size = BoxSize(length, width, height)
            return self._box_size

    @property
    def start_time_step(self):
        """Time step of first occurrence of the tracked object."""
        return min(self.poses.keys(), default=0)

    @property
    def end_time_step(self):
        """Time step of last occurrence of the tracked object."""
        return max(self.poses.keys(), default=-1)

    @property
    def positions(self):
        """Get the positions of the object in world frame.

        Intermediate time steps with no pose found will be linearly interpolated.

        :return: (Nx2)-array of x, y position
        """
        positions = []
        last_i = None
        last_p = None
        for i in range(self.start_time_step, self.end_time_step + 1):
            pose = self.poses.get(i)
            if pose is None:
                # Interpolate none poses
                j = i + 1
                next_p = self.poses.get(j)
                while next_p is None:
                    j += 1
                    next_p = self.poses.get(j)
                pos = [
                    np.interp(i, [last_i, j], [last_p.x, next_p.x]),
                    np.interp(i, [last_i, j], [last_p.y, next_p.y]),
                ]
            else:
                last_p = pose
                last_i = i
                pos = [pose.x, pose.y]
            positions.append(pos)
        return np.array(positions)

    def __len__(self):
        return self.end_time_step - self.start_time_step + 1

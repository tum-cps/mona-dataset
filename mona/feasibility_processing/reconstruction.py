from functools import singledispatch
from typing import Optional, Tuple

from commonroad.scenario.trajectory import Trajectory

from .base_optimizer import ReconstructionFailedException
from .ks_optimizer import KSOptimizer
from .pm_optimizer import PMOptimizer
from commonroad_dc.feasibility.vehicle_dynamics import (
    VehicleDynamics,
    KinematicSingleTrackDynamics,
    PointMassDynamics,
)


@singledispatch
def reconstruct_trajectory(
    dynamics: VehicleDynamics, trajectory: Trajectory, dt: float
) -> Optional[Tuple[Trajectory, Trajectory]]:
    """
    Reconstruct a feasible trajectory from a partly defined or infeasible trajectory.

    :param dynamics: Vehicle dynamics to respect
    :param trajectory: (Infeasible) trajectory
    :param dt: Time step length in seconds
    :return: Feasible trajectory, corresponding input trajectory
    """
    raise NotImplementedError()


@reconstruct_trajectory.register
def _(dynamics: KinematicSingleTrackDynamics, trajectory: Trajectory, dt: float):
    try:
        opti = KSOptimizer(dt, dynamics)
    except ReconstructionFailedException:
        return None
    return opti.reconstruct(trajectory)


@reconstruct_trajectory.register
def _(dynamics: PointMassDynamics, trajectory: Trajectory, dt: float):
    try:
        opti = PMOptimizer(dt, dynamics)
    except ReconstructionFailedException:
        return None
    return opti.reconstruct(trajectory)

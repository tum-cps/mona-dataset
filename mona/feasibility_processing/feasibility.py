import logging
import os
import warnings
from pathlib import Path
from typing import Tuple, Optional, Union

import numpy as np
import pandas as pd
import scipy.integrate
from commonroad.common.solution import VehicleType
from commonroad.scenario.trajectory import State, Trajectory as CRTrajectory
from commonroad_dc.feasibility import feasibility_checker
from commonroad_dc.feasibility.vehicle_dynamics import (
    KinematicSingleTrackDynamics,
)
from matplotlib import pyplot as plt
from omegaconf import DictConfig

from mona.smoothing.trajectory import Trajectory
from mona.utils.util import angle_difference, _draw_trajectory
from .base_optimizer import ReconstructionFailedException
from .ks_optimizer import KSOptimizer, SolverType


class KSModel(KinematicSingleTrackDynamics):
    """Wrapper class for Commonroad's KinematicSingleTrack model class."""

    def __init__(self, vehicle_type: VehicleType, vehicle_length: float):
        """
        Create an KSModel.

        :param vehicle_type: vehicle type
        :param vehicle_length: vehicle length
        """
        super().__init__(vehicle_type)
        self.parameters.a = self.parameters.b = vehicle_length * 0.2

    def forward_simulation(
        self, x: np.array, u: np.array, dt: float, throw: bool = True
    ) -> Union[None, np.array]:
        """Simulates state given input w.r.t. the KSModel.

        :param x: state
        :param u: input
        :param dt: time increment
        :param throw: flag to supress exceptions
        :return: resulting state or None in case of a failure
        """
        within_bounds = self.input_within_bounds(u, throw)
        violates_friction_constraint = self.violates_friction_circle(x, u, throw)
        if not throw and (not within_bounds or violates_friction_constraint):
            return None

        sol = scipy.integrate.solve_ivp(
            self.dynamics, (0, dt), x, "RK45", args=(u,), t_eval=(0, dt)
        )
        if not sol.success:
            return None
        # Euler
        # x1 = x + np.array(self.dynamics(0, x, u)) * dt
        return sol.y[:, 1]

    def _state_to_array(
        self, state: State, steering_angle_default=0.0
    ) -> Tuple[np.array, int]:
        wheelbase = self.parameters.a + self.parameters.b
        if hasattr(state, "yaw_rate"):
            yaw_rate = state.yaw_rate
            v = state.velocity
            steering_angle = np.arctan2(wheelbase * yaw_rate, v)
        else:
            steering_angle = 0.0
        return super()._state_to_array(state, steering_angle)

    def array_to_yaw_rate_array(self, np_states: np.ndarray) -> np.ndarray:
        """Convert numpy state array in commonroad KS format to yaw rate model state.

        state = [x, y, v, yaw angle, yaw_rate]

        :param np_states: Trajectory states in
        [x rear axle, y rear axle, steering angle, v, yaw angle] format
        :return:
        """
        distance_rear_axle = self.parameters.b
        wheel_base = self.parameters.a + self.parameters.b
        psi_rad = np_states[:, 4]
        x = np_states[:, 0] + distance_rear_axle * np.cos(psi_rad)
        y = np_states[:, 1] + distance_rear_axle * np.sin(psi_rad)
        v = np_states[:, 3]
        steering_angle = np_states[:, 2]
        yaw_rate = np.tan(steering_angle) * v / wheel_base
        return np.stack((x, y, v, psi_rad, yaw_rate), axis=1)


def check_feasibility(
    trajectory: np.ndarray, dt: float, dynamics: KSModel
) -> Optional[int]:
    """Check feasibility of a trajectory.

    :param trajectory:
    :param dynamics: Vehicle dynamics model
    :param dt: Time step length
    :return: First time step k that contains an infeasible state transition k-1 -> k,
        None if trajectory is feasible
    """
    feasible = True
    i = 0
    with warnings.catch_warnings():
        warnings.filterwarnings(
            "ignore",
            "Values in x were outside bounds during a minimize step, "
            "clipping to bounds",
        )
        for i, (x_k, x_k1) in enumerate(zip(trajectory[:-1], trajectory[1:]), 1):
            feasible, _ = feasibility_checker.state_transition_feasibility(
                x_k, x_k1, dynamics, dt
            )
            if not feasible:
                break
    return i if not feasible else None


def check_forward_feasibility(
    reconstructed_trajectory: np.ndarray,
    input_trajectory: np.ndarray,
    dt: float,
    dynamics: KSModel,
) -> None:
    """Check if inputs are feasible and result in the provided trajectory.

    This is done by forward simulation and boundary checking.

    :param reconstructed_trajectory: Trajectory states in numpy format provided by
    dynamics.state_to_array()
    :param input_trajectory: Input trajectory in numpy format as provided by
    dynamics.input_to_array()
    :param dt: Time step length in seconds
    :param dynamics: Vehicle dynamics model
    """
    reconstructed_trajectory = CRTrajectory(
        0,
        [dynamics.array_to_state(s, i) for i, s in enumerate(reconstructed_trajectory)],
    )
    cr_input_trajectory = CRTrajectory(
        0, [dynamics.array_to_input(e, i) for i, e in enumerate(input_trajectory)]
    )
    input_feasible, simulated_trajectory = feasibility_checker.input_vector_feasibility(
        reconstructed_trajectory.state_list[0], cr_input_trajectory, dynamics, dt
    )
    assert input_feasible, "Input trajectory infeasible!"
    for state, state_sim in zip(
        reconstructed_trajectory.state_list, simulated_trajectory.state_list
    ):
        np.testing.assert_allclose(state.position, state_sim.position, atol=2.0e-2)
        np.testing.assert_allclose(
            angle_difference(state.orientation, state_sim.orientation), 0, atol=3.0e-2
        )


logger = logging.getLogger(__name__)


def subsample(df: pd.DataFrame, factor: int) -> Tuple[float, pd.DataFrame]:
    """Subsamples data to decrease the time discretization of the trajectories.

    :param df: input data that shall be subsampled
    :param factor: factor that describes the subsampling
    :return: tuple of new time increment and subsampled data
    """
    df = df.copy()
    df.sort_values("timestamp_ms", inplace=True)
    df = df.iloc[::factor]
    dt = np.round(df.timestamp_ms.iloc[1] - df.timestamp_ms.iloc[0] / 1000.0, 4)
    return dt, df


def draw_trajectory_optimized(
    track_id: int, original: pd.DataFrame, feasible: Trajectory
) -> None:
    """Draws original and optimized trajectory side-by-side.

    :param track_id: track id (only for visualization)
    :param original: original trajectory
    :param feasible: trajectory optimized for feasibility
    """
    original = original[["x", "y", "v", "psi_rad", "yaw_rate"]].values
    feasible = feasible.dataframe[["x", "y", "v", "psi_rad", "yaw_rate"]].values
    path = Path("/tmp/optimizer/optimized")
    os.makedirs(path, exist_ok=True)
    f, axs = plt.subplots(6, 2, figsize=(8, 16), sharex="none")
    f.suptitle(f"Trajectory {track_id}")
    f.tight_layout()
    error = np.abs(original - feasible)
    _draw_trajectory(axs[:, 0], original, "before")
    _draw_trajectory(axs[:, 0], feasible, "optimized")

    axs[0, 1].plot(np.linalg.norm(error[:, :2], axis=1))
    axs[0, 1].set_ylabel("error")
    for i, ax in enumerate(axs[1:, 1]):
        ax.set_xlabel("t [s]")
        ax.plot(error[:, i])
        ax.set_ylabel("error")
    f.savefig(path / f"trajectory_{track_id}.png", bbox_inches="tight")
    plt.close(f)


def draw_trajectory(track_id: int, trajectory: Trajectory, prefix: str) -> None:
    """Draws a single trajectory.

    :param track_id: track id (only for visualization)
    :param trajectory: trajectory
    :param prefix: prefix string that names the directory where the plot is saved
    """
    trajectory = trajectory[["x", "y", "v", "psi_rad", "yaw_rate"]].values
    path = Path("/tmp/optimizer") / prefix
    os.makedirs(path, exist_ok=True)
    f, axs = plt.subplots(6, figsize=(8, 8), sharex="none")
    f.suptitle(f"Trajectory {track_id}")
    f.tight_layout()
    _draw_trajectory(axs, trajectory)
    f.savefig(path / f"trajectory_{track_id}.png", bbox_inches="tight")
    plt.close(f)


def calculate_error(
    reconstructed_trajectory: np.array, original_trajectory: np.array
) -> dict:
    """Calculates errors between reconstructed and original trajectory.

    :param reconstructed_trajectory: reconstructed trajectory (yaw rate model state)
    :param original_trajectory: original trajectory (yaw rate model state)
    returns: dictionary with errors
    """
    err = reconstructed_trajectory - original_trajectory
    err[:, 3] = angle_difference(
        reconstructed_trajectory[:, 3], original_trajectory[:, 3]
    )
    abs_err = np.abs(err)
    mean_err = np.mean(abs_err, axis=0)
    max_err = np.max(abs_err, axis=0)
    pos_err = np.linalg.norm(err[:, :2], axis=1)
    mean_pos_err = np.mean(pos_err)
    max_pos_err = np.max(pos_err)
    return {
        "mae_position": mean_pos_err,
        "max_error_position": max_pos_err,
        "mae_orientation": mean_err[3],
        "max_error_orientation": max_err[3],
    }


def _create_trajectory_object(yaw_rate_trajectory, input_traj_df) -> Trajectory:
    track_id = input_traj_df.iloc[0].track_id
    length = input_traj_df["length"].iloc[0]
    width = input_traj_df["width"].iloc[0]
    df = input_traj_df.copy()
    df[["x", "y", "v", "psi_rad", "yaw_rate"]] = yaw_rate_trajectory
    df["timestamp_sec"] = df.timestamp_ms / 1000.0
    rec_array = df.to_records(index=False)
    traj_df = Trajectory(rec_array, length, width, track_id)
    return traj_df


def optimize_track(p: Tuple[pd.DataFrame, DictConfig]) -> Tuple[dict, Trajectory]:
    """Optimizes trajectory w.r.t. feasibility, using configuration data provided.

    :param p: tuple of input trajectory data and configuration
    returns: tuple of trajectory information (optimized) and the optimized trajectory
    """
    input_traj_df, feasibility_config = p
    track_id = input_traj_df.track_id.iloc[0]
    dt, input_traj_df = subsample(input_traj_df, feasibility_config.subsample)
    vehicle_length = input_traj_df.length.iloc[0]
    vehicle_model = KSModel(VehicleType.BMW_320i, vehicle_length=vehicle_length)
    traj = input_traj_df[["frame_id", "x", "y", "v", "psi_rad", "yaw_rate"]].copy()
    traj.rename(
        columns={"frame_id": "time_step", "v": "velocity", "psi_rad": "orientation"},
        inplace=True,
    )
    traj["position"] = list(traj[["x", "y"]].values)
    original_trajectory = traj.to_records(index=False)

    trajectory_arr = np.array(
        [vehicle_model.state_to_array(s)[0] for s in original_trajectory]
    )
    if feasibility_config.check_feasibility:
        first_infeasible_time_step = check_feasibility(
            original_trajectory, dt, vehicle_model
        )
    else:
        logger.warning("Feasibility check disabled! Optimizing all trajectories!")
        first_infeasible_time_step = 0
    info = {}
    reconstructed_trajectory = None
    if first_infeasible_time_step is not None:

        optimizer = KSOptimizer(
            dt,
            vehicle_model,
            solver=SolverType.get_block_sqp_available(),
            **feasibility_config.optimizer,
        )
        # Trajectory is a-priori infeasible
        logger.debug(
            "Track %d infeasible after time step %s",
            track_id,
            first_infeasible_time_step,
        )
        try:
            (
                reconstructed_rear_axle_trajectory,
                reconstructed_inputs,
            ) = optimizer.reconstruct(trajectory_arr)
            # Check if solution is really feasible
            if feasibility_config.verify_solution:
                check_forward_feasibility(
                    reconstructed_rear_axle_trajectory,
                    reconstructed_inputs,
                    dt,
                    vehicle_model,
                )
            reconstructed_states = vehicle_model.array_to_yaw_rate_array(
                reconstructed_rear_axle_trajectory
            )
            reconstructed_trajectory = _create_trajectory_object(
                reconstructed_states, input_traj_df
            )
            if feasibility_config.debug:
                draw_trajectory_optimized(
                    track_id, input_traj_df, reconstructed_trajectory
                )
            error = calculate_error(reconstructed_states, traj.iloc[:, 1:-1].values)
            info["feasible"] = True
            logger.debug("MAE Position: %.3f", error["mae_position"])
        except AssertionError:
            raise
        except ReconstructionFailedException:
            info["feasible"] = False
            logger.debug("Failed to create feasible trajectory for track %d", track_id)
            if feasibility_config.debug:
                draw_trajectory(track_id, input_traj_df, "optimization_failed")
        # Optimized
        info["optimized"] = True
    else:
        # Trajectory is a-prior feasible
        if feasibility_config.debug:
            draw_trajectory(track_id, input_traj_df, "apriori_feasible")
        info["feasible"] = True
        info["optimized"] = False
        first_infeasible_time_step = len(input_traj_df.index)
    info["track_id"] = track_id
    info["first_infeasible_time_step"] = first_infeasible_time_step
    info["trajectory_length"] = len(input_traj_df.index)
    return info, reconstructed_trajectory

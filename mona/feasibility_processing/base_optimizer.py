from abc import ABCMeta, abstractmethod
from copy import copy
from typing import Union, Dict, Tuple

import numpy as np
from commonroad.scenario.trajectory import Trajectory, State
from commonroad_dc.feasibility.vehicle_dynamics import (
    VehicleDynamics,
    KinematicSingleTrackDynamics,
    PointMassDynamics,
)


class ReconstructionFailedException(Exception):
    """Wrapper for Exception class."""

    pass


class BaseOptimizer(metaclass=ABCMeta):
    """Base Optimizer class for reconstructing trajectories w.r.t. vehicle dynamics."""

    def __init__(self, dt: float, vehicle_dynamics: VehicleDynamics):
        """
        Create a BaseOptimizer object.

        :param dt: Time step length in seconds
        :param vehicle_dynamics: vehicle dynamics to respect
        """
        self.dt = dt
        self.vehicle_dynamics = vehicle_dynamics

    @property
    def vehicle_params(self):
        """Get the parameters of the respected vehicle dynamics.

        :return: parameters of vehicle dynamics
        """
        return self.vehicle_dynamics.parameters

    def _fill_missing_state_values(self, state: State, fill=np.nan):
        state = copy(state)
        if isinstance(self.vehicle_dynamics, KinematicSingleTrackDynamics):
            required_attrs = ["position", "orientation", "steering_angle", "velocity"]
        elif isinstance(self.vehicle_dynamics, PointMassDynamics):
            required_attrs = ["position", "velocity", "orientation"]
        else:
            raise TypeError

        for attr in required_attrs:
            if not hasattr(state, attr):
                if attr == "position":
                    state.position = np.full(2, fill)
                else:
                    setattr(state, attr, fill)
        return state

    def reconstruct(
        self, trajectory: Union[Trajectory, np.ndarray, Dict[int, State]]
    ) -> Union[Tuple[Trajectory, Trajectory], Tuple[np.ndarray, np.ndarray]]:
        """Reconstruct the trajectory and corresponding inputs.

        :param trajectory: Reference trajectory which may either be a Trajectory
            object, a numpy array of state vectors, or a dictionary mapping from
            time step to State objects (can be used if states are missing).
        :return: optimized trajectory as numpy array if reference trajectory is
            given as numpy array otherwise as Trajectory; input trajectory as
            numpy array if reference trajectory is given as numpy array otherwise as
            Trajectory.
        """
        if isinstance(trajectory, Trajectory):
            np_trajectory = np.array(
                [
                    self.vehicle_dynamics.state_to_array(
                        self._fill_missing_state_values(s)
                    )[0]
                    for s in trajectory.state_list
                ]
            )
        elif isinstance(trajectory, dict):
            initial_time_step = min(trajectory.keys())
            final_time_step = max(trajectory.keys())
            np_trajectory = []
            for i in range(initial_time_step, final_time_step + 1):
                state = trajectory.get(i)
                if state is None:
                    state = State(timestep=i)
                np_trajectory.append(state)
            np_trajectory = np.stack(np_trajectory)
        else:
            np_trajectory = trajectory

        res = self._reconstruct(np_trajectory)

        recons_traj, inputs = res
        if isinstance(trajectory, Trajectory):
            recons_traj = Trajectory(
                trajectory.initial_time_step,
                [
                    self.vehicle_dynamics.array_to_state(s, i)
                    for i, s in enumerate(recons_traj, trajectory.initial_time_step)
                ],
            )
            inputs = Trajectory(
                trajectory.initial_time_step,
                [
                    self.vehicle_dynamics.array_to_input(u, i)
                    for i, u in enumerate(inputs, trajectory.initial_time_step)
                ],
            )
        return recons_traj, inputs

    @abstractmethod
    def _reconstruct(self, np_trajectory) -> Tuple[np.ndarray, np.ndarray]:
        """Reconstruct the trajectory and corresponding inputs.

        :param np_trajectory: Reference trajectory as numpy array
        :return: optimized trajectory as numpy array, input trajectory as
        numpy array
        """
        pass

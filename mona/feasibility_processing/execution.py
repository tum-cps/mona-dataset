import logging
from pathlib import Path

import pandas as pd
from ray.util.multiprocessing import Pool
from tqdm import tqdm

from mona.feasibility_processing import optimize_track
from mona.feasibility_processing.ks_optimizer import SolverType
from mona.serialization.trajectory_serializer import InteractionTrajectorySerializer
from mona.utils.util import read_trajectories

logger = logging.getLogger(__name__)


def optimize_feasibility(config, input_traj_df_all=None):
    SolverType.get_block_sqp_available(warn=True)
    output_dir = Path(config["output_dir"])
    feasible_trajectories_path = (
        output_dir / f"feasible_trajectories.{config.trajectory_output_format}"
    )
    if feasible_trajectories_path.exists() and config.get("skip_existing", False):
        logger.info(
            f"Found previous results {feasible_trajectories_path.absolute()}. "
            f"Skipping pose estimation!"
        )
        return
    if input_traj_df_all is None:
        # load input
        trajectories_path = list(
            output_dir.rglob(f"trajectories.{config.trajectory_output_format}")
        )
        if len(trajectories_path) == 0:
            logger.info(
                "No trajectory file found in %s! Skipping!", output_dir.absolute()
            )
            return
        trajectories_path = trajectories_path[0]
        input_traj_df_all = read_trajectories(trajectories_path)

    # load parameters
    feasibility_config = config["feasibility"]

    # loop over all tracks in input file
    track_ids = input_traj_df_all["track_id"].unique()
    if config.num_threads <= 1:
        outs = []
        for track_id in tqdm(track_ids, desc="Feasibility optimization", unit="track"):
            input_traj_df = input_traj_df_all[input_traj_df_all.track_id == track_id]
            out = optimize_track((input_traj_df, feasibility_config))
            outs.append(out)
    else:
        # parallelize
        with Pool(config.num_threads) as pool:
            it = pool.imap_unordered(
                optimize_track,
                [
                    (
                        input_traj_df_all[input_traj_df_all.track_id == track_id],
                        feasibility_config,
                    )
                    for track_id in track_ids
                ],
                chunksize=1,
            )
            outs = [
                _
                for _ in tqdm(
                    it,
                    total=len(track_ids),
                    desc="Feasibility optimization",
                    unit="track",
                )
            ]
    (info, output_trajectories) = zip(*outs)

    # write output file
    serializer = InteractionTrajectorySerializer(
        filter(lambda t: t is not None, output_trajectories)
    )
    serializer.save(feasible_trajectories_path)

    deviations = pd.DataFrame(info)
    deviations.set_index("track_id", inplace=True)
    deviations.sort_index(inplace=True)
    deviations.to_csv(output_dir / "feasibility_error.csv")
    deviations.optimized = deviations.optimized.astype(bool)
    deviations.feasible = deviations.feasible.astype(bool)
    priori_feasible = (
        len(deviations[deviations.feasible & ~deviations.optimized].index)
        / len(deviations.index)
        * 100
    )
    posteriori_feasible = (
        len(deviations[deviations.feasible].index) / len(deviations.index) * 100
    )
    if priori_feasible < 100:
        optimization_failed = (
            len(deviations[~deviations.feasible & deviations.optimized].index)
            / len(deviations[deviations.optimized].index)
            * 100
        )
    else:
        optimization_failed = -1
    logger.info(
        "A priori feasible %.2f%%, posteriori feasible: %.2f%%, optimization "
        "failed: %.2f",
        priori_feasible,
        posteriori_feasible,
        optimization_failed,
    )

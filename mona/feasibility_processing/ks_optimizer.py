import contextlib
import dataclasses
import enum
import io
import warnings
from dataclasses import dataclass, field
from pathlib import Path
from typing import Dict, Tuple

import casadi as ca

import numpy as np

from .base_optimizer import ReconstructionFailedException, BaseOptimizer
from commonroad_dc.feasibility.vehicle_dynamics import VehicleDynamics

from ..utils.util import angle_difference


def forward_fill_nan(arr: np.array) -> np.array:
    """Fills nan's of one-dimensional numpy array with previous non-nan value."""
    # code copied from https://stackoverflow.com/questions/41190852/most-efficient-way-to-forward-fill-nan-values-in-numpy-array  # noqa: E501
    mask = np.isnan(arr)
    idx = np.where(~mask, np.arange(mask.shape[-1]), 0)
    np.maximum.accumulate(idx, axis=-1, out=idx)
    out = arr[idx]
    return out


class SolverType(enum.Enum):
    """Class that defines the solver type being used."""

    IPOPT = "ipopt"
    BLOCK_SQP = "blocksqp"
    SQP = "sqpmethod"

    @classmethod
    def get_block_sqp_available(cls, warn=False) -> "SolverType":
        """
        Getter function for blocksqp solver, if available on this system.

        :param warn: boolean flag that enables warning
        :returns: available sqp solver
        """
        if Path("/usr/local/lib/libcoinhsl.so").exists():
            return cls.BLOCK_SQP
        else:
            if warn:
                warnings.warn("blocksqp solver unavailable! Using sqpmethod!")
            return cls.SQP


@dataclass(frozen=True)
class BaseOpts:
    """Data class defining settings of KS feasibility optimizer."""

    verbose: bool = False
    expand: bool = False
    print_time: bool = False
    qpsol_options: Dict[str, bool] = field(
        default_factory=lambda: {"error_on_fail": False}
    )


@dataclass(frozen=True)
class BlockSqpOpts(BaseOpts):
    """Data class defining blocksqp settings of KS feasibility optimizer."""

    max_iter: int = 200
    max_time_qp: float = 2.0
    print_header: bool = False
    print_iteration: bool = False


@dataclass(frozen=True)
class IpoptSolverOpts(BaseOpts):
    """Data defining ipopt solver settings of KS feasibility optimizer."""

    max_cpu_time: int = 120
    max_iter: int = 500
    print_level: int = 5
    sb: str = "yes"  # Suppress banner
    fixed_variable_treatment: str = "make_constraint"


@dataclass(frozen=True)
class IpoptOpts(BaseOpts):
    """Data defining ipopt settings of KS feasibility optimizer."""

    ipopt: IpoptSolverOpts = dataclasses.field(default_factory=IpoptSolverOpts)


class KSOptimizer(BaseOptimizer):
    """Kinematic single track optimizer.

    Trajectories are reconstructed w.r.t. vehicle dynamics.
    """

    def __init__(
        self,
        dt: float,
        vehicle_dynamics: VehicleDynamics,
        q_x=None,
        q_u=None,
        solver: SolverType = SolverType.get_block_sqp_available(),
        verbose=False,
    ):
        """
        Create a KSOptimizer object.

        :param dt: Time step length in seconds
        :param vehicle_dynamics: vehicle dynamics to respect
        :param q_x: state weights
        :param q_u: input weights
        :param solver: solver type
        :param verbose: flag for verbose output
        """
        super().__init__(dt, vehicle_dynamics)
        if q_x is not None:
            self.q_x = np.array(q_x)
        else:
            self.q_x = np.array([0.1, 0.1, 0.0, 0.0, 1.0])

        if q_u is not None:
            self.q_u = np.array(q_u)
        else:
            self.q_u = np.array([0.0, 0.0])
        self.solver = solver
        x = ca.MX.sym("x0", 5, 1)
        u = ca.MX.sym("u", 2, 1)
        ode = {"x": x, "p": u, "ode": self._ks_dynamics(x, u)}
        self.ode_integrator = ca.integrator("integrator", "rk", ode, {"tf": self.dt})
        self.verbose = verbose

    @property
    def _solver_opts(self):
        if self.solver == SolverType.IPOPT:
            return IpoptOpts(verbose=self.verbose)
        elif self.solver == SolverType.BLOCK_SQP:
            return BlockSqpOpts()
        else:
            return BaseOpts(verbose=self.verbose)

    @property
    def _wheelbase(self):
        return self.vehicle_params.a + self.vehicle_params.b

    def _ks_dynamics(self, state, control_input):
        """Forward dynamics of kinematic single track model.

        :param state: [x, y, delta, v, psi]
        :param control_input: [delta_dot, a_lon]
        :return: x_dot
        """
        delta_dot = control_input[0]
        v_dot = control_input[1]
        delta = state[2]
        v = state[3]
        psi = state[4]

        psi_dot = v / self._wheelbase * ca.tan(delta)
        s_x_dot = v * ca.cos(psi)
        s_y_dot = v * ca.sin(psi)
        return ca.vertcat(s_x_dot, s_y_dot, delta_dot, v_dot, psi_dot)

    def _ode_integrate(self, x_k, u_k):
        """Solve initial value problem for KS vehicle dynamics.

        :param x_k: current state
        :param u_k: control input
        :return: x_k+1
        """
        return self.ode_integrator(x0=x_k, p=u_k)["xf"]

    @staticmethod
    def _state_difference(x_opt: ca.MX, x_ref: np.ndarray):
        """Calculates the difference between two states: x_opt - x_ref.

        If an element in x_ref is nan, the difference is set to 0.

        :param x_opt: Optimized state
        :param x_ref: Reference state
        :return: x_opt - x_ref
        """
        d = x_opt - x_ref
        for i in range(5):
            if np.isnan(x_ref[i]):
                d[i] = 0.0
        return d

    def _setup_problem(self, trajectory: np.ndarray, decision_variables):
        # Decision variable constraints upper, lower bound
        state_upper_bound = [
            ca.inf,
            ca.inf,
            self.vehicle_params.steering.max,
            self.vehicle_params.longitudinal.v_max,
            ca.inf,
        ]
        state_lower_bound = [
            -ca.inf,
            -ca.inf,
            self.vehicle_params.steering.min,
            self.vehicle_params.longitudinal.v_min,
            -ca.inf,
        ]
        control_upper_bound = [
            self.vehicle_params.steering.v_max,
            self.vehicle_params.longitudinal.a_max,
        ]
        control_lower_bound = [
            self.vehicle_params.steering.v_min,
            -self.vehicle_params.longitudinal.a_max,
        ]

        ubx = [state_upper_bound.copy()]
        lbx = [state_lower_bound.copy()]
        # Ensure unambiguous orientation in initial states
        ubx[0][4] = np.pi
        lbx[0][4] = -np.pi
        ubu = []
        lbu = []
        # Inequality constraints upper, lower bound
        ubg = []
        lbg = []
        # Inequality function
        g = []
        xk = decision_variables[:5]
        error = self._state_difference(xk, trajectory[0:1].T)
        cost = ca.dot(error, error * self.q_x)
        # Split off first state
        decision_variables = decision_variables[5:]
        decision_variables = decision_variables.reshape((5 + 2, -1))
        for i in range(1, trajectory.shape[0]):
            u = decision_variables[:2, i - 1]
            x_dot = self._ks_dynamics(xk, u)
            v = xk[2]
            xk1 = self._ode_integrate(xk, u)
            xk = decision_variables[2:, i - 1]
            error = self._state_difference(xk, trajectory[i : i + 1].T)
            cost += ca.dot(error, error * self.q_x) + ca.dot(u, u * self.q_u)
            # Path constraint and friction circle
            a_lat = v * x_dot[4]
            a_lon = u[1]
            g.extend(ca.vertsplit(xk1 - xk) + [a_lon**2 + a_lat**2])
            ubg.extend(
                [
                    0,
                    0,
                    0,
                    0,
                    0,
                    self.vehicle_params.longitudinal.a_max**2,
                ]
            )
            lbg.extend(
                [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                ]
            )
            # Constrain inputs acceleration and steering velocity
            ubu.append(control_upper_bound)
            lbu.append(control_lower_bound)
            ubx.append(state_upper_bound)
            lbx.append(state_lower_bound)
        # Stitch together in correct order
        ubx = np.concatenate((ubx[0], np.concatenate((ubu, ubx[1:]), axis=-1).ravel()))
        lbx = np.concatenate((lbx[0], np.concatenate((lbu, lbx[1:]), axis=-1).ravel()))
        g = ca.vertcat(*g)
        return cost, ubx, lbx, g, ubg, lbg

    def _reconstruct(self, np_trajectory: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """Reconstruct the trajectory and corresponding inputs.

        :param np_trajectory: Reference trajectory as numpy array with shape
        N x 5
        :return: optimized trajectory as numpy array, input trajectory as
        numpy array
        """
        # De-normalize heading angle to ensure continuity in angles
        if np.isnan(np_trajectory[:, 4]).any():
            filled = forward_fill_nan(np_trajectory[:, 4])
            denormed = self._denormalize_angle(filled)
            denormed[np.isnan(np_trajectory[:, 4])] = np.nan
            np_trajectory[:, 4] = denormed
        else:
            np_trajectory[:, 4] = self._denormalize_angle(np_trajectory[:, 4])
        # Trajectory length
        n_steps = np_trajectory.shape[0]
        # Control inputs as decision variables
        n_controls = 2 * (n_steps - 1)
        n_states = 5 * n_steps
        # Order of variables x_0[5], u_0[2], x_1, u_1, ..., u_n-1, x_n-1
        xu = ca.MX.sym("xu", n_controls + n_states, 1)
        cost_function, ubx, lbx, constraint_function, ubg, lbg = self._setup_problem(
            np_trajectory, xu
        )
        # Setup solver
        nlp = {"x": xu, "f": cost_function, "g": constraint_function}
        solver = ca.nlpsol(
            "solver", self.solver.value, nlp, dataclasses.asdict(self._solver_opts)
        )
        # Initial guess
        x0 = self._initial_guess(np_trajectory)
        # Solve the problem
        if self.verbose:
            sol = solver(x0=x0, lbx=lbx, ubx=ubx, lbg=lbg, ubg=ubg)
        else:
            # Mute solver
            with contextlib.redirect_stdout(io.StringIO()):
                sol = solver(x0=x0, lbx=lbx, ubx=ubx, lbg=lbg, ubg=ubg)
        stats = solver.stats()
        if not stats["success"]:
            raise ReconstructionFailedException()
        sol_variables = sol["x"].full()

        # Skip first state
        # Be aware that casadi uses Fortran order. Since it uses column
        # vectors and numpy row vectors
        # reshaping works as usual.
        optimized_traj = np.reshape(sol_variables[5:], (n_steps - 1, 2 + 5))
        # Split away inputs
        inputs = optimized_traj[:, :2]
        optimized_traj = optimized_traj[:, 2:]
        # Insert first state again
        optimized_traj = np.insert(optimized_traj, 0, sol_variables[:5].ravel(), axis=0)
        # Normalize orientation angle
        optimized_traj[:, 4] = np.arctan2(
            np.sin(optimized_traj[:, 4]), np.cos(optimized_traj[:, 4])
        )
        return optimized_traj, inputs

    @staticmethod
    def _denormalize_angle(angles):
        denorm_angles = np.cumsum(angle_difference(angles[:-1], angles[1:]))
        denorm_angles += angles[0]
        denorm_angles = np.insert(denorm_angles, 0, angles[0])
        return denorm_angles

    def _initial_guess(self, np_trajectory):
        # Existing trajectory + inputs from differentiation
        clipped = np.clip(
            np_trajectory,
            [
                -np.inf,
                -np.inf,
                self.vehicle_params.steering.min,
                self.vehicle_params.longitudinal.v_min,
                -np.inf,
            ],
            [
                np.inf,
                np.inf,
                self.vehicle_params.steering.max,
                self.vehicle_params.longitudinal.v_max,
                np.inf,
            ],
        )
        diff_v = np.clip(
            np.diff(clipped[:, 3:4], axis=0) / self.dt,
            -self.vehicle_params.longitudinal.a_max,
            self.vehicle_params.longitudinal.a_max,
        )
        diff_delta = np.clip(
            angle_difference(clipped[:-1, 2:3], clipped[1:, 2:3]) / self.dt,
            self.vehicle_params.steering.v_min,
            self.vehicle_params.steering.v_max,
        )
        x0 = np.concatenate(
            (
                clipped[0],
                np.concatenate((diff_delta, diff_v, clipped[1:]), axis=1).ravel(),
            )
        )
        x0 = np.nan_to_num(x0)
        return x0

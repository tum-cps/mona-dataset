import cvxpy as cp

import numpy as np

from .base_optimizer import ReconstructionFailedException, BaseOptimizer
from commonroad_dc.feasibility.vehicle_dynamics import PointMassDynamics


class PMOptimizer(BaseOptimizer):
    """Optimizer for reconstructing trajectories of point-mass vehicle dynamics."""

    def __init__(
        self, dt, vehicle_dynamics: PointMassDynamics, q_x=None, q_u=None, verbose=False
    ):
        """
        Create an optimizer.

        :param dt: Time step length in seconds
        :param vehicle_dynamics: point mass vehicle dynamics to respect
        :param q_x: [4, 4] State deviation cost weight matrix
        :param q_u: [2, 2] Input cost weight matrix
        :param verbose: Show optimizer output
        """
        super().__init__(dt, vehicle_dynamics)
        if q_x is not None:
            self.q_x = np.array(q_x)
            assert self.q_x.shape == (4, 4)
        else:
            self.q_x = np.diag([1.0, 1.0, 1.0, 1.0])

        if q_u is not None:
            self.q_u = np.array(q_u)
            assert self.q_u.shape == (2, 2)
        else:
            self.q_u = np.diag([1.0e-5, 1.0e-5])

        self.A = np.eye(4)
        self.A[0, 2] = self.dt
        self.A[1, 3] = self.dt
        self.B = np.array(
            [
                [0.5 * self.dt * self.dt, 0],
                [0, 0.5 * self.dt * self.dt],
                [self.dt, 0],
                [0, self.dt],
            ]
        )
        self.verbose = verbose

    def _reconstruct(self, trajectory):
        a_max_squared = self.vehicle_params.longitudinal.a_max**2

        # Decision variables
        state_var = cp.Variable(shape=trajectory.shape)
        control_var = cp.Variable(shape=(trajectory.shape[0] - 1, 2))

        x_0_var = state_var[0:1].T
        x_0 = trajectory[0:1].T
        # Cost for initial state deviation
        cost = cp.quad_form(self._state_difference(x_0, x_0_var), self.q_x)
        constraints = []

        for k in range(1, state_var.shape[0]):
            # previous state
            x_k_prev_var = state_var[k - 1 : k].T
            # Current state variable
            x_k_var = state_var[k : k + 1].T
            # Current reference state
            x_k = trajectory[k : k + 1].T
            # Current control variable for transition from k-1 -> k
            u_k_var = control_var[k - 1 : k].T
            # Forward simulation from k-1 to k
            x_k_sim = self.A @ x_k_prev_var + self.B @ u_k_var
            # Cost from state deviation and control magnitude
            cost += cp.quad_form(
                self._state_difference(x_k, x_k_var), self.q_x
            ) + cp.quad_form(u_k_var, self.q_u)
            # Constraints
            # Continuity constraint (multiple shooting)
            constraints.append(x_k_sim == x_k_var)
            # Friction circle
            constraints.append(cp.sum_squares(u_k_var) <= a_max_squared)

        prob = cp.Problem(cp.Minimize(cost), constraints)

        # Solve optimization problem
        prob.solve(verbose=self.verbose)
        if prob.status != "optimal":
            raise ReconstructionFailedException()
        return state_var.value, control_var.value

    @staticmethod
    def _state_difference(x_ref: np.ndarray, x_var):
        diff = [
            x_var[i] - x_ref[i] if not np.isnan(x_ref[i]) else 0.0
            for i in range(x_ref.size)
        ]
        return cp.hstack(diff)

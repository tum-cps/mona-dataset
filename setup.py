from setuptools import setup, find_packages

setup(
    name="mona-toolchain",
    version="1.0",
    packages=find_packages(exclude=["test"]),
    include_package_data=True,
    url="commonroad.in.tum.de",
    license="BSD",
    author="Luis Gressenbuch",
    author_email="luis.gressenbuch@tum.de",
    description="",
    entry_points={
        "console_scripts": [
            "mona = mona.cli.mona_cli:app",
            "mona-visualize = mona.cli.visualizer:app"
        ]
    },
)

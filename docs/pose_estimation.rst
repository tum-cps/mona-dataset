Pose Estimation
=======================================================

Strategies
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The pose estimation uses a strategy pattern with the interface :py:class:`PoseEstimator`. Currently, the only two implemented strategies
are the :py:class:`BoxCenterEstimator` and :py:class:`BoxFitEstimator` that work by projecting the middle points of the rotated minimum-area rectangle of a detection to
the ground plane and fitting a fixed size 3D box to the segmentation mask. However, this introduces an error and should be improved in the future.

.. automodule:: mona.pose_estimation.pose_estimation
   :members:
   :undoc-members:
   :member-order: bysource


Pose Object
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The estimation strategy creates a :py:class:`Pose` object for each :py:class:`DetObject` containing the position in the image plane and in
world coordinates. Multiple poses in different frames that are associated with the same object can be grouped to a
:py:class:`PoseTrack` object.

.. autoclass:: mona.pose_estimation.pose.Pose
   :members:
   :undoc-members:
   :member-order: bysource

.. autoclass:: mona.pose_estimation.pose.PoseTrack
   :members:
   :undoc-members:
   :member-order: bysource



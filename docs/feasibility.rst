Feasibility Optimization
=======================================================

The output trajectories from the track smoothing stage are not necessarily drivable by a standard vehicle model due to measurement noise.
As for some applications, e.g., imitation learning, it is crucial that trajectories are drivable and the input actions are known, we provide an optional post-processing step that ensures drivability by a specific vehicle model.
The task is formulated as an optimization problem of minimizing the position and orientation deviation between the extracted trajectory (from smoothing) and the optimal one while adhering to the model constraints.
In particular, a first-order point-mass model and a kinematic single-track model are implemented. The former yields a convex optimization problem, solved using the `cvxpy <https://www.cvxpy.org/>`_ library, the latter a non-linear optimization problem solved using `casadi <https://web.casadi.org/>`_ optimization library.

Kinematic Single Track Model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Casadi offers multiple different backends for solving and optimization problem. As a default we use the *sqpmethod*-backend.

.. _blocksqp:

During our experiments we found that the *blocksqp*-backend produced the best solutions for our use-case. However, this backend requires a third-party optimization library `coinhsl <https://www.hsl.rl.ac.uk/ipopt/>`_, which is free for academic and personal use, but may not be redistributed. Please, refer to `HSL <https://www.hsl.rl.ac.uk/ipopt/>`_ for obtaining the source code and the `casadi FAQs <https://github.com/casadi/casadi/wiki/Obtaining-HSL>`_ for installation instructions.

.. autoclass:: mona.feasibility_processing.KSOptimizer
   :members:
   :inherited-members:
   :member-order: bysource


Point Mass Model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: mona.feasibility_processing.PMOptimizer
   :members:
   :inherited-members:
   :member-order: bysource






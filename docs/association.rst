Association
=======================================================

During the association stage, individual detections on each frame are associated with an object.

:py:class:`mona.association.deep_sort.DeepSortAssociator` is the strategy object, implementing an adapted version of the DeepSORT multi-object tracking approach. It uses a combination of Kalman filter (KF) predictions and IoU metric as a score for a linear sum assignment problem.

DeepSortAssociator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: mona.association.deep_sort
   :members:
   :undoc-members:
   :member-order: bysource

Tracklet
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
A tracked object is represented by a :py:class:`mona.association.tracklet.PoseTrack` object. It contains the KF state and the associated poses of the past frames. A tracklet can have differnt states, i.e., tentative, confirmed, inactive and dropped. After the first assocation a tracklet is tentative until k poses have been associated in consecutive frames. If the tracklet is missed in a frame while still tentative, its state becomes dropped. If a confirmed tracklet is not associated for at least n frames, its state becomes inactive and it will not be considered for further associations.

.. automodule:: mona.association.tracklet
   :members:
   :undoc-members:
   :member-order: bysource
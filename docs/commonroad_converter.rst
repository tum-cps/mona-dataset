CommonRoad Converter
=======================================================

The MONA dataset natively supports conversion into CommonRoad scenarios.

Installation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The converter is included in the *CommonRoad Dataset converters* package. It can be easily installed via pip::

    pip install commonroad-dataset-converter

Conversion
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The converter is accessible from a command line interface and supports multiple options::

    crconvert mona --help

    Usage: crconvert mona [OPTIONS] LOCATION:{east|west|merge} TRAJECTORY_FILE

      Convert MONA recording into CommonRoad scenario(s).

    Arguments:
      LOCATION:{east|west|merge}  Recording location of the trajectory file
                                  [required]
      TRAJECTORY_FILE             Path to the trajectory file in csv or parquet
                                  format  [required]

    Options:
      --output-dir PATH               Output directory for scenarios  [default: .]
      --track-id INTEGER              Track id to use for the planning problem
      --num-scenarios INTEGER         Number of scenarios to extract
      --max-duration INTEGER          Maximum duration of a scenario in number of
                                      time steps
      --keep-ego / --no-keep-ego      Keep the vehicle of the planning problem
                                      [default: no-keep-ego]
      --disjunctive-scenarios / --no-disjunctive-scenarios
                                      Create only non-overlapping scenarios
                                      [default: disjunctive-scenarios]
      --num-processes INTEGER         Number of processes to use for conversion
                                      [default: 1]
      --help                          Show this message and exit.



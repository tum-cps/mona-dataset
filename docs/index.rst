.. Video Trajectory Extraction documentation master file, created by
   sphinx-quickstart on Wed Sep 29 09:35:08 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the MONA dataset documentation!
=======================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. toctree::
   :maxdepth: 2
   :caption: First steps:

   docker
   getting_started
   visualization
   commonroad_converter

.. toctree::
   :maxdepth: 2
   :caption: Documentation:

   detection
   camera_model
   pose_estimation
   association
   smoothing
   feasibility


.. figure:: uml/mona_toolchain.png
  :width: 1200

  Important classes of the MONA toolchain

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Docker Image
=======================================================

We assume you have ``docker`` installed. If not please refer to https://docker.com

Build the docker image with the provided docker file::

    docker build -t mona-toolchain .

The main application can be run with::

    docker run mona-toolchain mona --help

The visualizer with ::

    docker run mona-toolchain mona-visualize --help


HSL
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you have access to the :ref:`HSL library <blocksqp>`, place the .zip file in the project root directory and uncomment the corresponding lines in the Dockerfile.
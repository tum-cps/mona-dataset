=======================================================
Smoothing
=======================================================

During the smoothing stage, the noisy track positions of type :py:class:`.PoseTrack` are smoothed using the Rauch-Tung-Striebal (RTS) smoother. The output is a trajectory of type :py:class:`.Trajectory`.

`Trajectory` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: mona.smoothing.smoothing.Trajectory
    :members:
    :member-order: bysource

``BaseSmoother`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The :py:class:`.BaseSmoother` defines the above mentioned input and output of the smoothing stage.

.. autoclass:: mona.smoothing.smoothing.BaseSmoother
    :members:
    :member-order: bysource

``ModelBasedRtsSmoother`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The :py:class:`.ModelBasedRtsSmoother` is a Model-based Rauch-Tung-Striebal (RTS) smoother, which extends BaseSmoother class and will process and smooth all
pose tracks.

.. autoclass:: mona.smoothing.smoothing.ModelBasedRtsSmoother
   :members:
   :undoc-members:
   :member-order: bysource

``BaseFilter`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
:py:class:`ModelBasedRtsSmoother` requires an underlying filter implementation, which is defined by :py:class:`BaseFilter`.
.. autoclass:: mona.smoothing.smoothing.BaseFilter

There exist two types of filter implementations, which the user can choose from:

* :py:class:`EKFConstVelocityConstYawrate`: an Extended Kalman Filter with a nonlinear model with constant velocity and constant yaw rate
* :py:class:`EKFPointMass`: a Kalman Filter with a point mass model with acceleration in x and y direction as inputs to the model

Additional filter implementations can easily be added by deriving from :py:class:`BaseFilter`.

For an overview to smoothing, check out Chapter 7 of (Särkkä, 2013):

Särkkä, S. (2013). Bayesian filtering and smoothing (No. 3). Cambridge University Press.

.. figure:: uml/smoothing.png
  :width: 1200
  :alt: Alternative text

  UML Diagram of the most important classes of the smoothing package.

``EKFConstVelocityConstYawrate`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The state :math:`\mathbf{x} = [s_x, s_y, v, theta, omega]` sits in the rear axle of the vehicle, where  where s_x, s_y denote the x- and y- position of the vehicle, theta the heading, v the velocity, and omega the yaw rate.

The measurement is the center position :math:`\mathbf{z} = [x_c, y_c]`. The resulting measurement function is :math:`\mathbf{h} = [x_c + L*cos(theta), y_c+L*sin(theta)]`, which maps the state to the corresponding measurement.
Furthermore, noise in the measurement is captured by the additive zero-mean Gaussian white noises

.. autoclass:: mona.smoothing.ekf_const_velocity_const_yawrate.EKFConstVelocityConstYawrate
   :members:
   :undoc-members:
   :exclude-members: get_pos_x_center, get_pos_y_center, get_vel, get_theta
   :member-order: bysource

``EKFPointMass`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The state :math:`\mathbf{x} = [x_c, y_c, v_x, v_y]` sits in the center of the vehicle, which is denoted by subscript c.

The measurement is the center position :math:`\mathbf{z} = [x_c, y_c]`. The resulting measurement function :math:`\mathbf{h} = [x_c, y_c]`, which maps the state to the corresponding measurement, is trivial.

.. autoclass:: mona.smoothing.ekf_point_mass.EKFPointMass
   :members:
   :undoc-members:
   :exclude-members: get_pos_x_center, get_pos_y_center, get_vel, get_theta
   :member-order: bysource

Visualization
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The MONA toolchain provides a separate command to visualize all intermediate results of the processing pipeline. The following CLI commands gives access to the individual visualization functions::

    mona-visualize --help
    Usage: mona-visualize [OPTIONS] COMMAND [ARGS]...

    Options:
      --help                          Show this message and exit.

    Commands:
      detections    Visualize detections
      poses         Visualize poses
      tracks        Visualize tracks
      trajectories  Visualize trajectories


.. automodule:: mona.visualization.visualization
   :members:
   :undoc-members:
   :member-order: bysource
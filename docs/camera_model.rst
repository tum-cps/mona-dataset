Camera Model
=======================================================

To map between image and world coordinates, a camera model is needed. We use a simple `pin hole camera model <https://docs.opencv.org/3.4.18/d9/d0c/group__calib3d.html>`_ for this purpose. The parameters can be obtained from :py:meth:`~mona.camera_model.CameraModel.calibrate` with pairs of corresponding points in image and world coordinates.

The MONA toolchain provides a convenient set of classes to represent a calibrated camera model:

CameraModel
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: mona.camera_model.CameraModel
   :members:
   :member-order: bysource

TerrainCameraModel
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

While the simple pin hole camera model assumes a planar ground level when back-projecting from image to world coordinates, this assumption does only hold for the locations *west* and *merge*. For *east*, however, there is a significant change in elevation for vehicles that drive into the tunnel in the middle of the image. To compensate, we use a digital terrain model providing the elevation of the ground level. To create MONA with the highest possible accuracy, a terrain model with a 1m mesh size was used. The model was obtained from `LDBV <https://geodatenonline.bayern.de/geodatenonline/npB-2MjTW-LF8EqnslzZ_w/npB2f>`_ but can unfortunately not be freely provided with the dataset due to license restrictions. However, the model can be commercially obtained.

To make MONA still usable without the DGM we provide the open-data version of the DGM with a 50m mesh size. It can simply be replaced with the smaller mesh size version by replacing the file under ``mona/dgm/*.txt`` and the corresponding file path in the *east* config file ``mona/config/locations/east_terrain.yaml``.

.. autoclass:: mona.camera_model.TerrainCameraModel
   :members:
   :member-order: bysource






Detection
=======================================================

Detectron2
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The purpose of the detection module is to find individual vehicles in the camera frame. Currently, we use the general purpose
object detection network `Detectron2 <https://detectron2.readthedocs.io/en/latest/>`_. It provides pre-trained instances
in a `model zoo <https://github.com/facebookresearch/detectron2/blob/main/MODEL_ZOO.md>`_ that can be used for vehicle
detection and segmentation.

.. autoclass:: mona.detection.detector.MpDetectronDetection
   :members:
   :undoc-members:
   :member-order: bysource

Detections
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The detector creates a list of detection objects for each frame, where each detection object consists of its axis-aligned
bounding box, class label, class label confidence and semantic segmentation mask. The semantic segmentation masks are
stored as run-length encoding in the same format as in the COCO dataset for saving memory. Note, that many operations
can be directly executed on the encoded mask without the need to restore the dense representation. See :py:mod:`mona.detection.mask_util`


.. autoclass:: mona.detection.detection.Detection
   :members:
   :undoc-members:
   :member-order: bysource

Serialization
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The detection objects are afterward written to an HDF5 file, which is well suited for large-scale further processing,
as it supports random access to each individual frame, without loading all detections into memory.

.. autoclass:: mona.serialization.detection_serializer.Hdf5Serializer
   :members:
   :undoc-members:
   :member-order: bysource


RLE mask utilities
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: mona.detection.mask_util
    :members:
    :undoc-members:

